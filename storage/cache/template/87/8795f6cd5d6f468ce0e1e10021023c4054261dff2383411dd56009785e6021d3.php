<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/home.twig */
class __TwigTemplate_e04c7d60421173f4076104624b198e6f433f9cffe4037c0c6798d966a4b316d8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"common-home\" class=\"container\">
  <div class=\"row\">";
        // line 3
        echo ($context["column_left"] ?? null);
        echo "
    ";
        // line 4
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 5
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 6
            echo "    ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 7
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 8
            echo "    ";
        } else {
            // line 9
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 10
            echo "    ";
        }
        // line 11
        echo "    <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 12
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
{ locations }
<section id=\"stores\">
\t<div class=\"store-wrap\">
\t\t<div class=\"store-card-wrap\">
\t\t\t<div class=\"store-card-inner\">
\t\t\t\t<div class=\"store-card-item\" data-attr=\"0\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store1.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">г. Одесса, Генуэзская 3Б</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>10:00-22:00</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:+380936121212\">+38 (093) 612-12-12</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"store-card-item\" data-attr=\"1\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store2.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">г. Одесса, Книжный рынок</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>9:00-21:00</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:+380936121212\">+38 (093) 612-12-12</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"store-card-item\" data-attr=\"2\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store3.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">г. Одесса, Жемчужная 5Б</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>10:00-22:00</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:+380936121212\">+38 (093) 612-12-12</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"store-map-wrap\" id=\"stores-map\">
\t
\t\t</div>
\t</div>
</section>
<section id=\"contactForm\" class=\"contactForm-section\">
\t<div class=\"container cf-container\">
\t\t<form class=\"cF-home\">
\t\t\t<h3>Напишите нам</h3>
\t\t\t<hr>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"fio\" class=\"form-label\">ФИО<span>*</span>:
\t\t\t\t\t</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"fio\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label for=\"cF-email\" class=\"form-label\">Email:</label>
\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"cF-email\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label class=\"form-check-label\" for=\"cF-phone\">Номер Телефона<span>*</span>:</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"cF-phone\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"cf-textarea\" class=\"form-label\">Сообщение<span>*</span>:</label>
\t\t\t\t\t<textarea class=\"form-control\" id=\"cf-textarea\" rows=\"6\" required></textarea>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<button type=\"submit\" class=\"cf-button\">Отправить</button>
\t\t</form>
\t</div>

</section>
";
        // line 97
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/common/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 97,  74 => 12,  66 => 11,  63 => 10,  60 => 9,  57 => 8,  54 => 7,  51 => 6,  48 => 5,  46 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/common/home.twig", "");
    }
}
