<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/tmdheader.twig */
class __TwigTemplate_5cf8e13d63ec76ec8850f64cd39039beae8fcfbd7453df488075e35edc542f25 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"mobile-sidebar-overlay collapse\" id=\"pageSubmenu\">
        <div class=\"sidebar-container py-3\">
          <ul class=\"sidebar-navigation\">
          ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
                ";
                // line 10
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 10) ==  -1)) {
                    // line 11
                    echo "                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 12
                        echo "                    ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 12)) {
                            // line 13
                            echo "                    <li class=\"nav-item dropdown\">
                      <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            // line 14
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 14);
                            echo "</a>
                      <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                        ";
                            // line 16
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 16), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 16)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 16), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 17
                                echo "                            ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 18
                                    echo "                            <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 18);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 18);
                                    echo "</a>
                            ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 20
                                echo "                          ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 21
                            echo "                      </div>
                    </li>
                    ";
                        } else {
                            // line 24
                            echo "                    <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 24);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 24);
                            echo "</a></li>
                    ";
                        }
                        // line 26
                        echo "                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 27
                    echo "                ";
                } else {
                    // line 28
                    echo "                    <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 28);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 28);
                    echo "</a>
                    ";
                    // line 29
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 29)) {
                        // line 30
                        echo "                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                        <div class=\"dropdown-inner\">
                        <ul class=\"list-unstyled\">
                        ";
                        // line 33
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 33));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 34
                            echo "                        <li>
                            ";
                            // line 35
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 35)) {
                                echo "\t\t\t\t
                            <a href=\"";
                                // line 36
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 36);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 36);
                                echo "</a>
                            ";
                            } else {
                                // line 38
                                echo "                            <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 38);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 38);
                                echo "</a>\t
                            ";
                            }
                            // line 40
                            echo "                            ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 40)) {
                                // line 41
                                echo "                        
                        <ul>
                        ";
                                // line 43
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 43));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 44
                                    echo "                        <li>
                            ";
                                    // line 45
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 45)) {
                                        echo "\t\t\t\t\t\t
                            <a href=\"";
                                        // line 46
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 46);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 46);
                                        echo "</a>
                            ";
                                    } else {
                                        // line 48
                                        echo "                            <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 48);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 48);
                                        echo "</a>\t
                            ";
                                    }
                                    // line 50
                                    echo "                    </li>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 52
                                echo "                            
                        </ul>\t\t\t\t
                        ";
                            }
                            // line 55
                            echo "                    </li>
                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 57
                        echo "                        </ul>\t\t\t\t
                        </div>
                        </div>
                        
                        ";
                    }
                    // line 61
                    echo "\t
                    </li>\t
                ";
                }
                // line 63
                echo "                
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "          </ul>
          <div class=\"mobile-contacts ps-4\">
            <div class=\"mobile-contact-phone mb-2\">
                <img src=\"/image/catalog/icons/phone-call-white.svg\"> ";
            // line 68
            echo ($context["telephone"] ?? null);
            echo "
            </div>
            <div class=\"mobile-contact-phone\">
                <img src=\"/image/catalog/icons/clock-white.svg\"> ";
            // line 71
            echo ($context["open"] ?? null);
            echo "
            </div>
          </div>
        </div>    
    </div>
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  ";
            // line 78
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 79
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 79) ==  -1)) {
                    // line 80
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 81
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 81)) {
                            // line 82
                            echo "                <li class=\"nav-item dropdown\">
                  <a href=\"";
                            // line 83
                            echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 83);
                            echo "\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 83);
                            echo "</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    ";
                            // line 85
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 85), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 85)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 85), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 86
                                echo "                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 87
                                    echo "                        <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 87);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 87);
                                    echo "</a>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 89
                                echo "                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 90
                            echo "                  </div>
                </li>
                ";
                        } else {
                            // line 93
                            echo "                <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 93);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 93);
                            echo "</a></li>
                ";
                        }
                        // line 95
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 96
                    echo "            ";
                } else {
                    // line 97
                    echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 97);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 97);
                    echo "</a>
                ";
                    // line 98
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 98)) {
                        // line 99
                        echo "                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 102
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 102));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 103
                            echo "                    <li>
                        ";
                            // line 104
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 104)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 105
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 105);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 105);
                                echo "</a>
                        ";
                            } else {
                                // line 107
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 107);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 107);
                                echo "</a>\t
                        ";
                            }
                            // line 109
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 109)) {
                                // line 110
                                echo "                    
                    <ul>
                    ";
                                // line 112
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 112));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 113
                                    echo "                    <li>
                        ";
                                    // line 114
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 114)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 115
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 115);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 115);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 117
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 117);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 117);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 119
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 121
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 124
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 126
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 130
                    echo "\t
                </li>\t
            ";
                }
                // line 132
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 134
            echo "      </ul>
    </div>
  </nav>
";
        }
        // line 137
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/extension/tmdheader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  443 => 137,  437 => 134,  430 => 132,  425 => 130,  418 => 126,  411 => 124,  406 => 121,  399 => 119,  391 => 117,  384 => 115,  380 => 114,  377 => 113,  373 => 112,  369 => 110,  366 => 109,  358 => 107,  351 => 105,  347 => 104,  344 => 103,  340 => 102,  335 => 99,  333 => 98,  326 => 97,  323 => 96,  317 => 95,  309 => 93,  304 => 90,  298 => 89,  287 => 87,  282 => 86,  278 => 85,  271 => 83,  268 => 82,  265 => 81,  260 => 80,  258 => 79,  252 => 78,  242 => 71,  236 => 68,  231 => 65,  224 => 63,  219 => 61,  212 => 57,  205 => 55,  200 => 52,  193 => 50,  185 => 48,  178 => 46,  174 => 45,  171 => 44,  167 => 43,  163 => 41,  160 => 40,  152 => 38,  145 => 36,  141 => 35,  138 => 34,  134 => 33,  129 => 30,  127 => 29,  120 => 28,  117 => 27,  111 => 26,  103 => 24,  98 => 21,  92 => 20,  81 => 18,  76 => 17,  72 => 16,  67 => 14,  64 => 13,  61 => 12,  56 => 11,  54 => 10,  48 => 9,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if categories %}
  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"mobile-sidebar-overlay collapse\" id=\"pageSubmenu\">
        <div class=\"sidebar-container py-3\">
          <ul class=\"sidebar-navigation\">
          {% for header in headermenu %} 
                {% if header.column == -1 %}
                    {% for category in categories %}
                    {% if category.children %}
                    <li class=\"nav-item dropdown\">
                      <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                      <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                        {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                            {% for child in children %}
                            <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                            {% endfor %}
                          {% endfor %}
                      </div>
                    </li>
                    {% else %}
                    <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                    {% endif %}
                    {% endfor %}
                {% else %}
                    <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                    {% if header.sub_title %}
                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                        <div class=\"dropdown-inner\">
                        <ul class=\"list-unstyled\">
                        {% for subtitle in header.sub_title %}
                        <li>
                            {% if subtitle.href %}\t\t\t\t
                            <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                            {% else %}
                            <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                            {% endif %}
                            {% if header.sub_title %}
                        
                        <ul>
                        {% for subtitle in subtitle.sub_title %}
                        <li>
                            {% if subtitle.href %}\t\t\t\t\t\t
                            <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                            {% else %}
                            <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                            {% endif %}
                    </li>
                        {% endfor %}
                            
                        </ul>\t\t\t\t
                        {% endif %}
                    </li>
                        {% endfor %}
                        </ul>\t\t\t\t
                        </div>
                        </div>
                        
                        {% endif %}\t
                    </li>\t
                {% endif %}                
                {% endfor %}
          </ul>
          <div class=\"mobile-contacts ps-4\">
            <div class=\"mobile-contact-phone mb-2\">
                <img src=\"/image/catalog/icons/phone-call-white.svg\"> {{ telephone }}
            </div>
            <div class=\"mobile-contact-phone\">
                <img src=\"/image/catalog/icons/clock-white.svg\"> {{ open }}
            </div>
          </div>
        </div>    
    </div>
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  {% for header in headermenu %} 
            {% if header.column == -1 %}
                {% for category in categories %}
                {% if category.children %}
                <li class=\"nav-item dropdown\">
                  <a href=\"{{ header.link }}\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                        {% for child in children %}
                        <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                        {% endfor %}
                      {% endfor %}
                  </div>
                </li>
                {% else %}
                <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
            {% else %}
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                {% if header.sub_title %}
                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    {% for subtitle in header.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                        {% if header.sub_title %}
                    
                    <ul>
                    {% for subtitle in subtitle.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                </li>
                    {% endfor %}
                        
                    </ul>\t\t\t\t
                    {% endif %}
                </li>
                    {% endfor %}
                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    {% endif %}\t
                </li>\t
            {% endif %}                
\t\t\t{% endfor %}
      </ul>
    </div>
  </nav>
{% endif %} ", "default/template/extension/tmdheader.twig", "");
    }
}
