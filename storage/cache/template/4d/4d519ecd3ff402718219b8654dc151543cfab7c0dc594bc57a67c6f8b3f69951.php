<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/tmdheader.twig */
class __TwigTemplate_d980d25d97d7834c1c7c68c0c7038af8e7cfc0a984c308869a607a5dc80a1365 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "+++
";
        // line 2
        if (($context["categories"] ?? null)) {
            // line 3
            echo "<div class=\"container\">
  <nav id=\"menu\" class=\"navbar\">
    <div class=\"navbar-header\"><span id=\"category\" class=\"visible-xs\">";
            // line 5
            echo ($context["text_category"] ?? null);
            echo "</span>
      <button type=\"button\" class=\"btn btn-navbar navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\"><i class=\"fa fa-bars\"></i></button>
    </div>
    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
      <ul class=\"nav navbar-nav\">
\t  ";
            // line 10
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
\t\t\t<li class=\"dropdown\"><a class=\"dropdown-toggle\" href=\"";
                // line 11
                echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 11);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 11);
                echo "</a>
\t\t\t";
                // line 12
                if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 12)) {
                    // line 13
                    echo "\t\t\t\t<div class=\"dropdown-menu\">\t
\t\t\t\t<div class=\"dropdown-inner\">
\t\t\t\t<ul class=\"list-unstyled\">
\t\t\t\t";
                    // line 16
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 16));
                    foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                        // line 17
                        echo "\t\t\t\t<li>
\t\t\t\t\t";
                        // line 18
                        if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 18)) {
                            echo "\t\t\t\t
\t\t\t\t\t<a href=\"";
                            // line 19
                            echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 19);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 19);
                            echo "</a>
\t\t\t\t\t";
                        } else {
                            // line 21
                            echo "\t\t\t\t\t<a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 21);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 21);
                            echo "</a>\t
\t\t\t\t\t";
                        }
                        // line 23
                        echo "\t\t\t\t\t";
                        if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 23)) {
                            // line 24
                            echo "\t\t\t\t
\t\t\t\t<ul>
\t\t\t\t";
                            // line 26
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 26));
                            foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                // line 27
                                echo "\t\t\t\t<li>
\t\t\t\t\t";
                                // line 28
                                if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 28)) {
                                    echo "\t\t\t\t\t\t
\t\t\t\t\t<a href=\"";
                                    // line 29
                                    echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 29);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 29);
                                    echo "</a>
\t\t\t\t\t";
                                } else {
                                    // line 31
                                    echo "\t\t\t\t\t<a href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 31);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 31);
                                    echo "</a>\t
\t\t\t\t\t";
                                }
                                // line 33
                                echo "\t\t\t</li>
\t\t\t\t";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 35
                            echo "\t\t\t\t\t
\t\t\t\t</ul>\t\t\t\t
\t\t\t\t";
                        }
                        // line 38
                        echo "\t\t\t</li>
\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 40
                    echo "\t\t\t\t</ul>\t\t\t\t
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t";
                }
                // line 44
                echo "\t
\t\t\t</li>\t\t\t
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 48
                echo "        ";
                if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 48)) {
                    // line 49
                    echo "        <li class=\"dropdown\"><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 49);
                    echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 49);
                    echo "</a>
          <div class=\"dropdown-menu\">
            <div class=\"dropdown-inner\"> ";
                    // line 51
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 51), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 51)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 51), 1, "ceil"))));
                    foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                        // line 52
                        echo "              <ul class=\"list-unstyled\">
                ";
                        // line 53
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["children"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            // line 54
                            echo "                <li><a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 54);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 54);
                            echo "</a></li>
                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 56
                        echo "              </ul>
              ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 57
                    echo "</div>
            <a href=\"";
                    // line 58
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 58);
                    echo "\" class=\"see-all\">";
                    echo ($context["text_all"] ?? null);
                    echo " ";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 58);
                    echo "</a> </div>
        </li>
        ";
                } else {
                    // line 61
                    echo "        <li><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 61);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 61);
                    echo "</a></li>
        ";
                }
                // line 63
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "      </ul>
    </div>
  </nav>
</div>
";
        }
        // line 68
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/extension/tmdheader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 68,  238 => 64,  232 => 63,  224 => 61,  214 => 58,  211 => 57,  204 => 56,  193 => 54,  189 => 53,  186 => 52,  182 => 51,  174 => 49,  171 => 48,  166 => 47,  158 => 44,  151 => 40,  144 => 38,  139 => 35,  132 => 33,  124 => 31,  117 => 29,  113 => 28,  110 => 27,  106 => 26,  102 => 24,  99 => 23,  91 => 21,  84 => 19,  80 => 18,  77 => 17,  73 => 16,  68 => 13,  66 => 12,  60 => 11,  54 => 10,  46 => 5,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/tmdheader.twig", "");
    }
}
