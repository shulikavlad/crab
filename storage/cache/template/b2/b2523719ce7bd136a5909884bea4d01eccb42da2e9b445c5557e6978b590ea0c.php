<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/tmdheader.twig */
class __TwigTemplate_87cc0479a3633c6fe9448b84b3dbdf92fe10215e1c64081472321f8c82ad8a55 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "<div class=\"container\">
  <nav id=\"menu\" class=\"navbar\">
    <div class=\"navbar-header\"><span id=\"category\" class=\"visible-xs\">";
            // line 4
            echo ($context["text_category"] ?? null);
            echo "</span>
      <button type=\"button\" class=\"btn btn-navbar navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\"><i class=\"fa fa-bars\"></i></button>
    </div>
    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
      <ul class=\"nav navbar-nav\">
\t  ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
\t\t\t<li class=\"dropdown\"><a class=\"dropdown-toggle\" href=\"";
                // line 10
                echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 10);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 10);
                echo "</a>
\t\t\t";
                // line 11
                if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 11)) {
                    // line 12
                    echo "\t\t\t\t<div class=\"dropdown-menu\">\t
\t\t\t\t<div class=\"dropdown-inner\">
\t\t\t\t<ul class=\"list-unstyled\">
\t\t\t\t";
                    // line 15
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 15));
                    foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                        // line 16
                        echo "\t\t\t\t<li>
\t\t\t\t\t";
                        // line 17
                        if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 17)) {
                            echo "\t\t\t\t
\t\t\t\t\t<a href=\"";
                            // line 18
                            echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 18);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 18);
                            echo "</a>
\t\t\t\t\t";
                        } else {
                            // line 20
                            echo "\t\t\t\t\t<a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 20);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 20);
                            echo "</a>\t
\t\t\t\t\t";
                        }
                        // line 22
                        echo "\t\t\t\t\t";
                        if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 22)) {
                            // line 23
                            echo "\t\t\t\t
\t\t\t\t<ul>
\t\t\t\t";
                            // line 25
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 25));
                            foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                // line 26
                                echo "\t\t\t\t<li>
\t\t\t\t\t";
                                // line 27
                                if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 27)) {
                                    echo "\t\t\t\t\t\t
\t\t\t\t\t<a href=\"";
                                    // line 28
                                    echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 28);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 28);
                                    echo "</a>
\t\t\t\t\t";
                                } else {
                                    // line 30
                                    echo "\t\t\t\t\t<a href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 30);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 30);
                                    echo "</a>\t
\t\t\t\t\t";
                                }
                                // line 32
                                echo "\t\t\t</li>
\t\t\t\t";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 34
                            echo "\t\t\t\t\t
\t\t\t\t</ul>\t\t\t\t
\t\t\t\t";
                        }
                        // line 37
                        echo "\t\t\t</li>
\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 39
                    echo "\t\t\t\t</ul>\t\t\t\t
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t";
                }
                // line 43
                echo "\t
\t\t\t</li>\t\t\t
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 47
                echo "        ";
                if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 47)) {
                    // line 48
                    echo "        <li class=\"dropdown\"><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 48);
                    echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 48);
                    echo "</a>
          <div class=\"dropdown-menu\">
            <div class=\"dropdown-inner\"> ";
                    // line 50
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 50), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 50)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 50), 1, "ceil"))));
                    foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                        // line 51
                        echo "              <ul class=\"list-unstyled\">
                ";
                        // line 52
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["children"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            // line 53
                            echo "                <li><a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 53);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 53);
                            echo "</a></li>
                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 55
                        echo "              </ul>
              ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 56
                    echo "</div>
            <a href=\"";
                    // line 57
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 57);
                    echo "\" class=\"see-all\">";
                    echo ($context["text_all"] ?? null);
                    echo " ";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 57);
                    echo "</a> </div>
        </li>
        ";
                } else {
                    // line 60
                    echo "        <li><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 60);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 60);
                    echo "</a></li>
        ";
                }
                // line 62
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 63
            echo "      </ul>
    </div>
  </nav>
</div>
";
        }
        // line 67
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/extension/tmdheader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 67,  235 => 63,  229 => 62,  221 => 60,  211 => 57,  208 => 56,  201 => 55,  190 => 53,  186 => 52,  183 => 51,  179 => 50,  171 => 48,  168 => 47,  163 => 46,  155 => 43,  148 => 39,  141 => 37,  136 => 34,  129 => 32,  121 => 30,  114 => 28,  110 => 27,  107 => 26,  103 => 25,  99 => 23,  96 => 22,  88 => 20,  81 => 18,  77 => 17,  74 => 16,  70 => 15,  65 => 12,  63 => 11,  57 => 10,  51 => 9,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/tmdheader.twig", "");
    }
}
