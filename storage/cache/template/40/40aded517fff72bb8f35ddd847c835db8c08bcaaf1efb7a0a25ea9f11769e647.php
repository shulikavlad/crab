<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/home.twig */
class __TwigTemplate_91f4d2401917a5f2d39e38cc50f946af9d97d0499b11fe329a4a28b648aed0d3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"common-home\" class=\"container\">
  <div class=\"row\">";
        // line 3
        echo ($context["column_left"] ?? null);
        echo "
    ";
        // line 4
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 5
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 6
            echo "    ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 7
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 8
            echo "    ";
        } else {
            // line 9
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 10
            echo "    ";
        }
        // line 11
        echo "    <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 12
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["locations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
            // line 15
            echo "        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">
            <h4 class=\"panel-title\"><a href=\"#collapse-location";
            // line 17
            echo twig_get_attribute($this->env, $this->source, $context["location"], "location_id", [], "any", false, false, false, 17);
            echo "\" class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\">";
            echo twig_get_attribute($this->env, $this->source, $context["location"], "name", [], "any", false, false, false, 17);
            echo " <i class=\"fa fa-caret-down\"></i></a></h4>
          </div>
          <div class=\"panel-collapse collapse\" id=\"collapse-location";
            // line 19
            echo twig_get_attribute($this->env, $this->source, $context["location"], "location_id", [], "any", false, false, false, 19);
            echo "\">
            <div class=\"panel-body\">
              <div class=\"row\">
                ";
            // line 22
            if (twig_get_attribute($this->env, $this->source, $context["location"], "image", [], "any", false, false, false, 22)) {
                // line 23
                echo "                <div class=\"col-sm-3\"><img src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["location"], "image", [], "any", false, false, false, 23);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["location"], "name", [], "any", false, false, false, 23);
                echo "\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["location"], "name", [], "any", false, false, false, 23);
                echo "\" class=\"img-thumbnail\" /></div>
                ";
            }
            // line 25
            echo "                <div class=\"col-sm-3\"><strong>";
            echo twig_get_attribute($this->env, $this->source, $context["location"], "name", [], "any", false, false, false, 25);
            echo "</strong><br/>
                  <address>
                  ";
            // line 27
            echo twig_get_attribute($this->env, $this->source, $context["location"], "address", [], "any", false, false, false, 27);
            echo "
                  </address>
                  ";
            // line 29
            if (twig_get_attribute($this->env, $this->source, $context["location"], "geocode", [], "any", false, false, false, 29)) {
                // line 30
                echo "                  <a href=\"https://maps.google.com/maps?q=";
                echo twig_urlencode_filter(twig_get_attribute($this->env, $this->source, $context["location"], "geocode", [], "any", false, false, false, 30));
                echo "&hl=";
                echo ($context["geocode_hl"] ?? null);
                echo "&t=m&z=15\" target=\"_blank\" class=\"btn btn-info\"><i class=\"fa fa-map-marker\"></i> ";
                echo ($context["button_map"] ?? null);
                echo "</a>
                  ";
            }
            // line 32
            echo "                </div>
                <div class=\"col-sm-3\"> <strong>";
            // line 33
            echo ($context["text_telephone"] ?? null);
            echo "</strong><br/>
                  ";
            // line 34
            echo twig_get_attribute($this->env, $this->source, $context["location"], "telephone", [], "any", false, false, false, 34);
            echo "<br/>
                  <br/>
                  ";
            // line 36
            if (twig_get_attribute($this->env, $this->source, $context["location"], "fax", [], "any", false, false, false, 36)) {
                // line 37
                echo "                  <strong>";
                echo ($context["text_fax"] ?? null);
                echo "</strong><br/>
                  ";
                // line 38
                echo twig_get_attribute($this->env, $this->source, $context["location"], "fax", [], "any", false, false, false, 38);
                echo "
                  ";
            }
            // line 40
            echo "                </div>
                <div class=\"col-sm-3\">
                  ";
            // line 42
            if (twig_get_attribute($this->env, $this->source, $context["location"], "open", [], "any", false, false, false, 42)) {
                // line 43
                echo "                  <strong>";
                echo ($context["text_open"] ?? null);
                echo "</strong><br/>
                  ";
                // line 44
                echo twig_get_attribute($this->env, $this->source, $context["location"], "open", [], "any", false, false, false, 44);
                echo "<br/>
                  <br/>
                  ";
            }
            // line 47
            echo "                  ";
            if (twig_get_attribute($this->env, $this->source, $context["location"], "comment", [], "any", false, false, false, 47)) {
                // line 48
                echo "                  <strong>";
                echo ($context["text_comment"] ?? null);
                echo "</strong><br/>
                  ";
                // line 49
                echo twig_get_attribute($this->env, $this->source, $context["location"], "comment", [], "any", false, false, false, 49);
                echo "
                  ";
            }
            // line 51
            echo "                </div>
              </div>
            </div>
          </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "<section id=\"stores\">
\t<div class=\"store-wrap\">
\t\t<div class=\"store-card-wrap\">
\t\t\t<div class=\"store-card-inner\">
\t\t\t\t<div class=\"store-card-item\" data-attr=\"0\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store1.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">г. Одесса, Генуэзская 3Б</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>10:00-22:00</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:+380936121212\">+38 (093) 612-12-12</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"store-card-item\" data-attr=\"1\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store2.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">г. Одесса, Книжный рынок</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>9:00-21:00</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:+380936121212\">+38 (093) 612-12-12</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"store-card-item\" data-attr=\"2\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store3.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">г. Одесса, Жемчужная 5Б</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>10:00-22:00</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:+380936121212\">+38 (093) 612-12-12</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"store-map-wrap\" id=\"stores-map\">
\t
\t\t</div>
\t</div>
</section>
<section id=\"contactForm\" class=\"contactForm-section\">
\t<div class=\"container cf-container\">
\t\t<form class=\"cF-home\">
\t\t\t<h3>Напишите нам</h3>
\t\t\t<hr>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"fio\" class=\"form-label\">ФИО<span>*</span>:
\t\t\t\t\t</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"fio\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label for=\"cF-email\" class=\"form-label\">Email:</label>
\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"cF-email\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label class=\"form-check-label\" for=\"cF-phone\">Номер Телефона<span>*</span>:</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"cF-phone\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"cf-textarea\" class=\"form-label\">Сообщение<span>*</span>:</label>
\t\t\t\t\t<textarea class=\"form-control\" id=\"cf-textarea\" rows=\"6\" required></textarea>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<button type=\"submit\" class=\"cf-button\">Отправить</button>
\t\t</form>
\t</div>

</section>
";
        // line 139
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/common/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  284 => 139,  200 => 57,  189 => 51,  184 => 49,  179 => 48,  176 => 47,  170 => 44,  165 => 43,  163 => 42,  159 => 40,  154 => 38,  149 => 37,  147 => 36,  142 => 34,  138 => 33,  135 => 32,  125 => 30,  123 => 29,  118 => 27,  112 => 25,  102 => 23,  100 => 22,  94 => 19,  87 => 17,  83 => 15,  79 => 14,  74 => 12,  66 => 11,  63 => 10,  60 => 9,  57 => 8,  54 => 7,  51 => 6,  48 => 5,  46 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/common/home.twig", "");
    }
}
