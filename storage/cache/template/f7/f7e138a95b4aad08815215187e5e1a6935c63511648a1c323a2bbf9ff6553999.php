<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/home.twig */
class __TwigTemplate_cd7057cacb6d1ad0bf036f00ace46da837bd36e83e71e229693eb31040a56096 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"common-home\" class=\"container\">
  <div class=\"row\">";
        // line 3
        echo ($context["column_left"] ?? null);
        echo "
    ";
        // line 4
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 5
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 6
            echo "    ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 7
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 8
            echo "    ";
        } else {
            // line 9
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 10
            echo "    ";
        }
        // line 11
        echo "    <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 12
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["locations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
            // line 15
            echo "\t\t\t\t<div class=\"store-card-item\" data-attr=\"0\" data-geo=\"";
            echo twig_get_attribute($this->env, $this->source, $context["location"], "geocode", [], "any", false, false, false, 15);
            echo "\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store1.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">";
            // line 20
            echo twig_get_attribute($this->env, $this->source, $context["location"], "address", [], "any", false, false, false, 20);
            echo "</p>
\t\t\t\t\t\t<p>";
            // line 21
            echo ($context["text_open"] ?? null);
            echo "
\t\t\t\t\t\t\t<span>";
            // line 22
            echo twig_get_attribute($this->env, $this->source, $context["location"], "open", [], "any", false, false, false, 22);
            echo "</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:";
            // line 25
            echo twig_get_attribute($this->env, $this->source, $context["location"], "telephone", [], "any", false, false, false, 25);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["location"], "telephone", [], "any", false, false, false, 25);
            echo "</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "<section id=\"stores\">
\t<div class=\"store-wrap\">
\t\t<div class=\"store-card-wrap\">
\t\t\t<div class=\"store-card-inner\">
\t\t\t\t<div class=\"store-card-item\" data-attr=\"0\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store1.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">г. Одесса, Генуэзская 3Б</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>10:00-22:00</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:+380936121212\">+38 (093) 612-12-12</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"store-card-item\" data-attr=\"1\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store2.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">г. Одесса, Книжный рынок</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>9:00-21:00</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:+380936121212\">+38 (093) 612-12-12</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"store-card-item\" data-attr=\"2\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"image/catalog/store3.jpg\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">г. Одесса, Жемчужная 5Б</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>10:00-22:00</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:+380936121212\">+38 (093) 612-12-12</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"store-map-wrap\" id=\"stores-map\">
\t
\t\t</div>
\t</div>
</section>
<section id=\"contactForm\" class=\"contactForm-section\">
\t<div class=\"container cf-container\">
\t\t<form class=\"cF-home\">
\t\t\t<h3>Напишите нам</h3>
\t\t\t<hr>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"fio\" class=\"form-label\">ФИО<span>*</span>:
\t\t\t\t\t</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"fio\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label for=\"cF-email\" class=\"form-label\">Email:</label>
\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"cF-email\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label class=\"form-check-label\" for=\"cF-phone\">Номер Телефона<span>*</span>:</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"cF-phone\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"cf-textarea\" class=\"form-label\">Сообщение<span>*</span>:</label>
\t\t\t\t\t<textarea class=\"form-control\" id=\"cf-textarea\" rows=\"6\" required></textarea>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<button type=\"submit\" class=\"cf-button\">Отправить</button>
\t\t</form>
\t</div>

</section>
";
        // line 112
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/common/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 112,  119 => 30,  106 => 25,  100 => 22,  96 => 21,  92 => 20,  83 => 15,  79 => 14,  74 => 12,  66 => 11,  63 => 10,  60 => 9,  57 => 8,  54 => 7,  51 => 6,  48 => 5,  46 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/common/home.twig", "");
    }
}
