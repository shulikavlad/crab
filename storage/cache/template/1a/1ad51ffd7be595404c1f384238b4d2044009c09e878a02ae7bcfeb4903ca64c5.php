<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/load_format_pagination.twig */
class __TwigTemplate_deed94e776b658c0e8f5fef779c56345298661101a19291f56cff9369955c00b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- Load Format Pagination 1.0.0 -->
<style>
.btn-load-pagination {
\tbackground-color: ";
        // line 4
        echo ($context["load_format_pagination_backgroundcolor"] ?? null);
        echo ";
\tborder-color: ";
        // line 5
        echo ($context["load_format_pagination_bordercolor"] ?? null);
        echo ";
\tcolor: ";
        // line 6
        echo ($context["load_format_pagination_buttoncolor"] ?? null);
        echo ";
\tborder-radius: ";
        // line 7
        echo ($context["load_format_pagination_borderround"] ?? null);
        echo ";
\tborder: ";
        // line 8
        echo ($context["load_format_pagination_borderwidth"] ?? null);
        echo "px solid ";
        echo ($context["load_format_pagination_bordercolor"] ?? null);
        echo ";
}

.btn-load-pagination:hover {
    background-color: ";
        // line 12
        echo ($context["load_format_pagination_hover_backgroundcolor"] ?? null);
        echo ";
\tcolor: ";
        // line 13
        echo ($context["load_format_pagination_hover_buttoncolor"] ?? null);
        echo ";
}
</style>";
    }

    public function getTemplateName()
    {
        return "default/template/common/load_format_pagination.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 13,  67 => 12,  58 => 8,  54 => 7,  50 => 6,  46 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- Load Format Pagination 1.0.0 -->
<style>
.btn-load-pagination {
\tbackground-color: {{ load_format_pagination_backgroundcolor }};
\tborder-color: {{ load_format_pagination_bordercolor }};
\tcolor: {{ load_format_pagination_buttoncolor }};
\tborder-radius: {{ load_format_pagination_borderround }};
\tborder: {{ load_format_pagination_borderwidth }}px solid {{ load_format_pagination_bordercolor }};
}

.btn-load-pagination:hover {
    background-color: {{ load_format_pagination_hover_backgroundcolor }};
\tcolor: {{ load_format_pagination_hover_buttoncolor }};
}
</style>", "default/template/common/load_format_pagination.twig", "");
    }
}
