<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/home.twig */
class __TwigTemplate_bc54701f86330d692516b9aa441d5908e1054d242a853c8603ed075d0d97d8e3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"common-home\" >
  <div class=\"row\">";
        // line 3
        echo ($context["column_left"] ?? null);
        echo "
    ";
        // line 4
        if ((($context["column_left"] ?? null) && ($context["column_right"] ?? null))) {
            // line 5
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 6
            echo "    ";
        } elseif ((($context["column_left"] ?? null) || ($context["column_right"] ?? null))) {
            // line 7
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 8
            echo "    ";
        } else {
            // line 9
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 10
            echo "    ";
        }
        // line 11
        echo "    <div id=\"content\" class=\"";
        echo ($context["class"] ?? null);
        echo "\">";
        echo ($context["content_top"] ?? null);
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 12
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
<section id=\"stores\">
\t<div class=\"store-wrap\">
\t\t<div class=\"store-card-wrap\">
\t\t\t<div class=\"store-card-inner\">
        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["locations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
            // line 19
            echo "            <div>
\t\t\t\t<div class=\"store-card-item\" data-attr=\"";
            // line 20
            echo (twig_get_attribute($this->env, $this->source, $context["location"], "location_id", [], "any", false, false, false, 20) - 1);
            echo "\" data-geo=\"";
            echo twig_get_attribute($this->env, $this->source, $context["location"], "geocode", [], "any", false, false, false, 20);
            echo "\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"";
            // line 22
            echo twig_get_attribute($this->env, $this->source, $context["location"], "image", [], "any", false, false, false, 22);
            echo "\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">";
            // line 25
            echo twig_get_attribute($this->env, $this->source, $context["location"], "address", [], "any", false, false, false, 25);
            echo "</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>";
            // line 27
            echo twig_get_attribute($this->env, $this->source, $context["location"], "open", [], "any", false, false, false, 27);
            echo "</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:";
            // line 30
            echo twig_get_attribute($this->env, $this->source, $context["location"], "telephone", [], "any", false, false, false, 30);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["location"], "telephone", [], "any", false, false, false, 30);
            echo "</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "\t\t\t</div>
\t\t</div>
\t\t<div class=\"store-map-wrap\" id=\"stores-map\">
\t
\t\t</div>
\t</div>
</section>
<section id=\"contactForm\" class=\"contactForm-section\">
\t<div class=\"container cf-container\">
\t\t<form class=\"cF-home\">
\t\t\t<h3>Напишите нам</h3>
\t\t\t<hr>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"fio\" class=\"form-label\">ФИО<span>*</span>:
\t\t\t\t\t</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"fio\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label for=\"cF-email\" class=\"form-label\">Email:</label>
\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"cF-email\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label class=\"form-check-label\" for=\"cF-phone\">Номер Телефона<span>*</span>:</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"cF-phone\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"cf-textarea\" class=\"form-label\">Сообщение<span>*</span>:</label>
\t\t\t\t\t<textarea class=\"form-control\" id=\"cf-textarea\" rows=\"6\" required></textarea>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<button type=\"submit\" class=\"cf-button\">Отправить</button>
\t\t</form>
\t</div>

</section>
";
        // line 72
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "default/template/common/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 72,  128 => 36,  114 => 30,  108 => 27,  103 => 25,  97 => 22,  90 => 20,  87 => 19,  83 => 18,  74 => 12,  66 => 11,  63 => 10,  60 => 9,  57 => 8,  54 => 7,  51 => 6,  48 => 5,  46 => 4,  42 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}
<div id=\"common-home\" >
  <div class=\"row\">{{ column_left }}
    {% if column_left and column_right %}
    {% set class = 'col-sm-6' %}
    {% elseif column_left or column_right %}
    {% set class = 'col-sm-9' %}
    {% else %}
    {% set class = 'col-sm-12' %}
    {% endif %}
    <div id=\"content\" class=\"{{ class }}\">{{ content_top }}{{ content_bottom }}</div>
    {{ column_right }}</div>
</div>
<section id=\"stores\">
\t<div class=\"store-wrap\">
\t\t<div class=\"store-card-wrap\">
\t\t\t<div class=\"store-card-inner\">
        {% for location in locations %}
            <div>
\t\t\t\t<div class=\"store-card-item\" data-attr=\"{{ location.location_id - 1 }}\" data-geo=\"{{ location.geocode }}\">
\t\t\t\t\t<div class=\"store-card-image\">
\t\t\t\t\t\t<img src=\"{{ location.image }}\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"store-card-text\">
\t\t\t\t\t\t<p class=\"store-card-adress\">{{ location.address }}</p>
\t\t\t\t\t\t<p>Время работы:
\t\t\t\t\t\t\t<span>{{ location.open }}</span>
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<p>тел:
\t\t\t\t\t\t\t<a href=\"tel:{{ location.telephone }}\">{{ location.telephone }}</a>
\t\t\t\t\t\t</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
            </div>
        {% endfor %}
\t\t\t</div>
\t\t</div>
\t\t<div class=\"store-map-wrap\" id=\"stores-map\">
\t
\t\t</div>
\t</div>
</section>
<section id=\"contactForm\" class=\"contactForm-section\">
\t<div class=\"container cf-container\">
\t\t<form class=\"cF-home\">
\t\t\t<h3>Напишите нам</h3>
\t\t\t<hr>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"fio\" class=\"form-label\">ФИО<span>*</span>:
\t\t\t\t\t</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"fio\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label for=\"cF-email\" class=\"form-label\">Email:</label>
\t\t\t\t\t<input type=\"email\" class=\"form-control\" id=\"cF-email\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 cf-group\">
\t\t\t\t\t<label class=\"form-check-label\" for=\"cF-phone\">Номер Телефона<span>*</span>:</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"cF-phone\" required>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-12 cf-group\">
\t\t\t\t\t<label for=\"cf-textarea\" class=\"form-label\">Сообщение<span>*</span>:</label>
\t\t\t\t\t<textarea class=\"form-control\" id=\"cf-textarea\" rows=\"6\" required></textarea>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<button type=\"submit\" class=\"cf-button\">Отправить</button>
\t\t</form>
\t</div>

</section>
{{ footer }}", "default/template/common/home.twig", "");
    }
}
