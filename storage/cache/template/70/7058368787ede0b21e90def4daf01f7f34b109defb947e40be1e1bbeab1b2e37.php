<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/module/load_format_pagination.twig */
class __TwigTemplate_442504eab809e0237ca61433d33b44e9de024cf1638c540482821965987e3f6e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a class=\"btn btn-success\" onclick=\"\$('#save').val('stay');\$('#form-load-format-pagination').submit();\"><i class=\"fa fa-check\"></i> ";
        // line 6
        echo ($context["button_stay"] ?? null);
        echo "</a>
        <button type=\"submit\" form=\"form-load-format-pagination\" data-toggle=\"tooltip\" title=\"";
        // line 7
        echo ($context["button_save"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 8
        echo ($context["cancel"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_cancel"] ?? null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 9
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 12
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 12);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 12);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 18
        if (($context["error_warning"] ?? null)) {
            // line 19
            echo "    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 23
        echo "    ";
        if (($context["success"] ?? null)) {
            // line 24
            echo "      <div class=\"alert alert-success\"><i class=\"fa fa-check\"></i> ";
            echo ($context["success"] ?? null);
            echo "
          <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
      </div>
    ";
        }
        // line 28
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 30
        echo ($context["text_edit"] ?? null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 33
        echo ($context["action"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-load-format-pagination\" class=\"form-horizontal\">
          <input type=\"hidden\" name=\"save\" id=\"save\" value=\"0\">

            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-name\">";
        // line 37
        echo ($context["entry_name"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"load_format_pagination_name\" value=\"";
        // line 39
        echo ($context["load_format_pagination_name"] ?? null);
        echo "\" placeholder=\"";
        echo ($context["entry_name"] ?? null);
        echo "\" id=\"input-name\" class=\"form-control\" />
                ";
        // line 40
        if (($context["error_name"] ?? null)) {
            // line 41
            echo "                <div class=\"text-danger\">";
            echo ($context["error_name"] ?? null);
            echo "</div>
                ";
        }
        // line 43
        echo "              </div>
            </div>

            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-width\">";
        // line 47
        echo ($context["border_style"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <div class=\"row\">
                  <label class=\"col-sm-2 control-label\" for=\"input-borderwidth\">";
        // line 50
        echo ($context["input_borderwidth"] ?? null);
        echo "</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_borderwidth\" value=\"";
        // line 52
        echo ($context["load_format_pagination_borderwidth"] ?? null);
        echo "\"  id=\"load-pagination-borderwidth\" size=\"6\" class=\"form-control\" placeholder=\"";
        echo ($context["text_borderwidth"] ?? null);
        echo "\"/>
                  </div>
                  <label class=\"col-sm-2 control-label\" for=\"input-borderround\">";
        // line 54
        echo ($context["input_borderround"] ?? null);
        echo "</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_borderround\" value=\"";
        // line 56
        echo ($context["load_format_pagination_borderround"] ?? null);
        echo "\" id=\"load-pagination-borderround\" size=\"6\" class=\"form-control\" placeholder=\"";
        echo ($context["text_borderround"] ?? null);
        echo "\" />
                  </div>
                  <label class=\"col-sm-2 control-label\" for=\"input-bordercolor\">";
        // line 58
        echo ($context["input_bordercolor"] ?? null);
        echo "</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_bordercolor\" value=\"";
        // line 60
        echo ($context["load_format_pagination_bordercolor"] ?? null);
        echo "\"  id=\"load-pagination-bordercolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                  </div>
                </div>
              </div>
            </div>

            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-width\">";
        // line 67
        echo ($context["button_style"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <div class=\"row\">
                  <label class=\"col-sm-2 control-label\" for=\"input-borderwidth\">";
        // line 70
        echo ($context["input_buttoncolor"] ?? null);
        echo "</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_buttoncolor\" value=\"";
        // line 72
        echo ($context["load_format_pagination_buttoncolor"] ?? null);
        echo "\"  id=\"load-pagination-buttoncolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                  </div>
                  <label class=\"col-sm-2 control-label\" for=\"input-borderwidth\">";
        // line 74
        echo ($context["input_button_backgroundcolor"] ?? null);
        echo "</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_backgroundcolor\" value=\"";
        // line 76
        echo ($context["load_format_pagination_backgroundcolor"] ?? null);
        echo "\"  id=\"load-pagination-backgroundcolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                  </div>
                  <div class=\"col-sm-offset-4\">
                </div>
              </div>
            </div>
          </div>

            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-width\">";
        // line 85
        echo ($context["hover_button_style"] ?? null);
        echo "</label>
              <div class=\"col-sm-10\">
                <div class=\"row\">
                  <label class=\"col-sm-2 control-label\" for=\"load-pagination-buttoncolor\">";
        // line 88
        echo ($context["hover_input_buttoncolor"] ?? null);
        echo "</label>
                      <div class=\"col-sm-2\">
                        <input type=\"text\" name=\"load_format_pagination_hover_buttoncolor\" value=\"";
        // line 90
        echo ($context["load_format_pagination_hover_buttoncolor"] ?? null);
        echo "\"  id=\"load-pagination-buttoncolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                      </div>
                      <label class=\"col-sm-2 control-label\" for=\"load-pagination-backgroundcolor\">";
        // line 92
        echo ($context["hover_input_button_backgroundcolor"] ?? null);
        echo "</label>
                      <div class=\"col-sm-2\">
                        <input type=\"text\" name=\"load_format_pagination_hover_backgroundcolor\" value=\"";
        // line 94
        echo ($context["load_format_pagination_hover_backgroundcolor"] ?? null);
        echo "\"  id=\"load-pagination-backgroundcolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                      </div>
                      <div class=\"col-sm-offset-4\">
                    </div>
                </div>
              </div>
            </div>


            <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 104
        echo ($context["entry_status"] ?? null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"load_format_pagination_status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 107
        if (($context["load_format_pagination_status"] ?? null)) {
            // line 108
            echo "                <option value=\"1\" selected=\"selected\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\">";
            // line 109
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        } else {
            // line 111
            echo "                <option value=\"1\">";
            echo ($context["text_enabled"] ?? null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 112
            echo ($context["text_disabled"] ?? null);
            echo "</option>
                ";
        }
        // line 114
        echo "              </select>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\" src=\"view/javascript/jscolor.js\"></script>
</div>
";
        // line 124
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/module/load_format_pagination.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  301 => 124,  289 => 114,  284 => 112,  279 => 111,  274 => 109,  269 => 108,  267 => 107,  261 => 104,  248 => 94,  243 => 92,  238 => 90,  233 => 88,  227 => 85,  215 => 76,  210 => 74,  205 => 72,  200 => 70,  194 => 67,  184 => 60,  179 => 58,  172 => 56,  167 => 54,  160 => 52,  155 => 50,  149 => 47,  143 => 43,  137 => 41,  135 => 40,  129 => 39,  124 => 37,  117 => 33,  111 => 30,  107 => 28,  99 => 24,  96 => 23,  88 => 19,  86 => 18,  80 => 14,  69 => 12,  65 => 11,  60 => 9,  54 => 8,  50 => 7,  46 => 6,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}{{ column_left }}
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a class=\"btn btn-success\" onclick=\"\$('#save').val('stay');\$('#form-load-format-pagination').submit();\"><i class=\"fa fa-check\"></i> {{ button_stay }}</a>
        <button type=\"submit\" form=\"form-load-format-pagination\" data-toggle=\"tooltip\" title=\"{{ button_save }}\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"{{ cancel }}\" data-toggle=\"tooltip\" title=\"{{ button_cancel }}\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>{{ heading_title }}</h1>
      <ul class=\"breadcrumb\">
        {% for breadcrumb in breadcrumbs %}
        <li><a href=\"{{ breadcrumb.href }}\">{{ breadcrumb.text }}</a></li>
        {% endfor %}
      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    {% if error_warning %}
    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> {{ error_warning }}
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    {% endif %}
    {% if success %}
      <div class=\"alert alert-success\"><i class=\"fa fa-check\"></i> {{ success }}
          <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
      </div>
    {% endif %}
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> {{ text_edit }}</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"{{ action }}\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-load-format-pagination\" class=\"form-horizontal\">
          <input type=\"hidden\" name=\"save\" id=\"save\" value=\"0\">

            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-name\">{{ entry_name }}</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"load_format_pagination_name\" value=\"{{ load_format_pagination_name }}\" placeholder=\"{{ entry_name }}\" id=\"input-name\" class=\"form-control\" />
                {% if error_name %}
                <div class=\"text-danger\">{{ error_name }}</div>
                {% endif %}
              </div>
            </div>

            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-width\">{{ border_style }}</label>
              <div class=\"col-sm-10\">
                <div class=\"row\">
                  <label class=\"col-sm-2 control-label\" for=\"input-borderwidth\">{{ input_borderwidth }}</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_borderwidth\" value=\"{{ load_format_pagination_borderwidth }}\"  id=\"load-pagination-borderwidth\" size=\"6\" class=\"form-control\" placeholder=\"{{ text_borderwidth }}\"/>
                  </div>
                  <label class=\"col-sm-2 control-label\" for=\"input-borderround\">{{ input_borderround }}</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_borderround\" value=\"{{ load_format_pagination_borderround }}\" id=\"load-pagination-borderround\" size=\"6\" class=\"form-control\" placeholder=\"{{ text_borderround }}\" />
                  </div>
                  <label class=\"col-sm-2 control-label\" for=\"input-bordercolor\">{{ input_bordercolor }}</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_bordercolor\" value=\"{{ load_format_pagination_bordercolor }}\"  id=\"load-pagination-bordercolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                  </div>
                </div>
              </div>
            </div>

            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-width\">{{ button_style }}</label>
              <div class=\"col-sm-10\">
                <div class=\"row\">
                  <label class=\"col-sm-2 control-label\" for=\"input-borderwidth\">{{ input_buttoncolor }}</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_buttoncolor\" value=\"{{ load_format_pagination_buttoncolor }}\"  id=\"load-pagination-buttoncolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                  </div>
                  <label class=\"col-sm-2 control-label\" for=\"input-borderwidth\">{{ input_button_backgroundcolor }}</label>
                  <div class=\"col-sm-2\">
                    <input type=\"text\" name=\"load_format_pagination_backgroundcolor\" value=\"{{ load_format_pagination_backgroundcolor }}\"  id=\"load-pagination-backgroundcolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                  </div>
                  <div class=\"col-sm-offset-4\">
                </div>
              </div>
            </div>
          </div>

            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-width\">{{ hover_button_style }}</label>
              <div class=\"col-sm-10\">
                <div class=\"row\">
                  <label class=\"col-sm-2 control-label\" for=\"load-pagination-buttoncolor\">{{ hover_input_buttoncolor }}</label>
                      <div class=\"col-sm-2\">
                        <input type=\"text\" name=\"load_format_pagination_hover_buttoncolor\" value=\"{{ load_format_pagination_hover_buttoncolor }}\"  id=\"load-pagination-buttoncolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                      </div>
                      <label class=\"col-sm-2 control-label\" for=\"load-pagination-backgroundcolor\">{{ hover_input_button_backgroundcolor }}</label>
                      <div class=\"col-sm-2\">
                        <input type=\"text\" name=\"load_format_pagination_hover_backgroundcolor\" value=\"{{ load_format_pagination_hover_backgroundcolor }}\"  id=\"load-pagination-backgroundcolor\" size=\"6\" class=\"jscolor {required:false,hash:true} form-control\"  />
                      </div>
                      <div class=\"col-sm-offset-4\">
                    </div>
                </div>
              </div>
            </div>


            <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">{{ entry_status }}</label>
            <div class=\"col-sm-10\">
              <select name=\"load_format_pagination_status\" id=\"input-status\" class=\"form-control\">
                {% if load_format_pagination_status %}
                <option value=\"1\" selected=\"selected\">{{ text_enabled }}</option>
                <option value=\"0\">{{ text_disabled }}</option>
                {% else %}
                <option value=\"1\">{{ text_enabled }}</option>
                <option value=\"0\" selected=\"selected\">{{ text_disabled }}</option>
                {% endif %}
              </select>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\" src=\"view/javascript/jscolor.js\"></script>
</div>
{{ footer }}", "extension/module/load_format_pagination.twig", "");
    }
}
