<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/tmdheader.twig */
class __TwigTemplate_4efbb17fd07ebe6e06791a529d6c787e38b95b928dcdc7a20fb2cfb92cc9e01c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"sidebar-container\" id=\"pageSubmenu\">
      <div class=\"sidebar-logo\">
        Project Name
      </div>
      <ul class=\"sidebar-navigation\">
        <li class=\"header\">Navigation</li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-home\" aria-hidden=\"true\"></i> Homepage
          </a>
        </li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-tachometer\" aria-hidden=\"true\"></i> Dashboard
          </a>
        </li>
        <li class=\"header\">Another Menu</li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-users\" aria-hidden=\"true\"></i> Friends
          </a>
        </li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-cog\" aria-hidden=\"true\"></i> Settings
          </a>
        </li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i> Information
          </a>
        </li>
      </ul>
    </div>    
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  ";
            // line 42
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 43
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 43) ==  -1)) {
                    // line 44
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 45
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 45)) {
                            // line 46
                            echo "                <li class=\"nav-item dropdown\">
                  <a href=\"";
                            // line 47
                            echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 47);
                            echo "\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 47);
                            echo "</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    ";
                            // line 49
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 49), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 49)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 49), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 50
                                echo "                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 51
                                    echo "                        <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 51);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 51);
                                    echo "</a>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 53
                                echo "                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 54
                            echo "                  </div>
                </li>
                ";
                        } else {
                            // line 57
                            echo "                <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 57);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 57);
                            echo "</a></li>
                ";
                        }
                        // line 59
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 60
                    echo "            ";
                } else {
                    // line 61
                    echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 61);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 61);
                    echo "</a>
                ";
                    // line 62
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 62)) {
                        // line 63
                        echo "                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 66
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 66));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 67
                            echo "                    <li>
                        ";
                            // line 68
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 68)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 69
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 69);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 69);
                                echo "</a>
                        ";
                            } else {
                                // line 71
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 71);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 71);
                                echo "</a>\t
                        ";
                            }
                            // line 73
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 73)) {
                                // line 74
                                echo "                    
                    <ul>
                    ";
                                // line 76
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 76));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 77
                                    echo "                    <li>
                        ";
                                    // line 78
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 78)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 79
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 79);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 79);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 81
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 81);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 81);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 83
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 85
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 88
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 90
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 94
                    echo "\t
                </li>\t
            ";
                }
                // line 96
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 98
            echo "      </ul>
    </div>
  </nav>
";
        }
        // line 101
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/extension/tmdheader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  272 => 101,  266 => 98,  259 => 96,  254 => 94,  247 => 90,  240 => 88,  235 => 85,  228 => 83,  220 => 81,  213 => 79,  209 => 78,  206 => 77,  202 => 76,  198 => 74,  195 => 73,  187 => 71,  180 => 69,  176 => 68,  173 => 67,  169 => 66,  164 => 63,  162 => 62,  155 => 61,  152 => 60,  146 => 59,  138 => 57,  133 => 54,  127 => 53,  116 => 51,  111 => 50,  107 => 49,  100 => 47,  97 => 46,  94 => 45,  89 => 44,  87 => 43,  81 => 42,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if categories %}
  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"sidebar-container\" id=\"pageSubmenu\">
      <div class=\"sidebar-logo\">
        Project Name
      </div>
      <ul class=\"sidebar-navigation\">
        <li class=\"header\">Navigation</li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-home\" aria-hidden=\"true\"></i> Homepage
          </a>
        </li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-tachometer\" aria-hidden=\"true\"></i> Dashboard
          </a>
        </li>
        <li class=\"header\">Another Menu</li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-users\" aria-hidden=\"true\"></i> Friends
          </a>
        </li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-cog\" aria-hidden=\"true\"></i> Settings
          </a>
        </li>
        <li>
          <a href=\"#\">
            <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i> Information
          </a>
        </li>
      </ul>
    </div>    
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  {% for header in headermenu %} 
            {% if header.column == -1 %}
                {% for category in categories %}
                {% if category.children %}
                <li class=\"nav-item dropdown\">
                  <a href=\"{{ header.link }}\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                        {% for child in children %}
                        <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                        {% endfor %}
                      {% endfor %}
                  </div>
                </li>
                {% else %}
                <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
            {% else %}
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                {% if header.sub_title %}
                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    {% for subtitle in header.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                        {% if header.sub_title %}
                    
                    <ul>
                    {% for subtitle in subtitle.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                </li>
                    {% endfor %}
                        
                    </ul>\t\t\t\t
                    {% endif %}
                </li>
                    {% endfor %}
                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    {% endif %}\t
                </li>\t
            {% endif %}                
\t\t\t{% endfor %}
      </ul>
    </div>
  </nav>
{% endif %} ", "default/template/extension/tmdheader.twig", "");
    }
}
