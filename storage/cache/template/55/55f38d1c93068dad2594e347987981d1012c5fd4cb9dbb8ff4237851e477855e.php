<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/product/category.twig */
class __TwigTemplate_d2b99e4f3623dfa39d589beb53fb06e357a729589d7a6b3ec4c2f0daea70cf9c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"product-category\" class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 5);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 5);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
  <div class=\"row\">
    <div id=\"content\" class=\"col-12\">";
        // line 9
        echo ($context["content_top"] ?? null);
        echo "
      <h1>";
        // line 10
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      ";
        // line 11
        if ((($context["thumb"] ?? null) || ($context["description"] ?? null))) {
            // line 12
            echo "      <div class=\"row\"> ";
            if (($context["thumb"] ?? null)) {
                // line 13
                echo "        <div class=\"col-sm-2\"><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" title=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" class=\"img-thumbnail\" /></div>
        ";
            }
            // line 15
            echo "        ";
            if (($context["description"] ?? null)) {
                // line 16
                echo "        <div class=\"col-sm-10\">";
                echo ($context["description"] ?? null);
                echo "</div>
        ";
            }
            // line 17
            echo "</div>
      <hr>
      ";
        }
        // line 20
        echo "      ";
        if (($context["categories"] ?? null)) {
            // line 21
            echo "      <h3>";
            echo ($context["text_refine"] ?? null);
            echo "</h3>
      ";
            // line 22
            if ((twig_length_filter($this->env, ($context["categories"] ?? null)) <= 5)) {
                // line 23
                echo "      <div class=\"row\">
        <div class=\"col-sm-3\">
          <ul>
            ";
                // line 26
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 27
                    echo "            <li><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 27);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 27);
                    echo "</a></li>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 29
                echo "          </ul>
        </div>
      </div>
      ";
            } else {
                // line 33
                echo "      <div class=\"row\">";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_array_batch(($context["categories"] ?? null), twig_round((twig_length_filter($this->env, ($context["categories"] ?? null)) / 4), 1, "ceil")));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 34
                    echo "        <div class=\"col-sm-3\">
          <ul>
            ";
                    // line 36
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["category"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 37
                        echo "            <li><a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 37);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 37);
                        echo "</a></li>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 39
                    echo "          </ul>
        </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 41
                echo "</div>
      <br/>
      ";
            }
            // line 44
            echo "      ";
        }
        echo " 
      ";
        // line 45
        if (($context["products"] ?? null)) {
            // line 46
            echo "        <div class=\"row\">
      ";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 48
                echo "        <div class=\"product-layout product-grid col-md-4\">
          <div class=\"product-thumb\">
            <div class=\"image\"><a href=\"";
                // line 50
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 50);
                echo "\"><img src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 50);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 50);
                echo "\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 50);
                echo "\" class=\"img-responsive\" /></a></div>
            <div class=\"product-card-info\">
              <div class=\"row product-card-topinfo\">
                <div class=\"col-7 product-card-name\">
                    <a href=\"";
                // line 54
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 54);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 54);
                echo "</a>
                </div>
                <div class=\"col-5 product-card-price text-md-right\"> 
                ";
                // line 57
                if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 57)) {
                    // line 58
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 58)) {
                        // line 59
                        echo "                  ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 59);
                        echo "
                  ";
                    } else {
                        // line 60
                        echo " <span class=\"price-new\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 60);
                        echo "</span> <span class=\"price-old\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 60);
                        echo "</span> ";
                    }
                    // line 61
                    echo "                  ";
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 61)) {
                        echo " <span class=\"price-tax\">";
                        echo ($context["text_tax"] ?? null);
                        echo " ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 61);
                        echo "</span> ";
                    }
                    // line 62
                    echo "                ";
                }
                echo "/кг
                </div>
              </div>
              <div class=\"product-card-desc\">";
                // line 65
                echo twig_get_attribute($this->env, $this->source, $context["product"], "description", [], "any", false, false, false, 65);
                echo "</div>
              <div class=\"button-group\">
                <button type=\"button\" onclick=\"cart.add('";
                // line 67
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 67);
                echo "', '";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 67);
                echo "');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
                echo ($context["button_cart"] ?? null);
                echo "</span></button>
              </div>
            </div>
          </div>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 72
            echo " </div>
      <div class=\"load-more-section\">

        <!-- Load Format Pagination -->
        ";
            // line 76
            if ((($context["limit"] ?? null) < ($context["ttl"] ?? null))) {
                // line 77
                echo "        <div class=\"row row-pagination-container\">
          <div class=\"col-sm-12 pagination-text-justify\">
            <form action=\"post\">
              <button class=\"btn-load-pagination\" id=\"custom-pagination-button\"><div class=\"title-button\">";
                // line 80
                echo ($context["load_more"] ?? null);
                echo " (<span id=\"number-products\">";
                echo ($context["config_catalog_limit"] ?? null);
                echo "</span> ";
                echo ($context["show_product"] ?? null);
                echo " ";
                echo ($context["ttl"] ?? null);
                echo ")</div><div class=\"load-pagination\">
                  <div class=\"ring-pagination\"></div>
              </div></button>
              
              <input type=\"hidden\" name=\"load-more-botton\" value=\"";
                // line 84
                echo ($context["config_catalog_limit"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-category\" value=\"";
                // line 85
                echo ($context["category_data"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-urlcategory\" value=\"";
                // line 86
                echo ($context["url_category"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-path\" value=\"";
                // line 87
                echo ($context["path"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-sort\" value=\"";
                // line 88
                echo ($context["sort"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-filter\" value=\"";
                // line 89
                echo ($context["filter"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-order\" value=\"";
                // line 90
                echo ($context["order"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-totalproducts\" value=\"";
                // line 91
                echo ($context["ttl"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-step\" id=\"more-step\" value=\"";
                // line 92
                echo ($context["page"] ?? null);
                echo "\">
            </form>
          </div>
        </div>
        ";
            }
            // line 97
            echo "      
        <div class=\"col-sm-6 text-left\">";
            // line 98
            echo ($context["pagination"] ?? null);
            echo "</div>
      </div>      
      ";
        }
        // line 101
        echo "      ";
        if (( !($context["categories"] ?? null) &&  !($context["products"] ?? null))) {
            // line 102
            echo "      <p>";
            echo ($context["text_empty"] ?? null);
            echo "</p>
      ";
        }
        // line 104
        echo "      ";
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 105
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
";
        // line 107
        echo ($context["footer"] ?? null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "default/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  360 => 107,  355 => 105,  350 => 104,  344 => 102,  341 => 101,  335 => 98,  332 => 97,  324 => 92,  320 => 91,  316 => 90,  312 => 89,  308 => 88,  304 => 87,  300 => 86,  296 => 85,  292 => 84,  279 => 80,  274 => 77,  272 => 76,  266 => 72,  250 => 67,  245 => 65,  238 => 62,  229 => 61,  222 => 60,  216 => 59,  213 => 58,  211 => 57,  203 => 54,  190 => 50,  186 => 48,  182 => 47,  179 => 46,  177 => 45,  172 => 44,  167 => 41,  159 => 39,  148 => 37,  144 => 36,  140 => 34,  135 => 33,  129 => 29,  118 => 27,  114 => 26,  109 => 23,  107 => 22,  102 => 21,  99 => 20,  94 => 17,  88 => 16,  85 => 15,  75 => 13,  72 => 12,  70 => 11,  66 => 10,  62 => 9,  58 => 7,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}
<div id=\"product-category\" class=\"container\">
  <ul class=\"breadcrumb\">
    {% for breadcrumb in breadcrumbs %}
    <li><a href=\"{{ breadcrumb.href }}\">{{ breadcrumb.text }}</a></li>
    {% endfor %}
  </ul>
  <div class=\"row\">
    <div id=\"content\" class=\"col-12\">{{ content_top }}
      <h1>{{ heading_title }}</h1>
      {% if thumb or description %}
      <div class=\"row\"> {% if thumb %}
        <div class=\"col-sm-2\"><img src=\"{{ thumb }}\" alt=\"{{ heading_title }}\" title=\"{{ heading_title }}\" class=\"img-thumbnail\" /></div>
        {% endif %}
        {% if description %}
        <div class=\"col-sm-10\">{{ description }}</div>
        {% endif %}</div>
      <hr>
      {% endif %}
      {% if categories %}
      <h3>{{ text_refine }}</h3>
      {% if categories|length <= 5 %}
      <div class=\"row\">
        <div class=\"col-sm-3\">
          <ul>
            {% for category in categories %}
            <li><a href=\"{{ category.href }}\">{{ category.name }}</a></li>
            {% endfor %}
          </ul>
        </div>
      </div>
      {% else %}
      <div class=\"row\">{% for category in categories|batch((categories|length / 4)|round(1, 'ceil')) %}
        <div class=\"col-sm-3\">
          <ul>
            {% for child in category %}
            <li><a href=\"{{ child.href }}\">{{ child.name }}</a></li>
            {% endfor %}
          </ul>
        </div>
        {% endfor %}</div>
      <br/>
      {% endif %}
      {% endif %} 
      {% if products %}
        <div class=\"row\">
      {% for product in products %}
        <div class=\"product-layout product-grid col-md-4\">
          <div class=\"product-thumb\">
            <div class=\"image\"><a href=\"{{ product.href }}\"><img src=\"{{ product.thumb }}\" alt=\"{{ product.name }}\" title=\"{{ product.name }}\" class=\"img-responsive\" /></a></div>
            <div class=\"product-card-info\">
              <div class=\"row product-card-topinfo\">
                <div class=\"col-7 product-card-name\">
                    <a href=\"{{ product.href }}\">{{ product.name }}</a>
                </div>
                <div class=\"col-5 product-card-price text-md-right\"> 
                {% if product.price %}
                {% if not product.special %}
                  {{ product.price }}
                  {% else %} <span class=\"price-new\">{{ product.special }}</span> <span class=\"price-old\">{{ product.price }}</span> {% endif %}
                  {% if product.tax %} <span class=\"price-tax\">{{ text_tax }} {{ product.tax }}</span> {% endif %}
                {% endif %}/кг
                </div>
              </div>
              <div class=\"product-card-desc\">{{ product.description }}</div>
              <div class=\"button-group\">
                <button type=\"button\" onclick=\"cart.add('{{ product.product_id }}', '{{ product.minimum }}');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ button_cart }}</span></button>
              </div>
            </div>
          </div>
        </div>
        {% endfor %} </div>
      <div class=\"load-more-section\">

        <!-- Load Format Pagination -->
        {% if limit < ttl %}
        <div class=\"row row-pagination-container\">
          <div class=\"col-sm-12 pagination-text-justify\">
            <form action=\"post\">
              <button class=\"btn-load-pagination\" id=\"custom-pagination-button\"><div class=\"title-button\">{{ load_more }} (<span id=\"number-products\">{{ config_catalog_limit }}</span> {{ show_product }} {{ ttl }})</div><div class=\"load-pagination\">
                  <div class=\"ring-pagination\"></div>
              </div></button>
              
              <input type=\"hidden\" name=\"load-more-botton\" value=\"{{ config_catalog_limit }}\">
              <input type=\"hidden\" name=\"more-botton-category\" value=\"{{ category_data }}\">
              <input type=\"hidden\" name=\"more-botton-urlcategory\" value=\"{{ url_category }}\">
              <input type=\"hidden\" name=\"more-botton-path\" value=\"{{ path }}\">
              <input type=\"hidden\" name=\"more-botton-sort\" value=\"{{ sort }}\">
              <input type=\"hidden\" name=\"more-botton-filter\" value=\"{{ filter }}\">
              <input type=\"hidden\" name=\"more-botton-order\" value=\"{{ order }}\">
              <input type=\"hidden\" name=\"more-botton-totalproducts\" value=\"{{ ttl }}\">
              <input type=\"hidden\" name=\"more-botton-step\" id=\"more-step\" value=\"{{ page }}\">
            </form>
          </div>
        </div>
        {% endif %}
      
        <div class=\"col-sm-6 text-left\">{{ pagination }}</div>
      </div>      
      {% endif %}
      {% if not categories and not products %}
      <p>{{ text_empty }}</p>
      {% endif %}
      {{ content_bottom }}</div>
    {{ column_right }}</div>
</div>
{{ footer }} 
", "default/template/product/category.twig", "");
    }
}
