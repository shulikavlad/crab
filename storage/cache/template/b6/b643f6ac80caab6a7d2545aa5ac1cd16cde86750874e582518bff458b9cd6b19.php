<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/footer.twig */
class __TwigTemplate_10b7afc19a580d890d3e7dd4ecb3c17307b0d5f47ba5c2f975e011dbb096ef56 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<footer>
  <div class=\"container\">
    <div class=\"row\">
      ";
        // line 4
        if (($context["logo"] ?? null)) {
            // line 5
            echo "      <div class=\"col-sm-3\">
        <img src=\"";
            // line 6
            echo ($context["logo"] ?? null);
            echo "\" width=\"70\" height=\"70\">
      </div>
      ";
        }
        // line 9
        echo "      <div class=\"col-sm-3\">
            <ul class=\"navbar-nav\">
                <li><a href=\"/\">Домашняя</a></li>\t
                <li><a href=\"/about_us\">О нас</a></li>\t
                <li><a href=\"/produkciya\">Продукция</a></li>
                <li><a href=\"/contact\">Контакты</a></li>\t
                <li><a href=\"/optovikam\">Оптовикам</a></li>\t
            </ul>
      </div>
      <div class=\"col-sm-3\">
        <ul class=\"list-unstyled\">
          <li><a href=\"/sposoby-oplaty\">Способы оплаты</a></li>
          <li><a href=\"/obmen-i-vozvrat\">Обмен и возврат</a></li>
          <li><a href=\"/sotrudnichestvo-s-nami\">Сотрудничество с нами</a></li>
          <li><a href=\"/vakansii\">Вакансии</a></li>
        </ul>
      </div>
      <div class=\"col-sm-3\">
        <h5>";
        // line 27
        echo ($context["text_account"] ?? null);
        echo "</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"";
        // line 29
        echo ($context["account"] ?? null);
        echo "\">";
        echo ($context["text_account"] ?? null);
        echo "</a></li>
          <li><a href=\"";
        // line 30
        echo ($context["order"] ?? null);
        echo "\">";
        echo ($context["text_order"] ?? null);
        echo "</a></li>
          <li><a href=\"";
        // line 31
        echo ($context["wishlist"] ?? null);
        echo "\">";
        echo ($context["text_wishlist"] ?? null);
        echo "</a></li>
          <li><a href=\"";
        // line 32
        echo ($context["newsletter"] ?? null);
        echo "\">";
        echo ($context["text_newsletter"] ?? null);
        echo "</a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p>КОРОЛЕВСКИЙ КРАБ © 2022. ALL RIGHTS RESERVED.</p>
  </div>
</footer>
";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 41
            echo "<link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "href", [], "any", false, false, false, 41);
            echo "\" type=\"text/css\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "rel", [], "any", false, false, false, 41);
            echo "\" media=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "media", [], "any", false, false, false, 41);
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 44
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<script type=\"text/javascript\" src=\"/catalog/view/javascript/slick/slick.min.js\"></script>
<!--script src=\"catalog/view/javascript/bootstrap/js/bootstrap.bundle.min.js\" type=\"text/javascript\"></script-->


<script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js\" integrity=\"sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p\" crossorigin=\"anonymous\"></script>
<!--script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script-->
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.bundle.min.js\" type=\"text/javascript\"></script>

<script>
    \$('.store-card-inner').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        arrows: true,
        prevArrow: '<button class=\"slick-prev slick-arrow\" aria-label=\"Previous\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>',
        nextArrow: '<button class=\"slick-next slick-arrow\" aria-label=\"Next\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>'
    });
</script>
</body></html>";
    }

    public function getTemplateName()
    {
        return "default/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 46,  130 => 44,  126 => 43,  113 => 41,  109 => 40,  96 => 32,  90 => 31,  84 => 30,  78 => 29,  73 => 27,  53 => 9,  47 => 6,  44 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer>
  <div class=\"container\">
    <div class=\"row\">
      {% if logo %}
      <div class=\"col-sm-3\">
        <img src=\"{{ logo }}\" width=\"70\" height=\"70\">
      </div>
      {% endif %}
      <div class=\"col-sm-3\">
            <ul class=\"navbar-nav\">
                <li><a href=\"/\">Домашняя</a></li>\t
                <li><a href=\"/about_us\">О нас</a></li>\t
                <li><a href=\"/produkciya\">Продукция</a></li>
                <li><a href=\"/contact\">Контакты</a></li>\t
                <li><a href=\"/optovikam\">Оптовикам</a></li>\t
            </ul>
      </div>
      <div class=\"col-sm-3\">
        <ul class=\"list-unstyled\">
          <li><a href=\"/sposoby-oplaty\">Способы оплаты</a></li>
          <li><a href=\"/obmen-i-vozvrat\">Обмен и возврат</a></li>
          <li><a href=\"/sotrudnichestvo-s-nami\">Сотрудничество с нами</a></li>
          <li><a href=\"/vakansii\">Вакансии</a></li>
        </ul>
      </div>
      <div class=\"col-sm-3\">
        <h5>{{ text_account }}</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"{{ account }}\">{{ text_account }}</a></li>
          <li><a href=\"{{ order }}\">{{ text_order }}</a></li>
          <li><a href=\"{{ wishlist }}\">{{ text_wishlist }}</a></li>
          <li><a href=\"{{ newsletter }}\">{{ text_newsletter }}</a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p>КОРОЛЕВСКИЙ КРАБ © 2022. ALL RIGHTS RESERVED.</p>
  </div>
</footer>
{% for style in styles %}
<link href=\"{{ style.href }}\" type=\"text/css\" rel=\"{{ style.rel }}\" media=\"{{ style.media }}\" />
{% endfor %}
{% for script in scripts %}
<script src=\"{{ script }}\" type=\"text/javascript\"></script>
{% endfor %}
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<script type=\"text/javascript\" src=\"/catalog/view/javascript/slick/slick.min.js\"></script>
<!--script src=\"catalog/view/javascript/bootstrap/js/bootstrap.bundle.min.js\" type=\"text/javascript\"></script-->


<script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js\" integrity=\"sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p\" crossorigin=\"anonymous\"></script>
<!--script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script-->
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.bundle.min.js\" type=\"text/javascript\"></script>

<script>
    \$('.store-card-inner').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        arrows: true,
        prevArrow: '<button class=\"slick-prev slick-arrow\" aria-label=\"Previous\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>',
        nextArrow: '<button class=\"slick-next slick-arrow\" aria-label=\"Next\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>'
    });
</script>
</body></html>", "default/template/common/footer.twig", "");
    }
}
