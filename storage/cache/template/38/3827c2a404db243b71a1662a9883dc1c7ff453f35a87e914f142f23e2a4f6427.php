<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/tmdheader.twig */
class __TwigTemplate_fbe4e2fd3ed41aa402077e071dffefd31829aa77035b3b691c3a1a7ca29b6198 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-";
            // line 3
            if (((($context["home_url"] ?? null) == "/") || (($context["home_url"] ?? null) == "/index.php?route=common/home"))) {
                echo "light";
            } else {
                echo "dark";
            }
            echo "\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>   
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 9
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 9) ==  -1)) {
                    // line 10
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 11
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 11)) {
                            // line 12
                            echo "                <li class=\"nav-item dropdown\">
                  <a href=\"";
                            // line 13
                            echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 13);
                            echo "\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 13);
                            echo "</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    ";
                            // line 15
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 15), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 15)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 15), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 16
                                echo "                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 17
                                    echo "                        <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 17);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 17);
                                    echo "</a>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 19
                                echo "                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 20
                            echo "                  </div>
                </li>
                ";
                        } else {
                            // line 23
                            echo "                <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 23);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 23);
                            echo "</a></li>
                ";
                        }
                        // line 25
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 26
                    echo "            ";
                } else {
                    // line 27
                    echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 27);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 27);
                    echo "</a>
                ";
                    // line 28
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 28)) {
                        // line 29
                        echo "                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 32
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 32));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 33
                            echo "                    <li>
                        ";
                            // line 34
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 34)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 35
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 35);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 35);
                                echo "</a>
                        ";
                            } else {
                                // line 37
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 37);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 37);
                                echo "</a>\t
                        ";
                            }
                            // line 39
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 39)) {
                                // line 40
                                echo "                    
                    <ul>
                    ";
                                // line 42
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 42));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 43
                                    echo "                    <li>
                        ";
                                    // line 44
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 44)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 45
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 45);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 45);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 47
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 47);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 47);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 49
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 51
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 54
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 56
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 60
                    echo "\t
                </li>\t
            ";
                }
                // line 62
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "      </ul>
    </div>
  </nav>
        <div class=\"sidebar-container collapse py-3\" id=\"pageSubmenu\">
          <ul class=\"sidebar-navigation\">
          ";
            // line 69
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
                ";
                // line 70
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 70) ==  -1)) {
                    // line 71
                    echo "                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 72
                        echo "                    ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 72)) {
                            // line 73
                            echo "                    <li class=\"nav-item dropdown\">
                      <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            // line 74
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 74);
                            echo "</a>
                      <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                        ";
                            // line 76
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 76), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 76)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 76), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 77
                                echo "                            ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 78
                                    echo "                            <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 78);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 78);
                                    echo "</a>
                            ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 80
                                echo "                          ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 81
                            echo "                      </div>
                    </li>
                    ";
                        } else {
                            // line 84
                            echo "                    <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 84);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 84);
                            echo "</a></li>
                    ";
                        }
                        // line 86
                        echo "                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 87
                    echo "                ";
                } else {
                    // line 88
                    echo "                    <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 88);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 88);
                    echo "</a>
                    ";
                    // line 89
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 89)) {
                        // line 90
                        echo "                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                        <div class=\"dropdown-inner\">
                        <ul class=\"list-unstyled\">
                        ";
                        // line 93
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 93));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 94
                            echo "                        <li>
                            ";
                            // line 95
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 95)) {
                                echo "\t\t\t\t
                            <a href=\"";
                                // line 96
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 96);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 96);
                                echo "</a>
                            ";
                            } else {
                                // line 98
                                echo "                            <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 98);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 98);
                                echo "</a>\t
                            ";
                            }
                            // line 100
                            echo "                            ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 100)) {
                                // line 101
                                echo "                        
                        <ul>
                        ";
                                // line 103
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 103));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 104
                                    echo "                        <li>
                            ";
                                    // line 105
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 105)) {
                                        echo "\t\t\t\t\t\t
                            <a href=\"";
                                        // line 106
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 106);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 106);
                                        echo "</a>
                            ";
                                    } else {
                                        // line 108
                                        echo "                            <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 108);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 108);
                                        echo "</a>\t
                            ";
                                    }
                                    // line 110
                                    echo "                    </li>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 112
                                echo "                            
                        </ul>\t\t\t\t
                        ";
                            }
                            // line 115
                            echo "                    </li>
                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 117
                        echo "                        </ul>\t\t\t\t
                        </div>
                        </div>
                        
                        ";
                    }
                    // line 121
                    echo "\t
                    </li>\t
                ";
                }
                // line 123
                echo "                
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 125
            echo "          </ul>
          <div class=\"mobile-contacts ps-4\">
            <div class=\"mobile-contact-phone mb-2\">
                <img src=\"/image/catalog/icons/phone-call-white.svg\"> ";
            // line 128
            echo ($context["telephone"] ?? null);
            echo "
            </div>
            <div class=\"mobile-contact-phone\">
                <img src=\"/image/catalog/icons/clock-white.svg\"> ";
            // line 131
            echo ($context["open"] ?? null);
            echo "
            </div>
          </div>
        </div>   
";
        }
        // line 135
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/extension/tmdheader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  448 => 135,  440 => 131,  434 => 128,  429 => 125,  422 => 123,  417 => 121,  410 => 117,  403 => 115,  398 => 112,  391 => 110,  383 => 108,  376 => 106,  372 => 105,  369 => 104,  365 => 103,  361 => 101,  358 => 100,  350 => 98,  343 => 96,  339 => 95,  336 => 94,  332 => 93,  327 => 90,  325 => 89,  318 => 88,  315 => 87,  309 => 86,  301 => 84,  296 => 81,  290 => 80,  279 => 78,  274 => 77,  270 => 76,  265 => 74,  262 => 73,  259 => 72,  254 => 71,  252 => 70,  246 => 69,  239 => 64,  232 => 62,  227 => 60,  220 => 56,  213 => 54,  208 => 51,  201 => 49,  193 => 47,  186 => 45,  182 => 44,  179 => 43,  175 => 42,  171 => 40,  168 => 39,  160 => 37,  153 => 35,  149 => 34,  146 => 33,  142 => 32,  137 => 29,  135 => 28,  128 => 27,  125 => 26,  119 => 25,  111 => 23,  106 => 20,  100 => 19,  89 => 17,  84 => 16,  80 => 15,  73 => 13,  70 => 12,  67 => 11,  62 => 10,  60 => 9,  54 => 8,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if categories %}
  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-{% if (home_url == '/' or home_url == '/index.php?route=common/home') %}light{% else %}dark{% endif %}\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>   
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  {% for header in headermenu %} 
            {% if header.column == -1 %}
                {% for category in categories %}
                {% if category.children %}
                <li class=\"nav-item dropdown\">
                  <a href=\"{{ header.link }}\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                        {% for child in children %}
                        <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                        {% endfor %}
                      {% endfor %}
                  </div>
                </li>
                {% else %}
                <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
            {% else %}
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                {% if header.sub_title %}
                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    {% for subtitle in header.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                        {% if header.sub_title %}
                    
                    <ul>
                    {% for subtitle in subtitle.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                </li>
                    {% endfor %}
                        
                    </ul>\t\t\t\t
                    {% endif %}
                </li>
                    {% endfor %}
                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    {% endif %}\t
                </li>\t
            {% endif %}                
\t\t\t{% endfor %}
      </ul>
    </div>
  </nav>
        <div class=\"sidebar-container collapse py-3\" id=\"pageSubmenu\">
          <ul class=\"sidebar-navigation\">
          {% for header in headermenu %} 
                {% if header.column == -1 %}
                    {% for category in categories %}
                    {% if category.children %}
                    <li class=\"nav-item dropdown\">
                      <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                      <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                        {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                            {% for child in children %}
                            <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                            {% endfor %}
                          {% endfor %}
                      </div>
                    </li>
                    {% else %}
                    <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                    {% endif %}
                    {% endfor %}
                {% else %}
                    <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                    {% if header.sub_title %}
                        <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                        <div class=\"dropdown-inner\">
                        <ul class=\"list-unstyled\">
                        {% for subtitle in header.sub_title %}
                        <li>
                            {% if subtitle.href %}\t\t\t\t
                            <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                            {% else %}
                            <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                            {% endif %}
                            {% if header.sub_title %}
                        
                        <ul>
                        {% for subtitle in subtitle.sub_title %}
                        <li>
                            {% if subtitle.href %}\t\t\t\t\t\t
                            <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                            {% else %}
                            <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                            {% endif %}
                    </li>
                        {% endfor %}
                            
                        </ul>\t\t\t\t
                        {% endif %}
                    </li>
                        {% endfor %}
                        </ul>\t\t\t\t
                        </div>
                        </div>
                        
                        {% endif %}\t
                    </li>\t
                {% endif %}                
                {% endfor %}
          </ul>
          <div class=\"mobile-contacts ps-4\">
            <div class=\"mobile-contact-phone mb-2\">
                <img src=\"/image/catalog/icons/phone-call-white.svg\"> {{ telephone }}
            </div>
            <div class=\"mobile-contact-phone\">
                <img src=\"/image/catalog/icons/clock-white.svg\"> {{ open }}
            </div>
          </div>
        </div>   
{% endif %} ", "default/template/extension/tmdheader.twig", "");
    }
}
