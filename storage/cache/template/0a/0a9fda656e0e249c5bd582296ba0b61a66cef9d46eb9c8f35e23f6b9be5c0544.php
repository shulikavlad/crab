<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/product/category.twig */
class __TwigTemplate_bcb2ddb954cb9f585ff081baba28539871e637f34a8e7d376e5f04491be4b6c3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"product-category\" class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 5);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 5);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
  <div class=\"row\">
    <div id=\"content\" class=\"col-12\">";
        // line 9
        echo ($context["content_top"] ?? null);
        echo "
      <h1>";
        // line 10
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      ";
        // line 11
        if ((($context["thumb"] ?? null) || ($context["description"] ?? null))) {
            // line 12
            echo "      <div class=\"row\"> ";
            if (($context["thumb"] ?? null)) {
                // line 13
                echo "        <div class=\"col-sm-2\"><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" title=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" class=\"img-thumbnail\" /></div>
        ";
            }
            // line 15
            echo "        ";
            if (($context["description"] ?? null)) {
                // line 16
                echo "        <div class=\"col-sm-10\">";
                echo ($context["description"] ?? null);
                echo "</div>
        ";
            }
            // line 17
            echo "</div>
      <hr>
      ";
        }
        // line 20
        echo "      ";
        if (($context["categories"] ?? null)) {
            // line 21
            echo "      <h3>";
            echo ($context["text_refine"] ?? null);
            echo "</h3>
      ";
            // line 22
            if ((twig_length_filter($this->env, ($context["categories"] ?? null)) <= 5)) {
                // line 23
                echo "      <div class=\"row\">
        <div class=\"col-sm-3\">
          <ul>
            ";
                // line 26
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 27
                    echo "            <li><a href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 27);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 27);
                    echo "</a></li>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 29
                echo "          </ul>
        </div>
      </div>
      ";
            } else {
                // line 33
                echo "      <div class=\"row\">";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_array_batch(($context["categories"] ?? null), twig_round((twig_length_filter($this->env, ($context["categories"] ?? null)) / 4), 1, "ceil")));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 34
                    echo "        <div class=\"col-sm-3\">
          <ul>
            ";
                    // line 36
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["category"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 37
                        echo "            <li><a href=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 37);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 37);
                        echo "</a></li>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 39
                    echo "          </ul>
        </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 41
                echo "</div>
      <br/>
      ";
            }
            // line 44
            echo "      ";
        }
        echo " 
      <div class=\"row\">
      <div class=\"col-12\">
      ";
        // line 47
        if (($context["products"] ?? null)) {
            // line 48
            echo "      
        <div class=\"row\" id=\"load-format-pagination\">
      
      ";
            // line 51
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 52
                echo "        <div class=\"product-layout product-grid col-md-4\">
          <div class=\"product-thumb\">
            <div class=\"image\"><a href=\"";
                // line 54
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 54);
                echo "\"><img src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 54);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 54);
                echo "\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 54);
                echo "\" class=\"img-responsive\" /></a></div>
            <div class=\"product-card-info\">
              <div class=\"row product-card-topinfo\">
                <div class=\"col-7 product-card-name\">
                    <a href=\"";
                // line 58
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 58);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 58);
                echo "</a>
                </div>
                <div class=\"col-5 product-card-price text-md-right\"> 
                ";
                // line 61
                if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 61)) {
                    // line 62
                    echo "                ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 62)) {
                        // line 63
                        echo "                  ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 63);
                        echo "
                  ";
                    } else {
                        // line 64
                        echo " <span class=\"price-new\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 64);
                        echo "</span> <span class=\"price-old\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 64);
                        echo "</span> ";
                    }
                    // line 65
                    echo "                  ";
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 65)) {
                        echo " <span class=\"price-tax\">";
                        echo ($context["text_tax"] ?? null);
                        echo " ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 65);
                        echo "</span> ";
                    }
                    // line 66
                    echo "                ";
                }
                echo "/кг
                </div>
              </div>
              <div class=\"product-card-desc\">";
                // line 69
                echo twig_get_attribute($this->env, $this->source, $context["product"], "description", [], "any", false, false, false, 69);
                echo "</div>
              <div class=\"product-params\">
                <div class=\"col-12 form-group d-flex align-items-center justify-content-between\">
                  <label class=\"control-label me-2\" for=\"input-quantity\">Количество</label>
                    <div class=\"input-qty\">
                        <div class=\"input-qty-btn minus\">-</div>
                        <input type=\"text\" class=\"input-qty-field\" name=\"quantity\" value=\"1\" size=\"2\" id=\"input-quantity\">
                        <div class=\"input-qty-btn plus\">+</div>
                    </div>
                  <input type=\"hidden\" name=\"product_id\" value=\"";
                // line 78
                echo ($context["product_id"] ?? null);
                echo "\" />
                </div>
          ";
                // line 80
                if (twig_get_attribute($this->env, $this->source, $context["product"], "options", [], "any", false, false, false, 80)) {
                    // line 81
                    echo "            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["product"], "options", [], "any", false, false, false, 81));
                    foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                        // line 82
                        echo "            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 82) == "select")) {
                            // line 83
                            echo "            <div class=\"col-12 form-group d-flex align-items-center justify-content-between ";
                            if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 83)) {
                                echo " required ";
                            }
                            echo "\">
              <label class=\"control-label\" for=\"input-option";
                            // line 84
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 84);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 84);
                            echo "</label>
              <select name=\"option[";
                            // line 85
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 85);
                            echo "]\" id=\"input-option";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 85);
                            echo "\" class=\"form-control\">
                ";
                            // line 86
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 86));
                            foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                                // line 87
                                echo "                <option value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 87);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 87);
                                echo "</option>
                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 89
                            echo "              </select>
            </div>
            ";
                        }
                        // line 92
                        echo "            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 92) == "radio")) {
                            // line 93
                            echo "            <div class=\"col-12 form-group d-flex align-items-center justify-content-between";
                            if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 93)) {
                                echo " required ";
                            }
                            echo "\">
              <label class=\"control-label\">";
                            // line 94
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 94);
                            echo "</label>
              <div id=\"input-option";
                            // line 95
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 95);
                            echo "\"> ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 95));
                            foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                                // line 96
                                echo "                <div class=\"radio\">
                  <label>
                    <input type=\"radio\" name=\"option[";
                                // line 98
                                echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 98);
                                echo "]\" value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 98);
                                echo "\" />
                    ";
                                // line 99
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 99)) {
                                    echo " <img src=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 99);
                                    echo "\" alt=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 99);
                                    echo " ";
                                    if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 99)) {
                                        echo " ";
                                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 99);
                                        echo " ";
                                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 99);
                                        echo " ";
                                    }
                                    echo "\" class=\"img-thumbnail\" /> ";
                                }
                                echo "                  
                    ";
                                // line 100
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 100);
                                echo "
                    ";
                                // line 101
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 101)) {
                                    // line 102
                                    echo "                    (";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 102);
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 102);
                                    echo ")
                    ";
                                }
                                // line 103
                                echo " </label>
                </div>
                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 105
                            echo " </div>
            </div>
            ";
                        }
                        // line 108
                        echo "            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 108) == "checkbox")) {
                            // line 109
                            echo "            <div class=\"col-12 form-group d-flex align-items-center justify-content-between";
                            if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 109)) {
                                echo " required ";
                            }
                            echo "\">
              <label class=\"control-label\">";
                            // line 110
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 110);
                            echo "</label>
              <div id=\"input-option";
                            // line 111
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 111);
                            echo "\"> ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 111));
                            foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                                // line 112
                                echo "                <div class=\"checkbox\">
                  <label>
                    <input type=\"checkbox\" name=\"option[";
                                // line 114
                                echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 114);
                                echo "][]\" value=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 114);
                                echo "\" />
                    ";
                                // line 115
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 115)) {
                                    echo " <img src=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 115);
                                    echo "\" alt=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 115);
                                    echo " ";
                                    if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 115)) {
                                        echo " ";
                                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 115);
                                        echo " ";
                                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 115);
                                        echo " ";
                                    }
                                    echo "\" class=\"img-thumbnail\" /> ";
                                }
                                // line 116
                                echo "                    ";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 116);
                                echo "
                    ";
                                // line 117
                                if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 117)) {
                                    // line 118
                                    echo "                    (";
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 118);
                                    echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 118);
                                    echo ")
                    ";
                                }
                                // line 119
                                echo " </label>
                </div>
                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 121
                            echo " </div>
            </div>
            ";
                        }
                        // line 124
                        echo "            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 124) == "text")) {
                            // line 125
                            echo "            <div class=\"col-12 form-group d-flex align-items-center justify-content-between";
                            if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 125)) {
                                echo " required ";
                            }
                            echo "\">
              <label class=\"control-label\" for=\"input-option";
                            // line 126
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 126);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 126);
                            echo "</label>
              <input type=\"text\" name=\"option[";
                            // line 127
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 127);
                            echo "]\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 127);
                            echo "\" placeholder=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 127);
                            echo "\" id=\"input-option";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 127);
                            echo "\" class=\"form-control\" />
            </div>
            ";
                        }
                        // line 130
                        echo "            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 130) == "textarea")) {
                            // line 131
                            echo "            <div class=\"col-12 form-group d-flex align-items-center justify-content-betweenform-group";
                            if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 131)) {
                                echo " required ";
                            }
                            echo "\">
              <label class=\"control-label\" for=\"input-option";
                            // line 132
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 132);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 132);
                            echo "</label>
              <textarea name=\"option[";
                            // line 133
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 133);
                            echo "]\" rows=\"5\" placeholder=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 133);
                            echo "\" id=\"input-option";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 133);
                            echo "\" class=\"form-control\">";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 133);
                            echo "</textarea>
            </div>
            ";
                        }
                        // line 136
                        echo "            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 136) == "file")) {
                            // line 137
                            echo "            <div class=\"col-12 form-group d-flex align-items-center justify-content-between";
                            if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 137)) {
                                echo " required ";
                            }
                            echo "\">
              <label class=\"control-label\">";
                            // line 138
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 138);
                            echo "</label>
              <button type=\"button\" id=\"button-upload";
                            // line 139
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 139);
                            echo "\" data-loading-text=\"";
                            echo ($context["text_loading"] ?? null);
                            echo "\" class=\"btn btn-default btn-block\"><i class=\"fa fa-upload\"></i> ";
                            echo ($context["button_upload"] ?? null);
                            echo "</button>
              <input type=\"hidden\" name=\"option[";
                            // line 140
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 140);
                            echo "]\" value=\"\" id=\"input-option";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 140);
                            echo "\" />
            </div>
            ";
                        }
                        // line 143
                        echo "            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 143) == "date")) {
                            // line 144
                            echo "            <div class=\"col-12 form-group d-flex align-items-center justify-content-between";
                            if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 144)) {
                                echo " required ";
                            }
                            echo "\">
              <label class=\"control-label\" for=\"input-option";
                            // line 145
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 145);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 145);
                            echo "</label>
              <div class=\"input-group date\">
                <input type=\"text\" name=\"option[";
                            // line 147
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 147);
                            echo "]\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 147);
                            echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-option";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 147);
                            echo "\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            ";
                        }
                        // line 153
                        echo "            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 153) == "datetime")) {
                            // line 154
                            echo "            <div class=\"col-12 form-group d-flex align-items-center justify-content-between";
                            if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 154)) {
                                echo " required ";
                            }
                            echo "\">
              <label class=\"control-label\" for=\"input-option";
                            // line 155
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 155);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 155);
                            echo "</label>
              <div class=\"input-group datetime\">
                <input type=\"text\" name=\"option[";
                            // line 157
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 157);
                            echo "]\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 157);
                            echo "\" data-date-format=\"YYYY-MM-DD HH:mm\" id=\"input-option";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 157);
                            echo "\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            ";
                        }
                        // line 163
                        echo "            ";
                        if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 163) == "time")) {
                            // line 164
                            echo "            <div class=\"col-12 form-group d-flex align-items-center justify-content-between";
                            if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 164)) {
                                echo " required ";
                            }
                            echo "\">
              <label class=\"control-label\" for=\"input-option";
                            // line 165
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 165);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 165);
                            echo "</label>
              <div class=\"input-group time\">
                <input type=\"text\" name=\"option[";
                            // line 167
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 167);
                            echo "]\" value=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 167);
                            echo "\" data-date-format=\"HH:mm\" id=\"input-option";
                            echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 167);
                            echo "\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            ";
                        }
                        // line 173
                        echo "            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 174
                    echo "            ";
                }
                // line 175
                echo "            ";
                if (($context["recurrings"] ?? null)) {
                    // line 176
                    echo "            <hr>
            <h3>";
                    // line 177
                    echo ($context["text_payment_recurring"] ?? null);
                    echo "</h3>
            <div class=\"col-6 form-group required\">
              <select name=\"recurring_id\" class=\"form-control\">
                <option value=\"\">";
                    // line 180
                    echo ($context["text_select"] ?? null);
                    echo "</option>
                ";
                    // line 181
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["recurrings"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                        // line 182
                        echo "                <option value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["recurring"], "recurring_id", [], "any", false, false, false, 182);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["recurring"], "name", [], "any", false, false, false, 182);
                        echo "</option>
                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 184
                    echo "              </select>
              <div class=\"help-block\" id=\"recurring-description\"></div>
            </div>
            ";
                }
                // line 187
                echo "                
              </div>
              <div class=\"button-group\">
                <button type=\"button\" onclick=\"cart.add('";
                // line 190
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 190);
                echo "', '";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 190);
                echo "');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
                echo ($context["button_cart"] ?? null);
                echo "</span></button>
              </div>
            </div>
          </div>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 195
            echo " </div></div></div>
      <div class=\"rowdd\">

        <!-- Load Format Pagination -->
        ";
            // line 199
            if ((($context["limit"] ?? null) < ($context["ttl"] ?? null))) {
                // line 200
                echo "        <div class=\"row row-pagination-container\">
          <div class=\"col-sm-12 pagination-text-justify\">
            <form action=\"post\">
              <button class=\"btn-load-pagination\" id=\"custom-pagination-button\"><div class=\"title-button\">";
                // line 203
                echo ($context["load_more"] ?? null);
                echo "</div><div class=\"load-pagination\">
                  <div class=\"ring-pagination\"></div>
              </div></button>
              
              <input type=\"hidden\" name=\"load-more-botton\" value=\"";
                // line 207
                echo ($context["config_catalog_limit"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-category\" value=\"";
                // line 208
                echo ($context["category_data"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-urlcategory\" value=\"";
                // line 209
                echo ($context["url_category"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-path\" value=\"";
                // line 210
                echo ($context["path"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-sort\" value=\"";
                // line 211
                echo ($context["sort"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-filter\" value=\"";
                // line 212
                echo ($context["filter"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-order\" value=\"";
                // line 213
                echo ($context["order"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-totalproducts\" value=\"";
                // line 214
                echo ($context["ttl"] ?? null);
                echo "\">
              <input type=\"hidden\" name=\"more-botton-step\" id=\"more-step\" value=\"";
                // line 215
                echo ($context["page"] ?? null);
                echo "\">
            </form>
          </div>
        </div>
        ";
            }
            // line 220
            echo "      
        <div class=\"col-sm-6 text-left\">";
            // line 221
            echo ($context["pagination"] ?? null);
            echo "</div>
      </div>      
      ";
        }
        // line 224
        echo "      ";
        if (( !($context["categories"] ?? null) &&  !($context["products"] ?? null))) {
            // line 225
            echo "      <p>";
            echo ($context["text_empty"] ?? null);
            echo "</p>
      ";
        }
        // line 227
        echo "      ";
        echo ($context["content_bottom"] ?? null);
        echo "</div>
    ";
        // line 228
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
";
        // line 230
        echo ($context["footer"] ?? null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "default/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  793 => 230,  788 => 228,  783 => 227,  777 => 225,  774 => 224,  768 => 221,  765 => 220,  757 => 215,  753 => 214,  749 => 213,  745 => 212,  741 => 211,  737 => 210,  733 => 209,  729 => 208,  725 => 207,  718 => 203,  713 => 200,  711 => 199,  705 => 195,  689 => 190,  684 => 187,  678 => 184,  667 => 182,  663 => 181,  659 => 180,  653 => 177,  650 => 176,  647 => 175,  644 => 174,  638 => 173,  625 => 167,  618 => 165,  611 => 164,  608 => 163,  595 => 157,  588 => 155,  581 => 154,  578 => 153,  565 => 147,  558 => 145,  551 => 144,  548 => 143,  540 => 140,  532 => 139,  528 => 138,  521 => 137,  518 => 136,  506 => 133,  500 => 132,  493 => 131,  490 => 130,  478 => 127,  472 => 126,  465 => 125,  462 => 124,  457 => 121,  449 => 119,  442 => 118,  440 => 117,  435 => 116,  419 => 115,  413 => 114,  409 => 112,  403 => 111,  399 => 110,  392 => 109,  389 => 108,  384 => 105,  376 => 103,  369 => 102,  367 => 101,  363 => 100,  345 => 99,  339 => 98,  335 => 96,  329 => 95,  325 => 94,  318 => 93,  315 => 92,  310 => 89,  299 => 87,  295 => 86,  289 => 85,  283 => 84,  276 => 83,  273 => 82,  268 => 81,  266 => 80,  261 => 78,  249 => 69,  242 => 66,  233 => 65,  226 => 64,  220 => 63,  217 => 62,  215 => 61,  207 => 58,  194 => 54,  190 => 52,  186 => 51,  181 => 48,  179 => 47,  172 => 44,  167 => 41,  159 => 39,  148 => 37,  144 => 36,  140 => 34,  135 => 33,  129 => 29,  118 => 27,  114 => 26,  109 => 23,  107 => 22,  102 => 21,  99 => 20,  94 => 17,  88 => 16,  85 => 15,  75 => 13,  72 => 12,  70 => 11,  66 => 10,  62 => 9,  58 => 7,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}
<div id=\"product-category\" class=\"container\">
  <ul class=\"breadcrumb\">
    {% for breadcrumb in breadcrumbs %}
    <li><a href=\"{{ breadcrumb.href }}\">{{ breadcrumb.text }}</a></li>
    {% endfor %}
  </ul>
  <div class=\"row\">
    <div id=\"content\" class=\"col-12\">{{ content_top }}
      <h1>{{ heading_title }}</h1>
      {% if thumb or description %}
      <div class=\"row\"> {% if thumb %}
        <div class=\"col-sm-2\"><img src=\"{{ thumb }}\" alt=\"{{ heading_title }}\" title=\"{{ heading_title }}\" class=\"img-thumbnail\" /></div>
        {% endif %}
        {% if description %}
        <div class=\"col-sm-10\">{{ description }}</div>
        {% endif %}</div>
      <hr>
      {% endif %}
      {% if categories %}
      <h3>{{ text_refine }}</h3>
      {% if categories|length <= 5 %}
      <div class=\"row\">
        <div class=\"col-sm-3\">
          <ul>
            {% for category in categories %}
            <li><a href=\"{{ category.href }}\">{{ category.name }}</a></li>
            {% endfor %}
          </ul>
        </div>
      </div>
      {% else %}
      <div class=\"row\">{% for category in categories|batch((categories|length / 4)|round(1, 'ceil')) %}
        <div class=\"col-sm-3\">
          <ul>
            {% for child in category %}
            <li><a href=\"{{ child.href }}\">{{ child.name }}</a></li>
            {% endfor %}
          </ul>
        </div>
        {% endfor %}</div>
      <br/>
      {% endif %}
      {% endif %} 
      <div class=\"row\">
      <div class=\"col-12\">
      {% if products %}
      
        <div class=\"row\" id=\"load-format-pagination\">
      
      {% for product in products %}
        <div class=\"product-layout product-grid col-md-4\">
          <div class=\"product-thumb\">
            <div class=\"image\"><a href=\"{{ product.href }}\"><img src=\"{{ product.thumb }}\" alt=\"{{ product.name }}\" title=\"{{ product.name }}\" class=\"img-responsive\" /></a></div>
            <div class=\"product-card-info\">
              <div class=\"row product-card-topinfo\">
                <div class=\"col-7 product-card-name\">
                    <a href=\"{{ product.href }}\">{{ product.name }}</a>
                </div>
                <div class=\"col-5 product-card-price text-md-right\"> 
                {% if product.price %}
                {% if not product.special %}
                  {{ product.price }}
                  {% else %} <span class=\"price-new\">{{ product.special }}</span> <span class=\"price-old\">{{ product.price }}</span> {% endif %}
                  {% if product.tax %} <span class=\"price-tax\">{{ text_tax }} {{ product.tax }}</span> {% endif %}
                {% endif %}/кг
                </div>
              </div>
              <div class=\"product-card-desc\">{{ product.description }}</div>
              <div class=\"product-params\">
                <div class=\"col-12 form-group d-flex align-items-center justify-content-between\">
                  <label class=\"control-label me-2\" for=\"input-quantity\">Количество</label>
                    <div class=\"input-qty\">
                        <div class=\"input-qty-btn minus\">-</div>
                        <input type=\"text\" class=\"input-qty-field\" name=\"quantity\" value=\"1\" size=\"2\" id=\"input-quantity\">
                        <div class=\"input-qty-btn plus\">+</div>
                    </div>
                  <input type=\"hidden\" name=\"product_id\" value=\"{{ product_id }}\" />
                </div>
          {% if product.options %}
            {% for option in product.options %}
            {% if option.type == 'select' %}
            <div class=\"col-12 form-group d-flex align-items-center justify-content-between {% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <select name=\"option[{{ option.product_option_id }}]\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\">
                {% for option_value in option.product_option_value %}
                <option value=\"{{ option_value.product_option_value_id }}\">{{ option_value.name }}</option>
                {% endfor %}
              </select>
            </div>
            {% endif %}
            {% if option.type == 'radio' %}
            <div class=\"col-12 form-group d-flex align-items-center justify-content-between{% if option.required %} required {% endif %}\">
              <label class=\"control-label\">{{ option.name }}</label>
              <div id=\"input-option{{ option.product_option_id }}\"> {% for option_value in option.product_option_value %}
                <div class=\"radio\">
                  <label>
                    <input type=\"radio\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option_value.product_option_value_id }}\" />
                    {% if option_value.image %} <img src=\"{{ option_value.image }}\" alt=\"{{ option_value.name }} {% if option_value.price %} {{ option_value.price_prefix }} {{ option_value.price }} {% endif %}\" class=\"img-thumbnail\" /> {% endif %}                  
                    {{ option_value.name }}
                    {% if option_value.price %}
                    ({{ option_value.price_prefix }}{{ option_value.price }})
                    {% endif %} </label>
                </div>
                {% endfor %} </div>
            </div>
            {% endif %}
            {% if option.type == 'checkbox' %}
            <div class=\"col-12 form-group d-flex align-items-center justify-content-between{% if option.required %} required {% endif %}\">
              <label class=\"control-label\">{{ option.name }}</label>
              <div id=\"input-option{{ option.product_option_id }}\"> {% for option_value in option.product_option_value %}
                <div class=\"checkbox\">
                  <label>
                    <input type=\"checkbox\" name=\"option[{{ option.product_option_id }}][]\" value=\"{{ option_value.product_option_value_id }}\" />
                    {% if option_value.image %} <img src=\"{{ option_value.image }}\" alt=\"{{ option_value.name }} {% if option_value.price %} {{ option_value.price_prefix }} {{ option_value.price }} {% endif %}\" class=\"img-thumbnail\" /> {% endif %}
                    {{ option_value.name }}
                    {% if option_value.price %}
                    ({{ option_value.price_prefix }}{{ option_value.price }})
                    {% endif %} </label>
                </div>
                {% endfor %} </div>
            </div>
            {% endif %}
            {% if option.type == 'text' %}
            <div class=\"col-12 form-group d-flex align-items-center justify-content-between{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <input type=\"text\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option.value }}\" placeholder=\"{{ option.name }}\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\" />
            </div>
            {% endif %}
            {% if option.type == 'textarea' %}
            <div class=\"col-12 form-group d-flex align-items-center justify-content-betweenform-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <textarea name=\"option[{{ option.product_option_id }}]\" rows=\"5\" placeholder=\"{{ option.name }}\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\">{{ option.value }}</textarea>
            </div>
            {% endif %}
            {% if option.type == 'file' %}
            <div class=\"col-12 form-group d-flex align-items-center justify-content-between{% if option.required %} required {% endif %}\">
              <label class=\"control-label\">{{ option.name }}</label>
              <button type=\"button\" id=\"button-upload{{ option.product_option_id }}\" data-loading-text=\"{{ text_loading }}\" class=\"btn btn-default btn-block\"><i class=\"fa fa-upload\"></i> {{ button_upload }}</button>
              <input type=\"hidden\" name=\"option[{{ option.product_option_id }}]\" value=\"\" id=\"input-option{{ option.product_option_id }}\" />
            </div>
            {% endif %}
            {% if option.type == 'date' %}
            <div class=\"col-12 form-group d-flex align-items-center justify-content-between{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <div class=\"input-group date\">
                <input type=\"text\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option.value }}\" data-date-format=\"YYYY-MM-DD\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            {% endif %}
            {% if option.type == 'datetime' %}
            <div class=\"col-12 form-group d-flex align-items-center justify-content-between{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <div class=\"input-group datetime\">
                <input type=\"text\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option.value }}\" data-date-format=\"YYYY-MM-DD HH:mm\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            {% endif %}
            {% if option.type == 'time' %}
            <div class=\"col-12 form-group d-flex align-items-center justify-content-between{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <div class=\"input-group time\">
                <input type=\"text\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option.value }}\" data-date-format=\"HH:mm\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            {% endif %}
            {% endfor %}
            {% endif %}
            {% if recurrings %}
            <hr>
            <h3>{{ text_payment_recurring }}</h3>
            <div class=\"col-6 form-group required\">
              <select name=\"recurring_id\" class=\"form-control\">
                <option value=\"\">{{ text_select }}</option>
                {% for recurring in recurrings %}
                <option value=\"{{ recurring.recurring_id }}\">{{ recurring.name }}</option>
                {% endfor %}
              </select>
              <div class=\"help-block\" id=\"recurring-description\"></div>
            </div>
            {% endif %}                
              </div>
              <div class=\"button-group\">
                <button type=\"button\" onclick=\"cart.add('{{ product.product_id }}', '{{ product.minimum }}');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ button_cart }}</span></button>
              </div>
            </div>
          </div>
        </div>
        {% endfor %} </div></div></div>
      <div class=\"rowdd\">

        <!-- Load Format Pagination -->
        {% if limit < ttl %}
        <div class=\"row row-pagination-container\">
          <div class=\"col-sm-12 pagination-text-justify\">
            <form action=\"post\">
              <button class=\"btn-load-pagination\" id=\"custom-pagination-button\"><div class=\"title-button\">{{ load_more }}</div><div class=\"load-pagination\">
                  <div class=\"ring-pagination\"></div>
              </div></button>
              
              <input type=\"hidden\" name=\"load-more-botton\" value=\"{{ config_catalog_limit }}\">
              <input type=\"hidden\" name=\"more-botton-category\" value=\"{{ category_data }}\">
              <input type=\"hidden\" name=\"more-botton-urlcategory\" value=\"{{ url_category }}\">
              <input type=\"hidden\" name=\"more-botton-path\" value=\"{{ path }}\">
              <input type=\"hidden\" name=\"more-botton-sort\" value=\"{{ sort }}\">
              <input type=\"hidden\" name=\"more-botton-filter\" value=\"{{ filter }}\">
              <input type=\"hidden\" name=\"more-botton-order\" value=\"{{ order }}\">
              <input type=\"hidden\" name=\"more-botton-totalproducts\" value=\"{{ ttl }}\">
              <input type=\"hidden\" name=\"more-botton-step\" id=\"more-step\" value=\"{{ page }}\">
            </form>
          </div>
        </div>
        {% endif %}
      
        <div class=\"col-sm-6 text-left\">{{ pagination }}</div>
      </div>      
      {% endif %}
      {% if not categories and not products %}
      <p>{{ text_empty }}</p>
      {% endif %}
      {{ content_bottom }}</div>
    {{ column_right }}</div>
</div>
{{ footer }} 
", "default/template/product/category.twig", "");
    }
}
