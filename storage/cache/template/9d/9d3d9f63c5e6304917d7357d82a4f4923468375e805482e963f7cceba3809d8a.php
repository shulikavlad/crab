<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/module/load_format_pagination.twig */
class __TwigTemplate_6737612b73883d67b3e5f9cd67f952e73216b4c6bfae0aca0ec663b3448efe98 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 2
            if (((($context["display"] ?? null) == "grid") && (($context["cols"] ?? null) == 2))) {
                // line 3
                echo "<div class=\"product-layout product-";
                echo ($context["display"] ?? null);
                echo " col-lg-6 col-md-6 col-sm-12 col-xs-12\">
";
            } elseif (((            // line 4
($context["display"] ?? null) == "grid") && (($context["cols"] ?? null) == 1))) {
                // line 5
                echo "<div class=\"product-layout product-";
                echo ($context["display"] ?? null);
                echo " col-lg-4 col-md-4 col-sm-6 col-xs-12\">
";
            } elseif ((((            // line 6
($context["display"] ?? null) == "grid") && (($context["cols"] ?? null) != 1)) && (($context["cols"] ?? null) != 2))) {
                // line 7
                echo "<div class=\"product-layout product-";
                echo ($context["display"] ?? null);
                echo " col-lg-3 col-md-3 col-sm-6 col-xs-12\">
";
            }
            // line 9
            if ((($context["display"] ?? null) == "list")) {
                // line 10
                echo "<div class=\"product-layout product-";
                echo ($context["display"] ?? null);
                echo " col-xs-12\">
";
            }
            // line 12
            echo "  <div class=\"product-thumb\">
    <div class=\"image\"><a href=\"";
            // line 13
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 13);
            echo "\"><img src=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 13);
            echo "\" alt=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 13);
            echo "\" title=\"";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 13);
            echo "\" class=\"img-responsive\" /></a></div>
    <div>
      <div class=\"caption\">
        <h4><a href=\"";
            // line 16
            echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 16);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 16);
            echo "</a></h4>
        <p>";
            // line 17
            echo twig_get_attribute($this->env, $this->source, $context["product"], "description", [], "any", false, false, false, 17);
            echo "</p>
        ";
            // line 18
            if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 18)) {
                // line 19
                echo "        <p class=\"price\">
          ";
                // line 20
                if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 20)) {
                    // line 21
                    echo "          ";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 21);
                    echo "
          ";
                } else {
                    // line 23
                    echo "          <span class=\"price-new\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 23);
                    echo "</span> <span class=\"price-old\">";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 23);
                    echo "</span>
          ";
                }
                // line 25
                echo "          ";
                if (twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 25)) {
                    // line 26
                    echo "          <span class=\"price-tax\">";
                    echo ($context["text_tax"] ?? null);
                    echo " ";
                    echo twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 26);
                    echo "</span>
          ";
                }
                // line 28
                echo "        </p>
        ";
            }
            // line 30
            echo "        ";
            if (twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 30)) {
                // line 31
                echo "        <div class=\"rating\">
          ";
                // line 32
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 33
                    echo "          ";
                    if ((twig_get_attribute($this->env, $this->source, $context["product"], "rating", [], "any", false, false, false, 33) < $context["i"])) {
                        // line 34
                        echo "          <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
          ";
                    } else {
                        // line 36
                        echo "          <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
          ";
                    }
                    // line 38
                    echo "          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 39
                echo "        </div>
        ";
            }
            // line 41
            echo "      </div>
      <div class=\"button-group\">
        <button type=\"button\" onclick=\"cart.add('";
            // line 43
            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 43);
            echo "', '";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 43);
            echo "');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
            echo ($context["button_cart"] ?? null);
            echo "</span></button>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 44
            echo ($context["button_wishlist"] ?? null);
            echo "\" onclick=\"wishlist.add('";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 44);
            echo "');\"><i class=\"fa fa-heart\"></i></button>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 45
            echo ($context["button_compare"] ?? null);
            echo "\" onclick=\"compare.add('";
            echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 45);
            echo "');\"><i class=\"fa fa-exchange\"></i></button>
      </div>
    </div>
  </div>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/load_format_pagination.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 45,  175 => 44,  167 => 43,  163 => 41,  159 => 39,  153 => 38,  149 => 36,  145 => 34,  142 => 33,  138 => 32,  135 => 31,  132 => 30,  128 => 28,  120 => 26,  117 => 25,  109 => 23,  103 => 21,  101 => 20,  98 => 19,  96 => 18,  92 => 17,  86 => 16,  74 => 13,  71 => 12,  65 => 10,  63 => 9,  57 => 7,  55 => 6,  50 => 5,  48 => 4,  43 => 3,  41 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for product in products %}
{% if display == \"grid\" and cols == 2 %}
<div class=\"product-layout product-{{ display }} col-lg-6 col-md-6 col-sm-12 col-xs-12\">
{% elseif display == \"grid\" and cols == 1 %}
<div class=\"product-layout product-{{ display }} col-lg-4 col-md-4 col-sm-6 col-xs-12\">
{% elseif display == \"grid\" and cols != 1 and cols != 2 %}
<div class=\"product-layout product-{{ display }} col-lg-3 col-md-3 col-sm-6 col-xs-12\">
{% endif %}
{% if display == \"list\" %}
<div class=\"product-layout product-{{ display }} col-xs-12\">
{% endif %}
  <div class=\"product-thumb\">
    <div class=\"image\"><a href=\"{{ product.href }}\"><img src=\"{{ product.thumb }}\" alt=\"{{ product.name }}\" title=\"{{ product.name }}\" class=\"img-responsive\" /></a></div>
    <div>
      <div class=\"caption\">
        <h4><a href=\"{{ product.href }}\">{{ product.name }}</a></h4>
        <p>{{ product.description }}</p>
        {% if product.price %}
        <p class=\"price\">
          {% if not product.special %}
          {{ product.price }}
          {% else %}
          <span class=\"price-new\">{{ product.special }}</span> <span class=\"price-old\">{{ product.price }}</span>
          {% endif %}
          {% if product.tax %}
          <span class=\"price-tax\">{{ text_tax }} {{ product.tax }}</span>
          {% endif %}
        </p>
        {% endif %}
        {% if product.rating %}
        <div class=\"rating\">
          {% for i in 1..5 %}
          {% if product.rating < i %}
          <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
          {% else %}
          <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
          {% endif %}
          {% endfor %}
        </div>
        {% endif %}
      </div>
      <div class=\"button-group\">
        <button type=\"button\" onclick=\"cart.add('{{ product.product_id }}', '{{ product.minimum }}');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ button_cart }}</span></button>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"{{ button_wishlist }}\" onclick=\"wishlist.add('{{ product.product_id }}');\"><i class=\"fa fa-heart\"></i></button>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"{{ button_compare }}\" onclick=\"compare.add('{{ product.product_id }}');\"><i class=\"fa fa-exchange\"></i></button>
      </div>
    </div>
  </div>
</div>
{% endfor %}", "default/template/extension/module/load_format_pagination.twig", "");
    }
}
