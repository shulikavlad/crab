<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/tmdheader.twig */
class __TwigTemplate_3fb057dad9dfe3354824cc1c86a51917cb6a670fa78ec84757c205cfa2bee211 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "<div class=\"container\">
  <nav id=\"menu\" class=\"navbar\">
    <div class=\"navbar-header\"><span id=\"category\" class=\"visible-xs\">";
            // line 4
            echo ($context["text_category"] ?? null);
            echo "</span>
      <button type=\"button\" class=\"btn btn-navbar navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\"><i class=\"fa fa-bars\"></i></button>
    </div>
    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
      <ul class=\"nav navbar-nav\">
\t  ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 10
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 10) ==  -1)) {
                    // line 11
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 12
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 12)) {
                            // line 13
                            echo "                <li class=\"dropdown\"><a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 13);
                            echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 13);
                            echo "</a>
                  <div class=\"dropdown-menu\">
                    <div class=\"dropdown-inner\"> ";
                            // line 15
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 15), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 15)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 15), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 16
                                echo "                      <ul class=\"list-unstyled\">
                        ";
                                // line 17
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 18
                                    echo "                        <li><a href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "link", [], "any", false, false, false, 18);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 18);
                                    echo "</a></li>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 20
                                echo "                      </ul>
                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 21
                            echo "</div>
                    <a href=\"";
                            // line 22
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 22);
                            echo "\" class=\"see-all\">";
                            echo ($context["text_all"] ?? null);
                            echo " ";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 22);
                            echo "</a> </div>
                </li>
                ";
                        } else {
                            // line 25
                            echo "                <li><a href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 25);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 25);
                            echo "</a></li>
                ";
                        }
                        // line 27
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 28
                    echo "            ";
                } else {
                    // line 29
                    echo "                <li class=\"dropdown\"><a class=\"dropdown-toggle\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 29);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 29);
                    echo "</a>
                ";
                    // line 30
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 30)) {
                        // line 31
                        echo "                    <div class=\"dropdown-menu\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 34
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 34));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 35
                            echo "                    <li>
                        ";
                            // line 36
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 36)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 37
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 37);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 37);
                                echo "</a>
                        ";
                            } else {
                                // line 39
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 39);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 39);
                                echo "</a>\t
                        ";
                            }
                            // line 41
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 41)) {
                                // line 42
                                echo "                    
                    <ul>
                    ";
                                // line 44
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 44));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 45
                                    echo "                    <li>
                        ";
                                    // line 46
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 46)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 47
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 47);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 47);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 49
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 49);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 49);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 51
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 53
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 56
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 58
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 62
                    echo "\t
                </li>\t
            ";
                }
                // line 64
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "      </ul>
    </div>
  </nav>
</div>
";
        }
        // line 70
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/extension/tmdheader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  252 => 70,  245 => 66,  238 => 64,  233 => 62,  226 => 58,  219 => 56,  214 => 53,  207 => 51,  199 => 49,  192 => 47,  188 => 46,  185 => 45,  181 => 44,  177 => 42,  174 => 41,  166 => 39,  159 => 37,  155 => 36,  152 => 35,  148 => 34,  143 => 31,  141 => 30,  134 => 29,  131 => 28,  125 => 27,  117 => 25,  107 => 22,  104 => 21,  97 => 20,  86 => 18,  82 => 17,  79 => 16,  75 => 15,  67 => 13,  64 => 12,  59 => 11,  57 => 10,  51 => 9,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "default/template/extension/tmdheader.twig", "");
    }
}
