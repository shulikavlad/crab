<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/product/product.twig */
class __TwigTemplate_4d8cdb5b4e98afecbf07ee2308a85ada540e2f0bb731919d19ed287f5325c0cf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo "
<div id=\"product-product\" class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 5);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 5);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
  <div class=\"row\">";
        // line 8
        echo ($context["column_left"] ?? null);
        echo "
    <div id=\"content\" class=\"container\">
      <div class=\"row\"> 
        <div class=\"col-md-6\"> ";
        // line 11
        if ((($context["thumb"] ?? null) || ($context["images"] ?? null))) {
            // line 12
            echo "          <div class=\"big-images\">
            ";
            // line 13
            if (($context["thumb"] ?? null)) {
                // line 14
                echo "            <div><img src=\"";
                echo ($context["thumb"] ?? null);
                echo "\" title=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" alt=\"";
                echo ($context["heading_title"] ?? null);
                echo "\" /></div>
            ";
            }
            // line 16
            echo "            ";
            if (($context["images"] ?? null)) {
                // line 17
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 18
                    echo "            <div class=\"image-additional\"><img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 18);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 20
                echo "            ";
            }
            // line 21
            echo "          </div>
          ";
            // line 22
            if (($context["images"] ?? null)) {
                // line 23
                echo "          <ul class=\"thumbnails\">
            ";
                // line 24
                if (($context["thumb"] ?? null)) {
                    // line 25
                    echo "            <li><a class=\"thumbnail\" href=\"";
                    echo ($context["popup"] ?? null);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\"><img src=\"";
                    echo ($context["thumb"] ?? null);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a></li>
            ";
                }
                // line 27
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 28
                    echo "            <li class=\"image-additional\"><a class=\"thumbnail\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "popup", [], "any", false, false, false, 28);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\"> <img src=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["image"], "thumb", [], "any", false, false, false, 28);
                    echo "\" title=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" alt=\"";
                    echo ($context["heading_title"] ?? null);
                    echo "\" /></a></li>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 30
                echo "          </ul>
          ";
            }
            // line 32
            echo "          ";
        }
        // line 33
        echo "        </div>
        <div class=\"col-md-6\">
          <div class=\"row\">
          <div class=\"row\">
            <div class=\"col-md-8\">
                <h1>";
        // line 38
        echo ($context["heading_title"] ?? null);
        echo "</h1>
            </div>
            <div class=\"col-md-4 product-page-price\">
                ";
        // line 41
        if ( !($context["special"] ?? null)) {
            echo ($context["price"] ?? null);
        } else {
            echo ($context["price"] ?? null);
            echo ($context["special"] ?? null);
        }
        echo "/кг
            </div>
          </div>
            
          <div id=\"product\" class=\"row\"> 
            <div class=\"col-6 form-group d-flex\">
              <label class=\"control-label me-2\" for=\"input-quantity\">";
        // line 47
        echo ($context["entry_qty"] ?? null);
        echo "</label>
                <div class=\"input-qty\">
                    <div class=\"input-qty-btn minus\">-</div>
                    <input type=\"text\" class=\"input-qty-field\" name=\"quantity\" value=\"";
        // line 50
        echo ($context["minimum"] ?? null);
        echo "\" size=\"2\" id=\"input-quantity\">
                    <div class=\"input-qty-btn plus\">+</div>
                </div>
              <input type=\"hidden\" name=\"product_id\" value=\"";
        // line 53
        echo ($context["product_id"] ?? null);
        echo "\" />
            </div>        
          ";
        // line 55
        if (($context["options"] ?? null)) {
            // line 56
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 57
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 57) == "select")) {
                    // line 58
                    echo "            <div class=\"col-6 form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 58)) {
                        echo " required ";
                    }
                    echo "\">
              <label class=\"control-label\" for=\"input-option";
                    // line 59
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 59);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 59);
                    echo "</label>
              <select name=\"option[";
                    // line 60
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 60);
                    echo "]\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 60);
                    echo "\" class=\"form-control\">
                <option value=\"\">";
                    // line 61
                    echo ($context["text_select"] ?? null);
                    echo "</option>
                ";
                    // line 62
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 62));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 63
                        echo "                <option value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 63);
                        echo "\">";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 63);
                        echo "
                ";
                        // line 64
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 64)) {
                            // line 65
                            echo "                (";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 65);
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 65);
                            echo ")
                ";
                        }
                        // line 66
                        echo " </option>
                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 68
                    echo "              </select>
            </div>
            ";
                }
                // line 71
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 71) == "radio")) {
                    // line 72
                    echo "            <div class=\"col-6 form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 72)) {
                        echo " required ";
                    }
                    echo "\">
              <label class=\"control-label\">";
                    // line 73
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 73);
                    echo "</label>
              <div id=\"input-option";
                    // line 74
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 74);
                    echo "\"> ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 74));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 75
                        echo "                <div class=\"radio\">
                  <label>
                    <input type=\"radio\" name=\"option[";
                        // line 77
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 77);
                        echo "]\" value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 77);
                        echo "\" />
                    ";
                        // line 78
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 78)) {
                            echo " <img src=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 78);
                            echo "\" alt=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 78);
                            echo " ";
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 78)) {
                                echo " ";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 78);
                                echo " ";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 78);
                                echo " ";
                            }
                            echo "\" class=\"img-thumbnail\" /> ";
                        }
                        echo "                  
                    ";
                        // line 79
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 79);
                        echo "
                    ";
                        // line 80
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 80)) {
                            // line 81
                            echo "                    (";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 81);
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 81);
                            echo ")
                    ";
                        }
                        // line 82
                        echo " </label>
                </div>
                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 84
                    echo " </div>
            </div>
            ";
                }
                // line 87
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 87) == "checkbox")) {
                    // line 88
                    echo "            <div class=\"col-6 form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 88)) {
                        echo " required ";
                    }
                    echo "\">
              <label class=\"control-label\">";
                    // line 89
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 89);
                    echo "</label>
              <div id=\"input-option";
                    // line 90
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 90);
                    echo "\"> ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["option"], "product_option_value", [], "any", false, false, false, 90));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 91
                        echo "                <div class=\"checkbox\">
                  <label>
                    <input type=\"checkbox\" name=\"option[";
                        // line 93
                        echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 93);
                        echo "][]\" value=\"";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "product_option_value_id", [], "any", false, false, false, 93);
                        echo "\" />
                    ";
                        // line 94
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 94)) {
                            echo " <img src=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "image", [], "any", false, false, false, 94);
                            echo "\" alt=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 94);
                            echo " ";
                            if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 94)) {
                                echo " ";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 94);
                                echo " ";
                                echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 94);
                                echo " ";
                            }
                            echo "\" class=\"img-thumbnail\" /> ";
                        }
                        // line 95
                        echo "                    ";
                        echo twig_get_attribute($this->env, $this->source, $context["option_value"], "name", [], "any", false, false, false, 95);
                        echo "
                    ";
                        // line 96
                        if (twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 96)) {
                            // line 97
                            echo "                    (";
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price_prefix", [], "any", false, false, false, 97);
                            echo twig_get_attribute($this->env, $this->source, $context["option_value"], "price", [], "any", false, false, false, 97);
                            echo ")
                    ";
                        }
                        // line 98
                        echo " </label>
                </div>
                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 100
                    echo " </div>
            </div>
            ";
                }
                // line 103
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 103) == "text")) {
                    // line 104
                    echo "            <div class=\"col-6 form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 104)) {
                        echo " required ";
                    }
                    echo "\">
              <label class=\"control-label\" for=\"input-option";
                    // line 105
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 105);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 105);
                    echo "</label>
              <input type=\"text\" name=\"option[";
                    // line 106
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 106);
                    echo "]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 106);
                    echo "\" placeholder=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 106);
                    echo "\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 106);
                    echo "\" class=\"form-control\" />
            </div>
            ";
                }
                // line 109
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 109) == "textarea")) {
                    // line 110
                    echo "            <div class=\"col-6 form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 110)) {
                        echo " required ";
                    }
                    echo "\">
              <label class=\"control-label\" for=\"input-option";
                    // line 111
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 111);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 111);
                    echo "</label>
              <textarea name=\"option[";
                    // line 112
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 112);
                    echo "]\" rows=\"5\" placeholder=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 112);
                    echo "\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 112);
                    echo "\" class=\"form-control\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 112);
                    echo "</textarea>
            </div>
            ";
                }
                // line 115
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 115) == "file")) {
                    // line 116
                    echo "            <div class=\"col-6 form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 116)) {
                        echo " required ";
                    }
                    echo "\">
              <label class=\"control-label\">";
                    // line 117
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 117);
                    echo "</label>
              <button type=\"button\" id=\"button-upload";
                    // line 118
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 118);
                    echo "\" data-loading-text=\"";
                    echo ($context["text_loading"] ?? null);
                    echo "\" class=\"btn btn-default btn-block\"><i class=\"fa fa-upload\"></i> ";
                    echo ($context["button_upload"] ?? null);
                    echo "</button>
              <input type=\"hidden\" name=\"option[";
                    // line 119
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 119);
                    echo "]\" value=\"\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 119);
                    echo "\" />
            </div>
            ";
                }
                // line 122
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 122) == "date")) {
                    // line 123
                    echo "            <div class=\"col-6 form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 123)) {
                        echo " required ";
                    }
                    echo "\">
              <label class=\"control-label\" for=\"input-option";
                    // line 124
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 124);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 124);
                    echo "</label>
              <div class=\"input-group date\">
                <input type=\"text\" name=\"option[";
                    // line 126
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 126);
                    echo "]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 126);
                    echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 126);
                    echo "\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            ";
                }
                // line 132
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 132) == "datetime")) {
                    // line 133
                    echo "            <div class=\"col-6 form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 133)) {
                        echo " required ";
                    }
                    echo "\">
              <label class=\"control-label\" for=\"input-option";
                    // line 134
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 134);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 134);
                    echo "</label>
              <div class=\"input-group datetime\">
                <input type=\"text\" name=\"option[";
                    // line 136
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 136);
                    echo "]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 136);
                    echo "\" data-date-format=\"YYYY-MM-DD HH:mm\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 136);
                    echo "\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            ";
                }
                // line 142
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["option"], "type", [], "any", false, false, false, 142) == "time")) {
                    // line 143
                    echo "            <div class=\"col-6 form-group";
                    if (twig_get_attribute($this->env, $this->source, $context["option"], "required", [], "any", false, false, false, 143)) {
                        echo " required ";
                    }
                    echo "\">
              <label class=\"control-label\" for=\"input-option";
                    // line 144
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 144);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "name", [], "any", false, false, false, 144);
                    echo "</label>
              <div class=\"input-group time\">
                <input type=\"text\" name=\"option[";
                    // line 146
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 146);
                    echo "]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "value", [], "any", false, false, false, 146);
                    echo "\" data-date-format=\"HH:mm\" id=\"input-option";
                    echo twig_get_attribute($this->env, $this->source, $context["option"], "product_option_id", [], "any", false, false, false, 146);
                    echo "\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            ";
                }
                // line 152
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 153
            echo "            ";
        }
        // line 154
        echo "            ";
        if (($context["recurrings"] ?? null)) {
            // line 155
            echo "            <hr>
            <h3>";
            // line 156
            echo ($context["text_payment_recurring"] ?? null);
            echo "</h3>
            <div class=\"col-6 form-group required\">
              <select name=\"recurring_id\" class=\"form-control\">
                <option value=\"\">";
            // line 159
            echo ($context["text_select"] ?? null);
            echo "</option>
                ";
            // line 160
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["recurrings"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                // line 161
                echo "                <option value=\"";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "recurring_id", [], "any", false, false, false, 161);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["recurring"], "name", [], "any", false, false, false, 161);
                echo "</option>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 163
            echo "              </select>
              <div class=\"help-block\" id=\"recurring-description\"></div>
            </div>
            ";
        }
        // line 167
        echo "              <div class=\"row py-4 align-items-center\">
                <div class=\"col-6\">
                    ";
        // line 169
        echo ($context["stock"] ?? null);
        echo "
                </div>
                <div class=\"col-6\">
                    <button type=\"button\" id=\"button-cart\" data-loading-text=\"";
        // line 172
        echo ($context["text_loading"] ?? null);
        echo "\">";
        echo ($context["button_cart"] ?? null);
        echo "</button>
                </div>
              </div>            
            </div>
              <ul class=\"list-unstyled\">
                <li>";
        // line 177
        echo ($context["text_model"] ?? null);
        echo " <b>";
        echo ($context["model"] ?? null);
        echo "</b></li>
                ";
        // line 178
        if (($context["manufacturer"] ?? null)) {
            // line 179
            echo "                <li>";
            echo ($context["text_manufacturer"] ?? null);
            echo " <b>";
            echo ($context["manufacturer"] ?? null);
            echo "</b></li>
                ";
        }
        // line 180
        echo "            
                ";
        // line 181
        if (($context["reward"] ?? null)) {
            // line 182
            echo "                <li>";
            echo ($context["text_reward"] ?? null);
            echo " ";
            echo ($context["reward"] ?? null);
            echo "</li>
                ";
        }
        // line 184
        echo "              </ul>          
              <div class=\"product-page-desc\">";
        // line 185
        echo ($context["description"] ?? null);
        echo "</div>
          </div>
      </div>
      </div>
        ";
        // line 189
        if (($context["tags"] ?? null)) {
            // line 190
            echo "        <p>";
            echo ($context["text_tags"] ?? null);
            echo "
        ";
            // line 191
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["tags"] ?? null)) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 192
                echo "        ";
                if (($context["i"] < (twig_length_filter($this->env, ($context["tags"] ?? null)) - 1))) {
                    echo " <a href=\"";
                    echo twig_get_attribute($this->env, $this->source, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["tags"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[$context["i"]] ?? null) : null), "href", [], "any", false, false, false, 192);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = ($context["tags"] ?? null)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[$context["i"]] ?? null) : null), "tag", [], "any", false, false, false, 192);
                    echo "</a>,
        ";
                } else {
                    // line 193
                    echo " <a href=\"";
                    echo twig_get_attribute($this->env, $this->source, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = ($context["tags"] ?? null)) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b[$context["i"]] ?? null) : null), "href", [], "any", false, false, false, 193);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = ($context["tags"] ?? null)) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002[$context["i"]] ?? null) : null), "tag", [], "any", false, false, false, 193);
                    echo "</a> ";
                }
                // line 194
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " </p>
        ";
        }
        // line 196
        echo "      ";
        echo ($context["content_bottom"] ?? null);
        echo "</div>    
    ";
        // line 197
        echo ($context["column_right"] ?? null);
        echo "</div>
</div>
      ";
        // line 199
        if (($context["products"] ?? null)) {
            // line 200
            echo "      <div id=\"related-products\">
        <div class=\"related-products-inner container\">
          <h3>";
            // line 202
            echo ($context["text_related"] ?? null);
            echo "</h3> 
          <div class=\"row\"> ";
            // line 203
            $context["i"] = 0;
            // line 204
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 205
                echo "            <div class=\"product-layout product-grid col-md-4\">
              <div class=\"product-thumb\">
                <div class=\"image\"><a href=\"";
                // line 207
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 207);
                echo "\"><img src=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "thumb", [], "any", false, false, false, 207);
                echo "\" alt=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 207);
                echo "\" title=\"";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 207);
                echo "\" class=\"img-responsive\" /></a></div>
                <div class=\"product-card-info\">
                  <div class=\"row product-card-topinfo\">
                    <div class=\"col-7 product-card-name\">
                        <a href=\"";
                // line 211
                echo twig_get_attribute($this->env, $this->source, $context["product"], "href", [], "any", false, false, false, 211);
                echo "\">";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 211);
                echo "</a>
                    </div>
                    <div class=\"col-5 product-card-price text-md-right\"> 
                    ";
                // line 214
                if (twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 214)) {
                    // line 215
                    echo "                    ";
                    if ( !twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 215)) {
                        // line 216
                        echo "                      ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 216);
                        echo "
                      ";
                    } else {
                        // line 217
                        echo " <span class=\"price-new\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "special", [], "any", false, false, false, 217);
                        echo "</span> <span class=\"price-old\">";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "price", [], "any", false, false, false, 217);
                        echo "</span> ";
                    }
                    // line 218
                    echo "                      ";
                    if (twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 218)) {
                        echo " <span class=\"price-tax\">";
                        echo ($context["text_tax"] ?? null);
                        echo " ";
                        echo twig_get_attribute($this->env, $this->source, $context["product"], "tax", [], "any", false, false, false, 218);
                        echo "</span> ";
                    }
                    // line 219
                    echo "                    ";
                }
                echo "/кг
                    </div>
                  </div>
                  <div class=\"product-card-desc\">";
                // line 222
                echo twig_get_attribute($this->env, $this->source, $context["product"], "description", [], "any", false, false, false, 222);
                echo "</div>
                  <div class=\"button-group\">
                    <button type=\"button\" onclick=\"cart.add('";
                // line 224
                echo twig_get_attribute($this->env, $this->source, $context["product"], "product_id", [], "any", false, false, false, 224);
                echo "', '";
                echo twig_get_attribute($this->env, $this->source, $context["product"], "minimum", [], "any", false, false, false, 224);
                echo "');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
                echo ($context["button_cart"] ?? null);
                echo "</span></button>
                  </div>
                </div>
              </div>
            </div>
            ";
                // line 229
                if (((($context["column_left"] ?? null) && ($context["column_right"] ?? null)) && (((($context["i"] ?? null) + 1) % 2) == 0))) {
                    // line 230
                    echo "            <div class=\"clearfix visible-md visible-sm\"></div>
            ";
                } elseif ((                // line 231
($context["column_left"] ?? null) || (($context["column_right"] ?? null) && (((($context["i"] ?? null) + 1) % 3) == 0)))) {
                    // line 232
                    echo "            <div class=\"clearfix visible-md\"></div>
            ";
                } elseif ((((                // line 233
($context["i"] ?? null) + 1) % 4) == 0)) {
                    // line 234
                    echo "            <div class=\"clearfix visible-md\"></div>
            ";
                }
                // line 236
                echo "            ";
                $context["i"] = (($context["i"] ?? null) + 1);
                // line 237
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " </div>
            </div>
        </div>
        ";
        }
        // line 240
        echo "  
<script type=\"text/javascript\"><!--
\$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
\t\$.ajax({
\t\turl: 'index.php?route=product/product/getRecurringDescription',
\t\ttype: 'post',
\t\tdata: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#recurring-description').html('');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();

\t\t\tif (json['success']) {
\t\t\t\t\$('#recurring-description').html(json['success']);
\t\t\t}
\t\t}
\t});
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('#button-cart').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=checkout/cart/add',
\t\ttype: 'post',
\t\tdata: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#button-cart').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-cart').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');

\t\t\tif (json['error']) {
\t\t\t\tif (json['error']['option']) {
\t\t\t\t\tfor (i in json['error']['option']) {
\t\t\t\t\t\tvar element = \$('#input-option' + i.replace('_', '-'));

\t\t\t\t\t\tif (element.parent().hasClass('input-group')) {
\t\t\t\t\t\t\telement.parent().after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\telement.after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\tif (json['error']['recurring']) {
\t\t\t\t\t\$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
\t\t\t\t}

\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().addClass('has-error');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('.breadcrumb').after('<div class=\"alert alert-success alert-dismissible\">' + json['success'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');

\t\t\t\t\$('#cart > button').html('<span id=\"cart-total\"><i class=\"fa fa-shopping-cart\"></i> ' + json['total'] + '</span>');

\t\t\t\t\$('html, body').animate({ scrollTop: 0 }, 'slow');

\t\t\t\t\$('#cart > ul').load('index.php?route=common/cart/info ul li');
\t\t\t}
\t\t},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
\t});
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('.date').datetimepicker({
\tlanguage: '";
        // line 317
        echo ($context["datepicker"] ?? null);
        echo "',
\tpickTime: false
});

\$('.datetime').datetimepicker({
\tlanguage: '";
        // line 322
        echo ($context["datepicker"] ?? null);
        echo "',
\tpickDate: true,
\tpickTime: true
});

\$('.time').datetimepicker({
\tlanguage: '";
        // line 328
        echo ($context["datepicker"] ?? null);
        echo "',
\tpickDate: false
});

\$('button[id^=\\'button-upload\\']').on('click', function() {
\tvar node = this;

\t\$('#form-upload').remove();

\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

\t\$('#form-upload input[name=\\'file\\']').trigger('click');

\tif (typeof timer != 'undefined') {
    \tclearInterval(timer);
\t}

\ttimer = setInterval(function() {
\t\tif (\$('#form-upload input[name=\\'file\\']').val() != '') {
\t\t\tclearInterval(timer);

\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=tool/upload',
\t\t\t\ttype: 'post',
\t\t\t\tdataType: 'json',
\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\tcache: false,
\t\t\t\tcontentType: false,
\t\t\t\tprocessData: false,
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\$(node).button('loading');
\t\t\t\t},
\t\t\t\tcomplete: function() {
\t\t\t\t\t\$(node).button('reset');
\t\t\t\t},
\t\t\t\tsuccess: function(json) {
\t\t\t\t\t\$('.text-danger').remove();

\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\t\$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
\t\t\t\t\t}

\t\t\t\t\tif (json['success']) {
\t\t\t\t\t\talert(json['success']);

\t\t\t\t\t\t\$(node).parent().find('input').val(json['code']);
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t}
\t\t\t});
\t\t}
\t}, 500);
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    \$('#review').fadeOut('slow');

    \$('#review').load(this.href);

    \$('#review').fadeIn('slow');
});

\$('#review').load('index.php?route=product/product/review&product_id=";
        // line 395
        echo ($context["product_id"] ?? null);
        echo "');

\$('#button-review').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=product/product/write&product_id=";
        // line 399
        echo ($context["product_id"] ?? null);
        echo "',
\t\ttype: 'post',
\t\tdataType: 'json',
\t\tdata: \$(\"#form-review\").serialize(),
\t\tbeforeSend: function() {
\t\t\t\$('#button-review').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-review').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible').remove();

\t\t\tif (json['error']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '</div>');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

\t\t\t\t\$('input[name=\\'name\\']').val('');
\t\t\t\t\$('textarea[name=\\'text\\']').val('');
\t\t\t\t\$('input[name=\\'rating\\']:checked').prop('checked', false);
\t\t\t}
\t\t}
\t});
});

\$(document).ready(function() {
\t\$('.thumbnails').magnificPopup({
\t\ttype:'image',
\t\tdelegate: 'a',
\t\tgallery: {
\t\t\tenabled: true
\t\t}
\t});
});
//--></script> 
";
        // line 437
        echo ($context["footer"] ?? null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "default/template/product/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1082 => 437,  1041 => 399,  1034 => 395,  964 => 328,  955 => 322,  947 => 317,  868 => 240,  857 => 237,  854 => 236,  850 => 234,  848 => 233,  845 => 232,  843 => 231,  840 => 230,  838 => 229,  826 => 224,  821 => 222,  814 => 219,  805 => 218,  798 => 217,  792 => 216,  789 => 215,  787 => 214,  779 => 211,  766 => 207,  762 => 205,  757 => 204,  755 => 203,  751 => 202,  747 => 200,  745 => 199,  740 => 197,  735 => 196,  726 => 194,  719 => 193,  709 => 192,  705 => 191,  700 => 190,  698 => 189,  691 => 185,  688 => 184,  680 => 182,  678 => 181,  675 => 180,  667 => 179,  665 => 178,  659 => 177,  649 => 172,  643 => 169,  639 => 167,  633 => 163,  622 => 161,  618 => 160,  614 => 159,  608 => 156,  605 => 155,  602 => 154,  599 => 153,  593 => 152,  580 => 146,  573 => 144,  566 => 143,  563 => 142,  550 => 136,  543 => 134,  536 => 133,  533 => 132,  520 => 126,  513 => 124,  506 => 123,  503 => 122,  495 => 119,  487 => 118,  483 => 117,  476 => 116,  473 => 115,  461 => 112,  455 => 111,  448 => 110,  445 => 109,  433 => 106,  427 => 105,  420 => 104,  417 => 103,  412 => 100,  404 => 98,  397 => 97,  395 => 96,  390 => 95,  374 => 94,  368 => 93,  364 => 91,  358 => 90,  354 => 89,  347 => 88,  344 => 87,  339 => 84,  331 => 82,  324 => 81,  322 => 80,  318 => 79,  300 => 78,  294 => 77,  290 => 75,  284 => 74,  280 => 73,  273 => 72,  270 => 71,  265 => 68,  258 => 66,  251 => 65,  249 => 64,  242 => 63,  238 => 62,  234 => 61,  228 => 60,  222 => 59,  215 => 58,  212 => 57,  207 => 56,  205 => 55,  200 => 53,  194 => 50,  188 => 47,  174 => 41,  168 => 38,  161 => 33,  158 => 32,  154 => 30,  137 => 28,  132 => 27,  118 => 25,  116 => 24,  113 => 23,  111 => 22,  108 => 21,  105 => 20,  92 => 18,  87 => 17,  84 => 16,  74 => 14,  72 => 13,  69 => 12,  67 => 11,  61 => 8,  58 => 7,  47 => 5,  43 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ header }}
<div id=\"product-product\" class=\"container\">
  <ul class=\"breadcrumb\">
    {% for breadcrumb in breadcrumbs %}
    <li><a href=\"{{ breadcrumb.href }}\">{{ breadcrumb.text }}</a></li>
    {% endfor %}
  </ul>
  <div class=\"row\">{{ column_left }}
    <div id=\"content\" class=\"container\">
      <div class=\"row\"> 
        <div class=\"col-md-6\"> {% if thumb or images %}
          <div class=\"big-images\">
            {% if thumb %}
            <div><img src=\"{{ thumb }}\" title=\"{{ heading_title }}\" alt=\"{{ heading_title }}\" /></div>
            {% endif %}
            {% if images %}
            {% for image in images %}
            <div class=\"image-additional\"><img src=\"{{ image.thumb }}\" title=\"{{ heading_title }}\" alt=\"{{ heading_title }}\" /></div>
            {% endfor %}
            {% endif %}
          </div>
          {% if images %}
          <ul class=\"thumbnails\">
            {% if thumb %}
            <li><a class=\"thumbnail\" href=\"{{ popup }}\" title=\"{{ heading_title }}\"><img src=\"{{ thumb }}\" title=\"{{ heading_title }}\" alt=\"{{ heading_title }}\" /></a></li>
            {% endif %}
            {% for image in images %}
            <li class=\"image-additional\"><a class=\"thumbnail\" href=\"{{ image.popup }}\" title=\"{{ heading_title }}\"> <img src=\"{{ image.thumb }}\" title=\"{{ heading_title }}\" alt=\"{{ heading_title }}\" /></a></li>
            {% endfor %}
          </ul>
          {% endif %}
          {% endif %}
        </div>
        <div class=\"col-md-6\">
          <div class=\"row\">
          <div class=\"row\">
            <div class=\"col-md-8\">
                <h1>{{ heading_title }}</h1>
            </div>
            <div class=\"col-md-4 product-page-price\">
                {% if not special %}{{ price }}{% else %}{{ price }}{{ special }}{% endif %}/кг
            </div>
          </div>
            
          <div id=\"product\" class=\"row\"> 
            <div class=\"col-6 form-group d-flex\">
              <label class=\"control-label me-2\" for=\"input-quantity\">{{ entry_qty }}</label>
                <div class=\"input-qty\">
                    <div class=\"input-qty-btn minus\">-</div>
                    <input type=\"text\" class=\"input-qty-field\" name=\"quantity\" value=\"{{ minimum }}\" size=\"2\" id=\"input-quantity\">
                    <div class=\"input-qty-btn plus\">+</div>
                </div>
              <input type=\"hidden\" name=\"product_id\" value=\"{{ product_id }}\" />
            </div>        
          {% if options %}
            {% for option in options %}
            {% if option.type == 'select' %}
            <div class=\"col-6 form-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <select name=\"option[{{ option.product_option_id }}]\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\">
                <option value=\"\">{{ text_select }}</option>
                {% for option_value in option.product_option_value %}
                <option value=\"{{ option_value.product_option_value_id }}\">{{ option_value.name }}
                {% if option_value.price %}
                ({{ option_value.price_prefix }}{{ option_value.price }})
                {% endif %} </option>
                {% endfor %}
              </select>
            </div>
            {% endif %}
            {% if option.type == 'radio' %}
            <div class=\"col-6 form-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\">{{ option.name }}</label>
              <div id=\"input-option{{ option.product_option_id }}\"> {% for option_value in option.product_option_value %}
                <div class=\"radio\">
                  <label>
                    <input type=\"radio\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option_value.product_option_value_id }}\" />
                    {% if option_value.image %} <img src=\"{{ option_value.image }}\" alt=\"{{ option_value.name }} {% if option_value.price %} {{ option_value.price_prefix }} {{ option_value.price }} {% endif %}\" class=\"img-thumbnail\" /> {% endif %}                  
                    {{ option_value.name }}
                    {% if option_value.price %}
                    ({{ option_value.price_prefix }}{{ option_value.price }})
                    {% endif %} </label>
                </div>
                {% endfor %} </div>
            </div>
            {% endif %}
            {% if option.type == 'checkbox' %}
            <div class=\"col-6 form-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\">{{ option.name }}</label>
              <div id=\"input-option{{ option.product_option_id }}\"> {% for option_value in option.product_option_value %}
                <div class=\"checkbox\">
                  <label>
                    <input type=\"checkbox\" name=\"option[{{ option.product_option_id }}][]\" value=\"{{ option_value.product_option_value_id }}\" />
                    {% if option_value.image %} <img src=\"{{ option_value.image }}\" alt=\"{{ option_value.name }} {% if option_value.price %} {{ option_value.price_prefix }} {{ option_value.price }} {% endif %}\" class=\"img-thumbnail\" /> {% endif %}
                    {{ option_value.name }}
                    {% if option_value.price %}
                    ({{ option_value.price_prefix }}{{ option_value.price }})
                    {% endif %} </label>
                </div>
                {% endfor %} </div>
            </div>
            {% endif %}
            {% if option.type == 'text' %}
            <div class=\"col-6 form-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <input type=\"text\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option.value }}\" placeholder=\"{{ option.name }}\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\" />
            </div>
            {% endif %}
            {% if option.type == 'textarea' %}
            <div class=\"col-6 form-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <textarea name=\"option[{{ option.product_option_id }}]\" rows=\"5\" placeholder=\"{{ option.name }}\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\">{{ option.value }}</textarea>
            </div>
            {% endif %}
            {% if option.type == 'file' %}
            <div class=\"col-6 form-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\">{{ option.name }}</label>
              <button type=\"button\" id=\"button-upload{{ option.product_option_id }}\" data-loading-text=\"{{ text_loading }}\" class=\"btn btn-default btn-block\"><i class=\"fa fa-upload\"></i> {{ button_upload }}</button>
              <input type=\"hidden\" name=\"option[{{ option.product_option_id }}]\" value=\"\" id=\"input-option{{ option.product_option_id }}\" />
            </div>
            {% endif %}
            {% if option.type == 'date' %}
            <div class=\"col-6 form-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <div class=\"input-group date\">
                <input type=\"text\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option.value }}\" data-date-format=\"YYYY-MM-DD\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            {% endif %}
            {% if option.type == 'datetime' %}
            <div class=\"col-6 form-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <div class=\"input-group datetime\">
                <input type=\"text\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option.value }}\" data-date-format=\"YYYY-MM-DD HH:mm\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            {% endif %}
            {% if option.type == 'time' %}
            <div class=\"col-6 form-group{% if option.required %} required {% endif %}\">
              <label class=\"control-label\" for=\"input-option{{ option.product_option_id }}\">{{ option.name }}</label>
              <div class=\"input-group time\">
                <input type=\"text\" name=\"option[{{ option.product_option_id }}]\" value=\"{{ option.value }}\" data-date-format=\"HH:mm\" id=\"input-option{{ option.product_option_id }}\" class=\"form-control\" />
                <span class=\"input-group-btn\">
                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                </span></div>
            </div>
            {% endif %}
            {% endfor %}
            {% endif %}
            {% if recurrings %}
            <hr>
            <h3>{{ text_payment_recurring }}</h3>
            <div class=\"col-6 form-group required\">
              <select name=\"recurring_id\" class=\"form-control\">
                <option value=\"\">{{ text_select }}</option>
                {% for recurring in recurrings %}
                <option value=\"{{ recurring.recurring_id }}\">{{ recurring.name }}</option>
                {% endfor %}
              </select>
              <div class=\"help-block\" id=\"recurring-description\"></div>
            </div>
            {% endif %}
              <div class=\"row py-4 align-items-center\">
                <div class=\"col-6\">
                    {{ stock }}
                </div>
                <div class=\"col-6\">
                    <button type=\"button\" id=\"button-cart\" data-loading-text=\"{{ text_loading }}\">{{ button_cart }}</button>
                </div>
              </div>            
            </div>
              <ul class=\"list-unstyled\">
                <li>{{ text_model }} <b>{{ model }}</b></li>
                {% if manufacturer %}
                <li>{{ text_manufacturer }} <b>{{ manufacturer }}</b></li>
                {% endif %}            
                {% if reward %}
                <li>{{ text_reward }} {{ reward }}</li>
                {% endif %}
              </ul>          
              <div class=\"product-page-desc\">{{ description }}</div>
          </div>
      </div>
      </div>
        {% if tags %}
        <p>{{ text_tags }}
        {% for i in 0..tags|length - 1 %}
        {% if i < (tags|length - 1) %} <a href=\"{{ tags[i].href }}\">{{ tags[i].tag }}</a>,
        {% else %} <a href=\"{{ tags[i].href }}\">{{ tags[i].tag }}</a> {% endif %}
        {% endfor %} </p>
        {% endif %}
      {{ content_bottom }}</div>    
    {{ column_right }}</div>
</div>
      {% if products %}
      <div id=\"related-products\">
        <div class=\"related-products-inner container\">
          <h3>{{ text_related }}</h3> 
          <div class=\"row\"> {% set i = 0 %}
            {% for product in products %}
            <div class=\"product-layout product-grid col-md-4\">
              <div class=\"product-thumb\">
                <div class=\"image\"><a href=\"{{ product.href }}\"><img src=\"{{ product.thumb }}\" alt=\"{{ product.name }}\" title=\"{{ product.name }}\" class=\"img-responsive\" /></a></div>
                <div class=\"product-card-info\">
                  <div class=\"row product-card-topinfo\">
                    <div class=\"col-7 product-card-name\">
                        <a href=\"{{ product.href }}\">{{ product.name }}</a>
                    </div>
                    <div class=\"col-5 product-card-price text-md-right\"> 
                    {% if product.price %}
                    {% if not product.special %}
                      {{ product.price }}
                      {% else %} <span class=\"price-new\">{{ product.special }}</span> <span class=\"price-old\">{{ product.price }}</span> {% endif %}
                      {% if product.tax %} <span class=\"price-tax\">{{ text_tax }} {{ product.tax }}</span> {% endif %}
                    {% endif %}/кг
                    </div>
                  </div>
                  <div class=\"product-card-desc\">{{ product.description }}</div>
                  <div class=\"button-group\">
                    <button type=\"button\" onclick=\"cart.add('{{ product.product_id }}', '{{ product.minimum }}');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">{{ button_cart }}</span></button>
                  </div>
                </div>
              </div>
            </div>
            {% if column_left and column_right and (i + 1) % 2 == 0 %}
            <div class=\"clearfix visible-md visible-sm\"></div>
            {% elseif column_left or column_right and (i + 1) % 3 == 0 %}
            <div class=\"clearfix visible-md\"></div>
            {% elseif (i + 1) % 4 == 0 %}
            <div class=\"clearfix visible-md\"></div>
            {% endif %}
            {% set i = i + 1 %}
            {% endfor %} </div>
            </div>
        </div>
        {% endif %}  
<script type=\"text/javascript\"><!--
\$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
\t\$.ajax({
\t\turl: 'index.php?route=product/product/getRecurringDescription',
\t\ttype: 'post',
\t\tdata: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#recurring-description').html('');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();

\t\t\tif (json['success']) {
\t\t\t\t\$('#recurring-description').html(json['success']);
\t\t\t}
\t\t}
\t});
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('#button-cart').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=checkout/cart/add',
\t\ttype: 'post',
\t\tdata: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#button-cart').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-cart').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');

\t\t\tif (json['error']) {
\t\t\t\tif (json['error']['option']) {
\t\t\t\t\tfor (i in json['error']['option']) {
\t\t\t\t\t\tvar element = \$('#input-option' + i.replace('_', '-'));

\t\t\t\t\t\tif (element.parent().hasClass('input-group')) {
\t\t\t\t\t\t\telement.parent().after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\telement.after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\tif (json['error']['recurring']) {
\t\t\t\t\t\$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
\t\t\t\t}

\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().addClass('has-error');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('.breadcrumb').after('<div class=\"alert alert-success alert-dismissible\">' + json['success'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');

\t\t\t\t\$('#cart > button').html('<span id=\"cart-total\"><i class=\"fa fa-shopping-cart\"></i> ' + json['total'] + '</span>');

\t\t\t\t\$('html, body').animate({ scrollTop: 0 }, 'slow');

\t\t\t\t\$('#cart > ul').load('index.php?route=common/cart/info ul li');
\t\t\t}
\t\t},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
\t});
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('.date').datetimepicker({
\tlanguage: '{{ datepicker }}',
\tpickTime: false
});

\$('.datetime').datetimepicker({
\tlanguage: '{{ datepicker }}',
\tpickDate: true,
\tpickTime: true
});

\$('.time').datetimepicker({
\tlanguage: '{{ datepicker }}',
\tpickDate: false
});

\$('button[id^=\\'button-upload\\']').on('click', function() {
\tvar node = this;

\t\$('#form-upload').remove();

\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

\t\$('#form-upload input[name=\\'file\\']').trigger('click');

\tif (typeof timer != 'undefined') {
    \tclearInterval(timer);
\t}

\ttimer = setInterval(function() {
\t\tif (\$('#form-upload input[name=\\'file\\']').val() != '') {
\t\t\tclearInterval(timer);

\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=tool/upload',
\t\t\t\ttype: 'post',
\t\t\t\tdataType: 'json',
\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\tcache: false,
\t\t\t\tcontentType: false,
\t\t\t\tprocessData: false,
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\$(node).button('loading');
\t\t\t\t},
\t\t\t\tcomplete: function() {
\t\t\t\t\t\$(node).button('reset');
\t\t\t\t},
\t\t\t\tsuccess: function(json) {
\t\t\t\t\t\$('.text-danger').remove();

\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\t\$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
\t\t\t\t\t}

\t\t\t\t\tif (json['success']) {
\t\t\t\t\t\talert(json['success']);

\t\t\t\t\t\t\$(node).parent().find('input').val(json['code']);
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t}
\t\t\t});
\t\t}
\t}, 500);
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    \$('#review').fadeOut('slow');

    \$('#review').load(this.href);

    \$('#review').fadeIn('slow');
});

\$('#review').load('index.php?route=product/product/review&product_id={{ product_id }}');

\$('#button-review').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=product/product/write&product_id={{ product_id }}',
\t\ttype: 'post',
\t\tdataType: 'json',
\t\tdata: \$(\"#form-review\").serialize(),
\t\tbeforeSend: function() {
\t\t\t\$('#button-review').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-review').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible').remove();

\t\t\tif (json['error']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '</div>');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

\t\t\t\t\$('input[name=\\'name\\']').val('');
\t\t\t\t\$('textarea[name=\\'text\\']').val('');
\t\t\t\t\$('input[name=\\'rating\\']:checked').prop('checked', false);
\t\t\t}
\t\t}
\t});
});

\$(document).ready(function() {
\t\$('.thumbnails').magnificPopup({
\t\ttype:'image',
\t\tdelegate: 'a',
\t\tgallery: {
\t\t\tenabled: true
\t\t}
\t});
});
//--></script> 
{{ footer }} 
", "default/template/product/product.twig", "");
    }
}
