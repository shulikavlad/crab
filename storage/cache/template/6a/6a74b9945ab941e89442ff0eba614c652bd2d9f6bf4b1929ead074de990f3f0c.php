<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/cart.twig */
class __TwigTemplate_669a0ce666d147eb7cc576d6ec6e95f2c91af342db81871d2e857b72bbd18e46 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"cart\">
  <img src=\"/image/catalog/icons/Groupbusket";
        // line 2
        if (((($context["home_url"] ?? null) != "/") && (($context["home_url"] ?? null) != "/index.php?route=common/home"))) {
            echo "is-home";
        }
        echo ".svg\"> <span id=\"cart-total\">";
        echo ($context["text_items"] ?? null);
        echo "</span>
</div>
";
    }

    public function getTemplateName()
    {
        return "default/template/common/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"cart\">
  <img src=\"/image/catalog/icons/Groupbusket{% if (home_url != '/' and home_url != '/index.php?route=common/home') %}is-home{% endif %}.svg\"> <span id=\"cart-total\">{{ text_items }}</span>
</div>
", "default/template/common/cart.twig", "");
    }
}
