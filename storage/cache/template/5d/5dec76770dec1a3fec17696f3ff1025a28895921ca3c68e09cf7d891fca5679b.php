<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/tmdheader.twig */
class __TwigTemplate_30f66652036401e63cc51ba4f09f78df25f04ad738a10715272dcd3bc4abd3d9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"sidebar-container collapse py-3\" id=\"pageSubmenu\">
      <ul class=\"sidebar-navigation\">
\t  ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 9
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 9) ==  -1)) {
                    // line 10
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 11
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 11)) {
                            // line 12
                            echo "                <li class=\"nav-item dropdown\">
                  <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            // line 13
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 13);
                            echo "</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    ";
                            // line 15
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 15), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 15)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 15), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 16
                                echo "                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 17
                                    echo "                        <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 17);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 17);
                                    echo "</a>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 19
                                echo "                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 20
                            echo "                  </div>
                </li>
                ";
                        } else {
                            // line 23
                            echo "                <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 23);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 23);
                            echo "</a></li>
                ";
                        }
                        // line 25
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 26
                    echo "            ";
                } else {
                    // line 27
                    echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 27);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 27);
                    echo "</a>
                ";
                    // line 28
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 28)) {
                        // line 29
                        echo "                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 32
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 32));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 33
                            echo "                    <li>
                        ";
                            // line 34
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 34)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 35
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 35);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 35);
                                echo "</a>
                        ";
                            } else {
                                // line 37
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 37);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 37);
                                echo "</a>\t
                        ";
                            }
                            // line 39
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 39)) {
                                // line 40
                                echo "                    
                    <ul>
                    ";
                                // line 42
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 42));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 43
                                    echo "                    <li>
                        ";
                                    // line 44
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 44)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 45
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 45);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 45);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 47
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 47);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 47);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 49
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 51
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 54
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 56
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 60
                    echo "\t
                </li>\t
            ";
                }
                // line 62
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "      </ul>
      <div class=\"mobile-contacts ps-4\">
        <div class=\"mobile-contact-phone mb-2\">
            <img src=\"/image/catalog/icons/phone-call-white.svg\"> ";
            // line 67
            echo ($context["telephone"] ?? null);
            echo "
        </div>
        <div class=\"mobile-contact-phone\">
            <img src=\"/image/catalog/icons/clock-white.svg\"> ";
            // line 70
            echo ($context["open"] ?? null);
            echo "
        </div>
      </div>
    </div>    
    <div class=\"mobile-sidebar-overlay\"></div>
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  ";
            // line 77
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 78
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 78) ==  -1)) {
                    // line 79
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 80
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 80)) {
                            // line 81
                            echo "                <li class=\"nav-item dropdown\">
                  <a href=\"";
                            // line 82
                            echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 82);
                            echo "\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 82);
                            echo "</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    ";
                            // line 84
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 84), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 84)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 84), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 85
                                echo "                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 86
                                    echo "                        <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 86);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 86);
                                    echo "</a>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 88
                                echo "                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 89
                            echo "                  </div>
                </li>
                ";
                        } else {
                            // line 92
                            echo "                <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 92);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 92);
                            echo "</a></li>
                ";
                        }
                        // line 94
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 95
                    echo "            ";
                } else {
                    // line 96
                    echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 96);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 96);
                    echo "</a>
                ";
                    // line 97
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 97)) {
                        // line 98
                        echo "                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 101
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 101));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 102
                            echo "                    <li>
                        ";
                            // line 103
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 103)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 104
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 104);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 104);
                                echo "</a>
                        ";
                            } else {
                                // line 106
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 106);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 106);
                                echo "</a>\t
                        ";
                            }
                            // line 108
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 108)) {
                                // line 109
                                echo "                    
                    <ul>
                    ";
                                // line 111
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 111));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 112
                                    echo "                    <li>
                        ";
                                    // line 113
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 113)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 114
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 114);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 114);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 116
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 116);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 116);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 118
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 120
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 123
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 125
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 129
                    echo "\t
                </li>\t
            ";
                }
                // line 131
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 133
            echo "      </ul>
    </div>
  </nav>
";
        }
        // line 136
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/extension/tmdheader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  442 => 136,  436 => 133,  429 => 131,  424 => 129,  417 => 125,  410 => 123,  405 => 120,  398 => 118,  390 => 116,  383 => 114,  379 => 113,  376 => 112,  372 => 111,  368 => 109,  365 => 108,  357 => 106,  350 => 104,  346 => 103,  343 => 102,  339 => 101,  334 => 98,  332 => 97,  325 => 96,  322 => 95,  316 => 94,  308 => 92,  303 => 89,  297 => 88,  286 => 86,  281 => 85,  277 => 84,  270 => 82,  267 => 81,  264 => 80,  259 => 79,  257 => 78,  251 => 77,  241 => 70,  235 => 67,  230 => 64,  223 => 62,  218 => 60,  211 => 56,  204 => 54,  199 => 51,  192 => 49,  184 => 47,  177 => 45,  173 => 44,  170 => 43,  166 => 42,  162 => 40,  159 => 39,  151 => 37,  144 => 35,  140 => 34,  137 => 33,  133 => 32,  128 => 29,  126 => 28,  119 => 27,  116 => 26,  110 => 25,  102 => 23,  97 => 20,  91 => 19,  80 => 17,  75 => 16,  71 => 15,  66 => 13,  63 => 12,  60 => 11,  55 => 10,  53 => 9,  47 => 8,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if categories %}
  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"sidebar-container collapse py-3\" id=\"pageSubmenu\">
      <ul class=\"sidebar-navigation\">
\t  {% for header in headermenu %} 
            {% if header.column == -1 %}
                {% for category in categories %}
                {% if category.children %}
                <li class=\"nav-item dropdown\">
                  <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                        {% for child in children %}
                        <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                        {% endfor %}
                      {% endfor %}
                  </div>
                </li>
                {% else %}
                <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
            {% else %}
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                {% if header.sub_title %}
                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    {% for subtitle in header.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                        {% if header.sub_title %}
                    
                    <ul>
                    {% for subtitle in subtitle.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                </li>
                    {% endfor %}
                        
                    </ul>\t\t\t\t
                    {% endif %}
                </li>
                    {% endfor %}
                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    {% endif %}\t
                </li>\t
            {% endif %}                
\t\t\t{% endfor %}
      </ul>
      <div class=\"mobile-contacts ps-4\">
        <div class=\"mobile-contact-phone mb-2\">
            <img src=\"/image/catalog/icons/phone-call-white.svg\"> {{ telephone }}
        </div>
        <div class=\"mobile-contact-phone\">
            <img src=\"/image/catalog/icons/clock-white.svg\"> {{ open }}
        </div>
      </div>
    </div>    
    <div class=\"mobile-sidebar-overlay\"></div>
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  {% for header in headermenu %} 
            {% if header.column == -1 %}
                {% for category in categories %}
                {% if category.children %}
                <li class=\"nav-item dropdown\">
                  <a href=\"{{ header.link }}\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                        {% for child in children %}
                        <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                        {% endfor %}
                      {% endfor %}
                  </div>
                </li>
                {% else %}
                <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
            {% else %}
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                {% if header.sub_title %}
                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    {% for subtitle in header.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                        {% if header.sub_title %}
                    
                    <ul>
                    {% for subtitle in subtitle.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                </li>
                    {% endfor %}
                        
                    </ul>\t\t\t\t
                    {% endif %}
                </li>
                    {% endfor %}
                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    {% endif %}\t
                </li>\t
            {% endif %}                
\t\t\t{% endfor %}
      </ul>
    </div>
  </nav>
{% endif %} ", "default/template/extension/tmdheader.twig", "");
    }
}
