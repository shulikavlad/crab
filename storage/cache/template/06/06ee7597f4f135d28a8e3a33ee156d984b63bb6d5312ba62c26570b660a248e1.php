<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/tmdheader.twig */
class __TwigTemplate_d9ed53855f93a202b11fe59cf0369e8ab02ab81313e44d79a1275377d5320dd6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"sidebar-container collapse\" id=\"pageSubmenu\">
      <div class=\"sidebar-logo text-center px-0 py-2\">
        <img src=\"/image/catalog/brandLogo.png\" width=\"50\" height=\"50\">
      </div>
      <ul class=\"sidebar-navigation\">
\t  ";
            // line 11
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 12
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 12) ==  -1)) {
                    // line 13
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 14
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 14)) {
                            // line 15
                            echo "                <li class=\"nav-item dropdown\">
                  <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            // line 16
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 16);
                            echo "</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    ";
                            // line 18
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 18), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 18)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 18), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 19
                                echo "                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 20
                                    echo "                        <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 20);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 20);
                                    echo "</a>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 22
                                echo "                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 23
                            echo "                  </div>
                </li>
                ";
                        } else {
                            // line 26
                            echo "                <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 26);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 26);
                            echo "</a></li>
                ";
                        }
                        // line 28
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 29
                    echo "            ";
                } else {
                    // line 30
                    echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 30);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 30);
                    echo "</a>
                ";
                    // line 31
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 31)) {
                        // line 32
                        echo "                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 35
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 35));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 36
                            echo "                    <li>
                        ";
                            // line 37
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 37)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 38
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 38);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 38);
                                echo "</a>
                        ";
                            } else {
                                // line 40
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 40);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 40);
                                echo "</a>\t
                        ";
                            }
                            // line 42
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 42)) {
                                // line 43
                                echo "                    
                    <ul>
                    ";
                                // line 45
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 45));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 46
                                    echo "                    <li>
                        ";
                                    // line 47
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 47)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 48
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 48);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 48);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 50
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 50);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 50);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 52
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 54
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 57
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 59
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 63
                    echo "\t
                </li>\t
            ";
                }
                // line 65
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "      </ul>      
    </div>    
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  ";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 72
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 72) ==  -1)) {
                    // line 73
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 74
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 74)) {
                            // line 75
                            echo "                <li class=\"nav-item dropdown\">
                  <a href=\"";
                            // line 76
                            echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 76);
                            echo "\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 76);
                            echo "</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    ";
                            // line 78
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 78), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 78)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 78), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 79
                                echo "                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 80
                                    echo "                        <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 80);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 80);
                                    echo "</a>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 82
                                echo "                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 83
                            echo "                  </div>
                </li>
                ";
                        } else {
                            // line 86
                            echo "                <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 86);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 86);
                            echo "</a></li>
                ";
                        }
                        // line 88
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 89
                    echo "            ";
                } else {
                    // line 90
                    echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 90);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 90);
                    echo "</a>
                ";
                    // line 91
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 91)) {
                        // line 92
                        echo "                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 95
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 95));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 96
                            echo "                    <li>
                        ";
                            // line 97
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 97)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 98
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 98);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 98);
                                echo "</a>
                        ";
                            } else {
                                // line 100
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 100);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 100);
                                echo "</a>\t
                        ";
                            }
                            // line 102
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 102)) {
                                // line 103
                                echo "                    
                    <ul>
                    ";
                                // line 105
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 105));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 106
                                    echo "                    <li>
                        ";
                                    // line 107
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 107)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 108
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 108);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 108);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 110
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 110);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 110);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 112
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 114
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 117
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 119
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 123
                    echo "\t
                </li>\t
            ";
                }
                // line 125
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 127
            echo "      </ul>
    </div>
  </nav>
";
        }
        // line 130
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/extension/tmdheader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  430 => 130,  424 => 127,  417 => 125,  412 => 123,  405 => 119,  398 => 117,  393 => 114,  386 => 112,  378 => 110,  371 => 108,  367 => 107,  364 => 106,  360 => 105,  356 => 103,  353 => 102,  345 => 100,  338 => 98,  334 => 97,  331 => 96,  327 => 95,  322 => 92,  320 => 91,  313 => 90,  310 => 89,  304 => 88,  296 => 86,  291 => 83,  285 => 82,  274 => 80,  269 => 79,  265 => 78,  258 => 76,  255 => 75,  252 => 74,  247 => 73,  245 => 72,  239 => 71,  233 => 67,  226 => 65,  221 => 63,  214 => 59,  207 => 57,  202 => 54,  195 => 52,  187 => 50,  180 => 48,  176 => 47,  173 => 46,  169 => 45,  165 => 43,  162 => 42,  154 => 40,  147 => 38,  143 => 37,  140 => 36,  136 => 35,  131 => 32,  129 => 31,  122 => 30,  119 => 29,  113 => 28,  105 => 26,  100 => 23,  94 => 22,  83 => 20,  78 => 19,  74 => 18,  69 => 16,  66 => 15,  63 => 14,  58 => 13,  56 => 12,  50 => 11,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if categories %}
  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"sidebar-container collapse\" id=\"pageSubmenu\">
      <div class=\"sidebar-logo text-center px-0 py-2\">
        <img src=\"/image/catalog/brandLogo.png\" width=\"50\" height=\"50\">
      </div>
      <ul class=\"sidebar-navigation\">
\t  {% for header in headermenu %} 
            {% if header.column == -1 %}
                {% for category in categories %}
                {% if category.children %}
                <li class=\"nav-item dropdown\">
                  <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                        {% for child in children %}
                        <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                        {% endfor %}
                      {% endfor %}
                  </div>
                </li>
                {% else %}
                <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
            {% else %}
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                {% if header.sub_title %}
                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    {% for subtitle in header.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                        {% if header.sub_title %}
                    
                    <ul>
                    {% for subtitle in subtitle.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                </li>
                    {% endfor %}
                        
                    </ul>\t\t\t\t
                    {% endif %}
                </li>
                    {% endfor %}
                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    {% endif %}\t
                </li>\t
            {% endif %}                
\t\t\t{% endfor %}
      </ul>      
    </div>    
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  {% for header in headermenu %} 
            {% if header.column == -1 %}
                {% for category in categories %}
                {% if category.children %}
                <li class=\"nav-item dropdown\">
                  <a href=\"{{ header.link }}\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                        {% for child in children %}
                        <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                        {% endfor %}
                      {% endfor %}
                  </div>
                </li>
                {% else %}
                <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
            {% else %}
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                {% if header.sub_title %}
                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    {% for subtitle in header.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                        {% if header.sub_title %}
                    
                    <ul>
                    {% for subtitle in subtitle.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                </li>
                    {% endfor %}
                        
                    </ul>\t\t\t\t
                    {% endif %}
                </li>
                    {% endfor %}
                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    {% endif %}\t
                </li>\t
            {% endif %}                
\t\t\t{% endfor %}
      </ul>
    </div>
  </nav>
{% endif %} ", "default/template/extension/tmdheader.twig", "");
    }
}
