<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/extension/tmdheader.twig */
class __TwigTemplate_5649eeb24eb0520a32f7ff52de383df1bcc6001028ffc4fe0895ab8534f9782b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["categories"] ?? null)) {
            // line 2
            echo "  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"sidebar-container collapse\" id=\"pageSubmenu\">
      <ul class=\"sidebar-navigation\">
\t  ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 9
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 9) ==  -1)) {
                    // line 10
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 11
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 11)) {
                            // line 12
                            echo "                <li class=\"nav-item dropdown\">
                  <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            // line 13
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 13);
                            echo "</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    ";
                            // line 15
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 15), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 15)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 15), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 16
                                echo "                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 17
                                    echo "                        <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 17);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 17);
                                    echo "</a>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 19
                                echo "                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 20
                            echo "                  </div>
                </li>
                ";
                        } else {
                            // line 23
                            echo "                <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 23);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 23);
                            echo "</a></li>
                ";
                        }
                        // line 25
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 26
                    echo "            ";
                } else {
                    // line 27
                    echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 27);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 27);
                    echo "</a>
                ";
                    // line 28
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 28)) {
                        // line 29
                        echo "                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 32
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 32));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 33
                            echo "                    <li>
                        ";
                            // line 34
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 34)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 35
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 35);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 35);
                                echo "</a>
                        ";
                            } else {
                                // line 37
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 37);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 37);
                                echo "</a>\t
                        ";
                            }
                            // line 39
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 39)) {
                                // line 40
                                echo "                    
                    <ul>
                    ";
                                // line 42
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 42));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 43
                                    echo "                    <li>
                        ";
                                    // line 44
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 44)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 45
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 45);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 45);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 47
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 47);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 47);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 49
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 51
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 54
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 56
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 60
                    echo "\t
                </li>\t
            ";
                }
                // line 62
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "      </ul>      
    </div>    
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  ";
            // line 68
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                echo " 
            ";
                // line 69
                if ((twig_get_attribute($this->env, $this->source, $context["header"], "column", [], "any", false, false, false, 69) ==  -1)) {
                    // line 70
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 71
                        echo "                ";
                        if (twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 71)) {
                            // line 72
                            echo "                <li class=\"nav-item dropdown\">
                  <a href=\"";
                            // line 73
                            echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 73);
                            echo "\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 73);
                            echo "</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    ";
                            // line 75
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_batch(twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 75), (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "children", [], "any", false, false, false, 75)) / twig_round(twig_get_attribute($this->env, $this->source, $context["category"], "column", [], "any", false, false, false, 75), 1, "ceil"))));
                            foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                // line 76
                                echo "                        ";
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($context["children"]);
                                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                    // line 77
                                    echo "                        <a class=\"dropdown-item\" href=\"";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "href", [], "any", false, false, false, 77);
                                    echo "\">";
                                    echo twig_get_attribute($this->env, $this->source, $context["child"], "name", [], "any", false, false, false, 77);
                                    echo "</a>
                        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 79
                                echo "                      ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 80
                            echo "                  </div>
                </li>
                ";
                        } else {
                            // line 83
                            echo "                <li><a class=\"nav-link\" href=\"";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "href", [], "any", false, false, false, 83);
                            echo "\">";
                            echo twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 83);
                            echo "</a></li>
                ";
                        }
                        // line 85
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 86
                    echo "            ";
                } else {
                    // line 87
                    echo "                <li class=\"nav-item\"><a class=\"nav-link\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "link", [], "any", false, false, false, 87);
                    echo "\">";
                    echo twig_get_attribute($this->env, $this->source, $context["header"], "title", [], "any", false, false, false, 87);
                    echo "</a>
                ";
                    // line 88
                    if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 88)) {
                        // line 89
                        echo "                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    ";
                        // line 92
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 92));
                        foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                            // line 93
                            echo "                    <li>
                        ";
                            // line 94
                            if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 94)) {
                                echo "\t\t\t\t
                        <a href=\"";
                                // line 95
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 95);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 95);
                                echo "</a>
                        ";
                            } else {
                                // line 97
                                echo "                        <a href=\"";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 97);
                                echo "\">";
                                echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 97);
                                echo "</a>\t
                        ";
                            }
                            // line 99
                            echo "                        ";
                            if (twig_get_attribute($this->env, $this->source, $context["header"], "sub_title", [], "any", false, false, false, 99)) {
                                // line 100
                                echo "                    
                    <ul>
                    ";
                                // line 102
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["subtitle"], "sub_title", [], "any", false, false, false, 102));
                                foreach ($context['_seq'] as $context["_key"] => $context["subtitle"]) {
                                    // line 103
                                    echo "                    <li>
                        ";
                                    // line 104
                                    if (twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 104)) {
                                        echo "\t\t\t\t\t\t
                        <a href=\"";
                                        // line 105
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "href", [], "any", false, false, false, 105);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 105);
                                        echo "</a>
                        ";
                                    } else {
                                        // line 107
                                        echo "                        <a href=\"";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "link", [], "any", false, false, false, 107);
                                        echo "\">";
                                        echo twig_get_attribute($this->env, $this->source, $context["subtitle"], "title", [], "any", false, false, false, 107);
                                        echo "</a>\t
                        ";
                                    }
                                    // line 109
                                    echo "                </li>
                    ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 111
                                echo "                        
                    </ul>\t\t\t\t
                    ";
                            }
                            // line 114
                            echo "                </li>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subtitle'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 116
                        echo "                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    ";
                    }
                    // line 120
                    echo "\t
                </li>\t
            ";
                }
                // line 122
                echo "                
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 124
            echo "      </ul>
    </div>
  </nav>
";
        }
        // line 127
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/extension/tmdheader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  427 => 127,  421 => 124,  414 => 122,  409 => 120,  402 => 116,  395 => 114,  390 => 111,  383 => 109,  375 => 107,  368 => 105,  364 => 104,  361 => 103,  357 => 102,  353 => 100,  350 => 99,  342 => 97,  335 => 95,  331 => 94,  328 => 93,  324 => 92,  319 => 89,  317 => 88,  310 => 87,  307 => 86,  301 => 85,  293 => 83,  288 => 80,  282 => 79,  271 => 77,  266 => 76,  262 => 75,  255 => 73,  252 => 72,  249 => 71,  244 => 70,  242 => 69,  236 => 68,  230 => 64,  223 => 62,  218 => 60,  211 => 56,  204 => 54,  199 => 51,  192 => 49,  184 => 47,  177 => 45,  173 => 44,  170 => 43,  166 => 42,  162 => 40,  159 => 39,  151 => 37,  144 => 35,  140 => 34,  137 => 33,  133 => 32,  128 => 29,  126 => 28,  119 => 27,  116 => 26,  110 => 25,  102 => 23,  97 => 20,  91 => 19,  80 => 17,  75 => 16,  71 => 15,  66 => 13,  63 => 12,  60 => 11,  55 => 10,  53 => 9,  47 => 8,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if categories %}
  <nav id=\"menu\" class=\"navbar navbar-expand-lg\">
    <button class=\"navbar-toggler navbar-light\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#pageSubmenu\" aria-controls=\"pageSubmenu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
    </button>
    <div class=\"sidebar-container collapse\" id=\"pageSubmenu\">
      <ul class=\"sidebar-navigation\">
\t  {% for header in headermenu %} 
            {% if header.column == -1 %}
                {% for category in categories %}
                {% if category.children %}
                <li class=\"nav-item dropdown\">
                  <a href=\"#\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                        {% for child in children %}
                        <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                        {% endfor %}
                      {% endfor %}
                  </div>
                </li>
                {% else %}
                <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
            {% else %}
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                {% if header.sub_title %}
                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    {% for subtitle in header.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                        {% if header.sub_title %}
                    
                    <ul>
                    {% for subtitle in subtitle.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                </li>
                    {% endfor %}
                        
                    </ul>\t\t\t\t
                    {% endif %}
                </li>
                    {% endfor %}
                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    {% endif %}\t
                </li>\t
            {% endif %}                
\t\t\t{% endfor %}
      </ul>      
    </div>    
    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">
\t  {% for header in headermenu %} 
            {% if header.column == -1 %}
                {% for category in categories %}
                {% if category.children %}
                <li class=\"nav-item dropdown\">
                  <a href=\"{{ header.link }}\" class=\"nav-link dropdown-toggle\" id=\"navbarDropdownCats\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">{{ category.name }}</a>
                  <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownCats\">
                    {% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}
                        {% for child in children %}
                        <a class=\"dropdown-item\" href=\"{{ child.href }}\">{{ child.name }}</a>
                        {% endfor %}
                      {% endfor %}
                  </div>
                </li>
                {% else %}
                <li><a class=\"nav-link\" href=\"{{ category.href }}\">{{ category.name }}</a></li>
                {% endif %}
                {% endfor %}
            {% else %}
                <li class=\"nav-item\"><a class=\"nav-link\" href=\"{{ header.link }}\">{{ header.title }}</a>
                {% if header.sub_title %}
                    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\t
                    <div class=\"dropdown-inner\">
                    <ul class=\"list-unstyled\">
                    {% for subtitle in header.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                        {% if header.sub_title %}
                    
                    <ul>
                    {% for subtitle in subtitle.sub_title %}
                    <li>
                        {% if subtitle.href %}\t\t\t\t\t\t
                        <a href=\"{{ subtitle.href }}\">{{ subtitle.title }}</a>
                        {% else %}
                        <a href=\"{{ subtitle.link }}\">{{ subtitle.title }}</a>\t
                        {% endif %}
                </li>
                    {% endfor %}
                        
                    </ul>\t\t\t\t
                    {% endif %}
                </li>
                    {% endfor %}
                    </ul>\t\t\t\t
                    </div>
                    </div>
                    
                    {% endif %}\t
                </li>\t
            {% endif %}                
\t\t\t{% endfor %}
      </ul>
    </div>
  </nav>
{% endif %} ", "default/template/extension/tmdheader.twig", "");
    }
}
