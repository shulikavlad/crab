<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/template/common/footer.twig */
class __TwigTemplate_53f2312e4e9a82865c1cfead18a0ef29f7261c1b3d16cb067cb3ebf14c67fa2e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<footer>
  <div class=\"container\">
    <div class=\"row\">
      ";
        // line 4
        if (($context["logo"] ?? null)) {
            // line 5
            echo "      <div class=\"col-sm-2\">
        <img src=\"";
            // line 6
            echo ($context["logo"] ?? null);
            echo "\" width=\"70\" height=\"70\">
      </div>
      ";
        }
        // line 9
        echo "      <div class=\"col-sm-3\">
            <ul class=\"navbar-nav\">
                <li><a href=\"/\">Домашняя</a></li>\t
                <li><a href=\"/about_us\">О нас</a></li>\t
                <li><a href=\"/produkciya\">Продукция</a></li>
                <li><a href=\"/contact\">Контакты</a></li>\t
                <li><a href=\"/optovikam\">Оптовикам</a></li>\t
            </ul>
      </div>
      <div class=\"col-sm-3\">
        <ul class=\"list-unstyled\">
          <li><a href=\"/sposoby-oplaty\">Способы оплаты</a></li>
          <li><a href=\"/obmen-i-vozvrat\">Обмен и возврат</a></li>
          <li><a href=\"/sotrudnichestvo-s-nami\">Сотрудничество с нами</a></li>
          <li><a href=\"/vakansii\">Вакансии</a></li>
        </ul>
      </div>
      <div class=\"col-sm-4\">
        <ul class=\"list-unstyled\">
        ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["locations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
            // line 29
            echo "            <li><img src=\"/image/catalog/icons/location-pin.png\"> <a href=\"";
            echo ($context["account"] ?? null);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["location"], "address", [], "any", false, false, false, 29);
            echo " <b>";
            echo twig_get_attribute($this->env, $this->source, $context["location"], "open", [], "any", false, false, false, 29);
            echo "</b></a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </ul>
      </div>
    </div>
  </div>
  <div class=\"footer-copyright text-center\">КОРОЛЕВСКИЙ КРАБ © 2022. ALL RIGHTS RESERVED.</div>
</footer>
";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["styles"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 38
            echo "<link href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "href", [], "any", false, false, false, 38);
            echo "\" type=\"text/css\" rel=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "rel", [], "any", false, false, false, 38);
            echo "\" media=\"";
            echo twig_get_attribute($this->env, $this->source, $context["style"], "media", [], "any", false, false, false, 38);
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["scripts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 41
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<script type=\"text/javascript\" src=\"/catalog/view/javascript/slick/slick.min.js\"></script>
<!--script src=\"catalog/view/javascript/bootstrap/js/bootstrap.bundle.min.js\" type=\"text/javascript\"></script-->


<script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js\" integrity=\"sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p\" crossorigin=\"anonymous\"></script>
<!--script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script-->
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.bundle.min.js\" type=\"text/javascript\"></script>

<script>
    \$('.big-images').slick({
        slidesToShow: 1
    });
    \$('.store-card-inner').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        arrows: true,
        prevArrow: '<button class=\"slick-prev slick-arrow\" aria-label=\"Previous\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>',
        nextArrow: '<button class=\"slick-next slick-arrow\" aria-label=\"Next\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>'
    });
    
  \$(\".about-slider\").slick({
    dots: false,
    infinite: false,
    autoplay: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button class=\"slick-prev slick-arrow\" aria-label=\"Previous\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>',
    nextArrow: '<button class=\"slick-next slick-arrow\" aria-label=\"Next\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>'
  });    
</script>
</body></html>";
    }

    public function getTemplateName()
    {
        return "default/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 43,  120 => 41,  116 => 40,  103 => 38,  99 => 37,  91 => 31,  78 => 29,  74 => 28,  53 => 9,  47 => 6,  44 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer>
  <div class=\"container\">
    <div class=\"row\">
      {% if logo %}
      <div class=\"col-sm-2\">
        <img src=\"{{ logo }}\" width=\"70\" height=\"70\">
      </div>
      {% endif %}
      <div class=\"col-sm-3\">
            <ul class=\"navbar-nav\">
                <li><a href=\"/\">Домашняя</a></li>\t
                <li><a href=\"/about_us\">О нас</a></li>\t
                <li><a href=\"/produkciya\">Продукция</a></li>
                <li><a href=\"/contact\">Контакты</a></li>\t
                <li><a href=\"/optovikam\">Оптовикам</a></li>\t
            </ul>
      </div>
      <div class=\"col-sm-3\">
        <ul class=\"list-unstyled\">
          <li><a href=\"/sposoby-oplaty\">Способы оплаты</a></li>
          <li><a href=\"/obmen-i-vozvrat\">Обмен и возврат</a></li>
          <li><a href=\"/sotrudnichestvo-s-nami\">Сотрудничество с нами</a></li>
          <li><a href=\"/vakansii\">Вакансии</a></li>
        </ul>
      </div>
      <div class=\"col-sm-4\">
        <ul class=\"list-unstyled\">
        {% for location in locations %}
            <li><img src=\"/image/catalog/icons/location-pin.png\"> <a href=\"{{ account }}\">{{ location.address }} <b>{{ location.open }}</b></a></li>
        {% endfor %}
        </ul>
      </div>
    </div>
  </div>
  <div class=\"footer-copyright text-center\">КОРОЛЕВСКИЙ КРАБ © 2022. ALL RIGHTS RESERVED.</div>
</footer>
{% for style in styles %}
<link href=\"{{ style.href }}\" type=\"text/css\" rel=\"{{ style.rel }}\" media=\"{{ style.media }}\" />
{% endfor %}
{% for script in scripts %}
<script src=\"{{ script }}\" type=\"text/javascript\"></script>
{% endfor %}
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<script type=\"text/javascript\" src=\"/catalog/view/javascript/slick/slick.min.js\"></script>
<!--script src=\"catalog/view/javascript/bootstrap/js/bootstrap.bundle.min.js\" type=\"text/javascript\"></script-->


<script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js\" integrity=\"sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p\" crossorigin=\"anonymous\"></script>
<!--script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script-->
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.bundle.min.js\" type=\"text/javascript\"></script>

<script>
    \$('.big-images').slick({
        slidesToShow: 1
    });
    \$('.store-card-inner').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        arrows: true,
        prevArrow: '<button class=\"slick-prev slick-arrow\" aria-label=\"Previous\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>',
        nextArrow: '<button class=\"slick-next slick-arrow\" aria-label=\"Next\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>'
    });
    
  \$(\".about-slider\").slick({
    dots: false,
    infinite: false,
    autoplay: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button class=\"slick-prev slick-arrow\" aria-label=\"Previous\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>',
    nextArrow: '<button class=\"slick-next slick-arrow\" aria-label=\"Next\" type=\"button\" style=\"display: block;\"><img src=\"/image/catalog/icons/Arrow.svg\"></button>'
  });    
</script>
</body></html>", "default/template/common/footer.twig", "");
    }
}
