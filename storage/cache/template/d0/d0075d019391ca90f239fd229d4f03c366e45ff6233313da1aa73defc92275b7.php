<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* extension/headermenu_list.twig */
class __TwigTemplate_bb09b59b2f6aafd19003e4a62f865cb8fd3ab45181cbb55169a009a9686020be extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ($context["header"] ?? null);
        echo ($context["column_left"] ?? null);
        echo "
<div id=\"content\">
\t<div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\"><a href=\"";
        // line 5
        echo ($context["insert"] ?? null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo ($context["button_insert"] ?? null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
         <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo ($context["button_delete"] ?? null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo ($context["text_confirm"] ?? null);
        echo "') ? \$('#form-header').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
      </div>
      <h1>";
        // line 8
        echo ($context["heading_title"] ?? null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "href", [], "any", false, false, false, 11);
            echo "\">";
            echo twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "text", [], "any", false, false, false, 11);
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
\t";
        // line 17
        if (($context["error_warning"] ?? null)) {
            // line 18
            echo "    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo ($context["error_warning"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    ";
        if (($context["success"] ?? null)) {
            // line 23
            echo "    <div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo ($context["success"] ?? null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 27
        echo "\t<div class=\"panel panel-default\">
      <form action=\"";
        // line 28
        echo ($context["delete"] ?? null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-header\">
        <div class=\"table-responsive\">     
\t\t<table class=\"table table-bordered table-hover\">
          <thead>
            <tr>
              <td width=\"1\" style=\"text-align: center;\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').attr('checked', this.checked);\" /></td>
              <td class=\"left\">";
        // line 34
        if ((($context["sort"] ?? null) == "id.title")) {
            // line 35
            echo "                <a href=\"";
            echo ($context["sort_title"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_title"] ?? null);
            echo "</a>
                ";
        } else {
            // line 37
            echo "                <a href=\"";
            echo ($context["sort_title"] ?? null);
            echo "\">";
            echo ($context["column_title"] ?? null);
            echo "</a>
                ";
        }
        // line 38
        echo "</td> 
\t\t\t\t
              <td class=\"right\">";
        // line 40
        if ((($context["sort"] ?? null) == "i.sort_order")) {
            // line 41
            echo "                <a href=\"";
            echo ($context["sort_sort_order"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_link"] ?? null);
            echo "</a>
                ";
        } else {
            // line 43
            echo "                <a href=\"";
            echo ($context["sort_sort_order"] ?? null);
            echo "\">";
            echo ($context["column_link"] ?? null);
            echo "</a>
                ";
        }
        // line 44
        echo "</td>
\t\t\t\t
\t\t\t\t<td class=\"left\">";
        // line 46
        if ((($context["sort"] ?? null) == "id.title")) {
            // line 47
            echo "                <a href=\"";
            echo ($context["sort_sort_order"] ?? null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, ($context["order"] ?? null));
            echo "\">";
            echo ($context["column_sort_order"] ?? null);
            echo "</a>
                ";
        } else {
            // line 49
            echo "                <a href=\"";
            echo ($context["sort_title"] ?? null);
            echo "\">";
            echo ($context["column_sort_order"] ?? null);
            echo "</a>
                ";
        }
        // line 50
        echo "</td>
              <td class=\"right\">";
        // line 51
        echo ($context["column_action"] ?? null);
        echo "</td>
            </tr>
          </thead>
          <tbody>
            ";
        // line 55
        if (($context["headermenus"] ?? null)) {
            // line 56
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["headermenus"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["headermenu"]) {
                // line 57
                echo "            <tr>
              <td style=\"text-align: center;\">";
                // line 58
                if (twig_in_filter($context["headermenu"], ($context["selected"] ?? null))) {
                    // line 59
                    echo "                <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["headermenu"], "headermenu_id", [], "any", false, false, false, 59);
                    echo "\" checked=\"checked\" />
                ";
                } else {
                    // line 61
                    echo "                <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["headermenu"], "headermenu_id", [], "any", false, false, false, 61);
                    echo "\" />
                ";
                }
                // line 62
                echo "</td>
              <td class=\"left\">";
                // line 63
                echo twig_get_attribute($this->env, $this->source, $context["headermenu"], "title", [], "any", false, false, false, 63);
                echo "</td>
              <td class=\"right\">";
                // line 64
                echo twig_get_attribute($this->env, $this->source, $context["headermenu"], "link", [], "any", false, false, false, 64);
                echo "</td>
              <td class=\"right\">";
                // line 65
                echo twig_get_attribute($this->env, $this->source, $context["headermenu"], "sort_order", [], "any", false, false, false, 65);
                echo "</td>
              <td class=\"right\">";
                // line 66
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["headermenu"], "action", [], "any", false, false, false, 66));
                foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
                    // line 67
                    echo "               <a class=\"btn btn-primary\" href=\"";
                    echo twig_get_attribute($this->env, $this->source, $context["action"], "href", [], "any", false, false, false, 67);
                    echo "\"><i class=\"fa fa-pencil\"></i></a></td>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 68
                echo "</td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['headermenu'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "            ";
        } else {
            // line 72
            echo "            <tr>
              <td class=\"center\" colspan=\"6\">";
            // line 73
            echo ($context["text_no_results"] ?? null);
            echo "</td>
            </tr>
           ";
        }
        // line 76
        echo "          </tbody>
        </table>
\t\t</div>
      </form>
      <div class=\"row\">
         <div class=\"col-sm-6 text-left\">";
        // line 81
        echo ($context["pagination"] ?? null);
        echo "</div>
          <div class=\"col-sm-6 text-right\">";
        // line 82
        echo ($context["results"] ?? null);
        echo "</div>
        </div>
  </div>
</div>
</div>
";
        // line 87
        echo ($context["footer"] ?? null);
    }

    public function getTemplateName()
    {
        return "extension/headermenu_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  286 => 87,  278 => 82,  274 => 81,  267 => 76,  261 => 73,  258 => 72,  255 => 71,  247 => 68,  238 => 67,  234 => 66,  230 => 65,  226 => 64,  222 => 63,  219 => 62,  213 => 61,  207 => 59,  205 => 58,  202 => 57,  197 => 56,  195 => 55,  188 => 51,  185 => 50,  177 => 49,  167 => 47,  165 => 46,  161 => 44,  153 => 43,  143 => 41,  141 => 40,  137 => 38,  129 => 37,  119 => 35,  117 => 34,  108 => 28,  105 => 27,  97 => 23,  94 => 22,  86 => 18,  84 => 17,  78 => 13,  67 => 11,  63 => 10,  58 => 8,  51 => 6,  45 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "extension/headermenu_list.twig", "");
    }
}
