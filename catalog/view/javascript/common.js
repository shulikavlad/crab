function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	} else { 			// Изменения для seo_url от Русской сборки OpenCart 3x
		var query = String(document.location.pathname).split('/');
		if (query[query.length - 1] == 'cart') value['route'] = 'checkout/cart';
		if (query[query.length - 1] == 'checkout') value['route'] = 'checkout/checkout';
		
		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}
$(document).ready(function() {
    $('.cF-home').submit(function(){
        let form = this;
		$.ajax({
			url: 'index.php?route=information/contact/sendMessage',
			type: 'post',
			data: $(form).serialize(),
			dataType: 'json',
			beforeSend: function() {
				$(form).find('.form-control').removeClass('error');
			},
			complete: function() {
				
			},
			success: function(json) {
                if (json.status)
                    $(form).html(`<div class="thank-you row align-items-center">
                    <div class="col-md-4">
                        <svg id="Layer_1" enable-background="new 0 0 512 512" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" style="width:100%; min-height: 150px; height: auto;"><path d="m201.3 185.1h12v25.2h-12z" fill="#c64f4f" transform="matrix(-.94 .342 -.342 -.94 469.73 312.509)"/><path d="m286.1 192.4h27.3v12h-27.3z" fill="#c64f4f" transform="matrix(-.389 .921 -.921 -.389 599.079 -.509)"/><path d="m199 158.2c-9.5 0-17.2 7.7-17.2 17.2s7.7 17.2 17.2 17.2 17.2-7.7 17.2-17.2-7.7-17.2-17.2-17.2z" fill="#f3ce7d"/><path d="m309 158.2c-9.5 0-17.2 7.7-17.2 17.2s7.7 17.2 17.2 17.2 17.2-7.7 17.2-17.2-7.7-17.2-17.2-17.2z" fill="#f3ce7d"/><path d="m506 356.1s1.9 39.1-67.2 6c2-.8 3.8-2.3 4.9-4.1 2.7-4.3 2.2-9.9-1.1-13.8-.1 0 49.4 20.1 63.4 11.9z" fill="#c64f4f"/><path d="m435.4 286c.9-.1 49.3-5.9 44-38.3 0 0 36 51.8-39.7 56.1 1.5-1.7 2.4-3.9 2.6-6.1.6-5-2.2-9.8-6.9-11.7z" fill="#c64f4f"/><path d="m435.4 286c4.7 1.9 7.5 6.7 6.9 11.7-.2 2.2-1.1 4.4-2.6 6.1l-.2.2c-1.5 1.7-3.6 2.8-5.8 3.2l-35.6 4.4c7.6-11.1-1.5-22.2-1.5-22.2l34.4-4c1.4-.1 2.9.1 4.4.6z" fill="#ec7774"/><path d="m396.5 289.5s9.2 11.1 1.5 22.2l-29.5 3.6-24.2 3 15.1-24.5z" fill="#c64f4f"/><path d="m402.6 328s8 17.5-6.3 25.3v.1l-46.6-11.4-17.3-4.3 11.9-19.4 24.2-3z" fill="#ef6865"/><path d="m442.5 344.2c3.4 3.8 3.8 9.4 1.1 13.8-1.2 1.9-2.9 3.3-4.9 4.1-1.1.5-2.4.7-3.6.7-1.1 0-2.2-.2-3.3-.6l-35.6-8.8v-.1c14.3-7.8 6.3-25.3 6.3-25.3l36.1 13.5c1.6.6 2.9 1.5 3.9 2.7z" fill="#ec7774"/><path d="m413.1 399.1c61.8 35.5 20.9 64.2 14.4 68.3-.1.1-.2 0-.3-.1 0-.1 0-.2 0-.2 16.5-26.9-25.1-53.9-25.1-53.9 4.9.5 9.5-2.6 10.9-7.4.4-1.1.5-2.3.5-3.5 0-1-.1-2.1-.4-3.2z" fill="#c64f4f"/><path d="m413.1 399.1c.3 1.1.5 2.1.5 3.2 0 1.2-.2 2.4-.5 3.5-1.3 4.8-5.9 7.9-10.9 7.4-1.5-.2-3-.7-4.3-1.6l-31.4-22.1c10.2-1.1 15.3-19.9 15.3-19.9l27.2 23.6c2 1.5 3.4 3.6 4.1 5.9z" fill="#ec7774"/><path d="m349.7 342 32 27.7s-5.1 18.8-15.3 19.9l-46.6-32.7c1.5-1.5 2.7-3.1 3.8-4.9l8.8-14.2z" fill="#c64f4f"/><path d="m190.3 359.6-44.6 31.2c-14.5-4.9-15.4-19.5-15.4-19.9l32.4-28 11.6-2.9 10.4 14.1c1.6 2 3.5 3.9 5.6 5.5z" fill="#c64f4f"/><path d="m145.7 390.8-31.6 22.1c-1.3.9-2.7 1.4-4.3 1.6-4.9.5-9.5-2.6-10.9-7.4-.4-1.1-.5-2.3-.5-3.5 0-1.1.2-2.2.4-3.2.7-2.4 2.1-4.4 4.1-5.9l27.2-23.6c.2.4 1.1 15 15.6 19.9z" fill="#ec7774"/><path d="m99 407c1.3 4.8 5.9 7.9 10.9 7.4s-42.1 27.3-24.7 54.4v.1h-.1c-4.2-2.5-49.9-32 13.9-68.6-.3 1.1-.4 2.1-.4 3.2-.2 1.2 0 2.4.4 3.5z" fill="#c64f4f"/><path d="m73.2 363.3c-69.2 33.1-67.2-6-67.2-6 13.8 8.1 62.2-11.4 63.4-11.9-3.4 3.8-3.8 9.4-1.1 13.7 1.2 1.9 2.9 3.3 4.9 4.2z" fill="#c64f4f"/><path d="m116.9 354.2-36.8 9.2c-2.2.8-4.7.8-6.9-.1-2-.8-3.8-2.3-4.9-4.1-2.7-4.3-2.2-9.9 1.1-13.7 1-1.2 2.3-2.1 3.8-2.7l36.7-13.4c0-.1-8.3 14.7 7 24.8z" fill="#ec7774"/><path d="m162.6 342.8-45.7 11.4c-15.3-10.1-7-24.9-7-24.9l30-11 20.7 3.3 13.6 18.3z" fill="#ef6865"/><path d="m140 318.3-24.8-4c-9-11.6.5-23.3.5-23.3l24.6 3.1 20.4 27.5z" fill="#c64f4f"/><path d="m76.6 287.2c1.4-.5 3-.7 4.5-.5l34.6 4.4s-9.5 11.6-.5 23.3l-36.8-5.9c-2.3-.4-4.3-1.5-5.8-3.2-.1-.1-.2-.2-.2-.2-1.5-1.7-2.4-3.8-2.6-6.1-.7-5.2 2.1-10 6.8-11.8z" fill="#ec7774"/><path d="m69.7 298.8c.2 2.3 1.1 4.4 2.6 6.1-75.7-4.3-39.7-56.1-39.7-56.1-5.3 32.4 43 38.2 44 38.3-4.7 1.9-7.5 6.7-6.9 11.7z" fill="#c64f4f"/><path d="m418.5 239.1c4.2 5.9 5 11.5 3.3 16.5-5 14.5-31.2 24.4-57.4 26.2.7-5.7-.8-11.4-4.2-16-1.3-1.7-2.6-3.5-4.2-5.4 0 0 37.3-36.6 56.9-26.4 2.2 1.2 4.2 3 5.6 5.1z" fill="#ef6865"/><path d="m358.4 43.1s31.2 17.1 28.3 42.7c-1.3 11.5 2.9 24.3 8.7 35.4-3.7-5.9-13.6-13.3-31.2-12.2-28.3 1.8-29.7-48.3-29.7-48.3s-50.8 91.7 92.4 144.8l34.8 3.4h.1c-.1-.1 55.4-137.9-103.4-165.8z" fill="#ec7774"/><path d="m360.2 265.8c-1.3-1.7-2.6-3.5-4.2-5.4-12.6-15.9-34-38.5-61.6-51.4-23.6-11-51.7-14.9-82.8-1.5-20.1 8.7-41.5 24.7-63.7 50.8-2.4 2.8-4.8 5.7-7.1 8.7-4 5.2-5.6 11.8-4.3 18.2.6 3.2 1.9 6.3 3.9 8.9l44.4 59.8c1.6 2.1 3.4 4 5.6 5.6 4.3 3.3 9.6 5 15 5h96.4c6.8 0 13.4-2.8 18.1-7.8 1.5-1.5 2.7-3.1 3.8-4.9l37.5-61.1c4.7-7.6 4.3-17.5-1-24.9z" fill="#ec7774"/><path d="m147.9 258.3c-2.4 2.8-4.8 5.7-7.1 8.7-4 5.2-5.6 11.8-4.3 18.2-26.6-1.8-46.8-13.3-48.1-28.8-.4-5 1.2-10.5 5.2-16.2 1.4-2.1 3.3-3.9 5.5-5.2 18.2-10.5 48.8 23.3 48.8 23.3z" fill="#ef6865"/><path d="m177.6 61.8s-1.4 50.1-29.7 48.3c-18.9-1.2-28.1 6.6-31.3 12.2 5.9-11.1 10.1-23.9 8.8-35.5-2.9-25.6 28.3-42.7 28.3-42.7-158.9 28.1-103.4 165.9-103.4 165.9l34.9-3.3c143.2-53.1 92.4-144.9 92.4-144.9z" fill="#ec7774"/><path d="m426.8 205.5c8.2-8 18.3-12.2 24.9-7.7 5.2 3.6 8.4 7.3 9.9 11.1 9.5 22.8-39.8 46.7-39.8 46.7 1.8-5 1-10.6-3.3-16.5-1.4-2.1-3.4-3.8-5.6-5l.1-.2c0-10.1 6.1-20.9 13.8-28.4z" fill="#c64f4f"/><path d="m99 235c-2.2 1.3-4.1 3.1-5.5 5.2-4 5.7-5.6 11.1-5.2 16.2h-.1s-47.9-23.7-37.9-46.4c1.6-3.7 4.8-7.4 10-11 6.6-4.5 16.7-.3 24.9 7.7 7.7 7.5 13.8 18.3 13.8 28.3z" fill="#c64f4f"/><g fill="#b75858"><path d="m356.2 108c-20.6-7-21.8-47.3-21.8-47.3s-50.8 91.7 92.4 144.8l34.8 3.4h.1s.4-1 1-2.8l-5.9-.6c-75.8-28.1-97.2-67.1-100.6-97.5z"/><path d="m214.7 354-44.4-59.8c-2-2.6-3.3-5.7-3.9-8.9-1.3-6.4.3-13.1 4.3-18.2 2.4-3 4.8-5.9 7.1-8.7 22.2-26.1 43.6-42.1 63.7-50.8 7.9-3.5 16.2-5.9 24.7-7.3-16.8-2.7-35.2-1.1-54.7 7.3-20.1 8.7-41.5 24.7-63.7 50.8-2.4 2.8-4.8 5.7-7.1 8.7-4 5.2-5.6 11.8-4.3 18.2.6 3.2 1.9 6.3 3.9 8.9l44.4 59.8c1.6 2.2 3.4 4.1 5.6 5.8 4.3 3.3 9.6 5.2 15 5.2h30c-5.4-.1-10.7-1.9-15-5.2-2.1-1.7-4-3.6-5.6-5.8z"/><path d="m136.4 58.1c5-5.5 10.8-10.1 17.2-13.9-158.8 28-103.3 165.8-103.3 165.8l29-2.7c-5.9-17.1-32.9-109.1 57.1-149.2z"/></g></svg>
                    </div>
                    <div class="col-md-8">
                        <h4>Спасибо за ваше сообщение!</h4>
                        <p>Наши менеджеры в ближайшее время обработают его и, при необходимости, свяжутся с вами!</p>
                    </div>
                </div>`); 
                else {
                    for (let item in json.errors) {
                        $(form).find('.form-control[name='+item+']').addClass('error');
                    }
                }
			},
			error: function(xhr, ajaxOptions, thrownError) {
				
			}
		});
        
        return false;
    });
    
	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// Currency
	$('#form-currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#form-currency input[name=\'code\']').val($(this).attr('name'));

		$('#form-currency').submit();
	});

	// Language
	$('#form-language .language-select').on('click', function(e) {
		e.preventDefault();

		$('#form-language input[name=\'code\']').val($(this).attr('name'));

		$('#form-language').submit();
	});

	/* Search */
	$('#search input[name=\'search\']').parent().find('button').on('click', function() {
		var url = $('base').attr('href') + 'index.php?route=product/search';

		var value = $('header #search input[name=\'search\']').val();

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
	});

	$('#search input[name=\'search\']').on('keydown', function(e) {
		if (e.keyCode == 13) {
			$('header #search input[name=\'search\']').parent().find('button').trigger('click');
		}
	});

	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 10) + 'px');
		}
	});

	// Product List
	$('#list-view').click(function() {
		$('#content .product-grid > .clearfix').remove();

		$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
		$('#grid-view').removeClass('active');
		$('#list-view').addClass('active');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		// What a shame bootstrap does not take into account dynamically loaded columns
		var cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		$('#list-view').removeClass('active');
		$('#grid-view').addClass('active');

		localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
		$('#list-view').addClass('active');
	} else {
		$('#grid-view').trigger('click');
		$('#grid-view').addClass('active');
	}

	// Checkout
	$(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function(e) {
		if (e.keyCode == 13) {
			$('#collapse-checkout-option #button-login').trigger('click');
		}
	});

	// tooltips on hover
	//$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});
    
  const storeItem = document.querySelectorAll(".store-card-item");
  let coord = [];
  
  Array.from(storeItem).forEach(function (item) {
    const id = item.getAttribute("data-attr");
    const geo = item.getAttribute("data-geo").split(',');
    const addr = $(item).find('.store-card-adress').text();
    const phone = $(item).find('.store-card-text a').text();
    const time = $(item).find('.store-card-text p span').text();
      
    coord[id] = [geo[0]*1, geo[1]*1, addr, phone, time];
    
    item.addEventListener("click", function (e) {
      //const id = item.getAttribute("data-attr");
      removeAddActive(id, storeItem, "active");
      initMap(id, coord);
    });
  });

  function removeAddActive(id, element, className) {
    Array.from(element).forEach(function (item) {
      if (id && id == item.getAttribute("data-attr")) {
        item.classList.add(className);
      } else {
        item.classList.remove(className);
      }
    });
  }
  initMap(null, coord);    
  
  $('.subcategory-filter li').click(function() {
        $('#load-format-pagination .product-layout').hide();
        $('#load-format-pagination .product-layout.cat-'+$(this).data('filter')).show();
        
        $('.subcategory-filter li').removeClass('active');
        $(this).addClass('active');
  });
  
  initAdditionsCatalog();
  
  $('#banner-nav-home ul li a').hover(function(){
      for (let i=2; i<=3; i++) {
        setTimeout(function(){ 
            $('#banner-nav-home').removeAttr('class');
            $('#banner-nav-home').addClass('state'+i);
        }, 100);  
      }
  }, function(){
        $('#banner-nav-home').removeAttr('class');
        $('#banner-nav-home').addClass('state1');
  });
  
  cartModal = new bootstrap.Modal(document.getElementById('modal-cart'));
});

function initAdditionsCatalog () {
  $('.product-params select.form-control, #d_quickcheckout select').niceSelect();
  
  $('.input-qty-btn').click(function(){
      let input = $(this).siblings('.input-qty-field');
      let item = $(this).closest('.product-thumb');
      let curValue = parseInt($(input).val());
      
      if ($(this).hasClass('plus'))
          $(input).val(curValue + 1);
      else {
          if (curValue - 1 >= 1)
            $(input).val(curValue - 1);
      }
      
      $(input).trigger('change');
  });
}

var cartModal = false;

function updateModal (data) {
    $('#modal-cart .modal-body').html('');
    let product = false;
    let option = '';
    let html = '';
            html += `<div class="row mt-3 cart-modal-header align-items-center d-none d-md-flex">
              <div class="col-2"> Товар</div>
              <div class="col-2 offset-3">Количество</div>
              <div class="col-2 offset-1">Цена</div>
              <div class="col-2">Сумма</div>
            </div>`;
            
    let htmlCart = '';
            htmlCart += `<div class="row mt-3 cart-modal-header align-items-center d-none d-md-flex">
                    <div class="col-md-5 col-lg-5 col-10 order-0"> Товар</div>
                    <div class="col-md-2 col-lg-2 col-4 order-md-2 order-1 py-2 py-md-0 product-qty">Количество</div>
                    <div class="offset-md-1 col-md-2 col-lg-2 col-4 order-md-4 order-1 product-price">Цена</div>
                    <div class="col-md-2 col-lg-2 col-4 order-md-5 order-1 product-total">Сумма</div>
                </div>`;
            
    for (let i = 0; i < data.products.length; i++) {
        option = '';
        product = data.products[i];
        if (product.option)
            for (let j = 0; j < product.option.length; j++) {
                option += ', ' + product.option[j].value;
            }
            
            html += `<div class="row mt-3 align-items-center">
              <div class="col-md-2 col-3 order-0 product-img"> <a href="`+product.href+`"><img src="`+product.thumb+`" alt="`+product.name+`" title="`+product.name+`" class="img-thumbnail"></a></div>
              <div class="col-md-3 col-7 product-name order-0"><a href="`+product.href+`">`+product.name+`</a><small>`+option+`</small></div>
              <div class="col-md-2 col-4 order-md-2 order-1 py-2 py-md-0 product-qty">
                        <div class="col-12 form-group d-flex align-items-center justify-content-between">
                            <div class="input-qty">
                                <div class="input-qty-btn minus"></div>
                                <input type="text" class="input-qty-field" name="quantity" value="`+product.quantity+`" size="2" id="input-quantity" onchange="cart.update('` + product.cart_id +`', this.value);">
                                <div class="input-qty-btn plus"></div>
                            </div> 
                        </div>
              </div>
              <div class="col-1 text-center order-md-3 order-0 produt-rm"><button type="button" onclick="cart.remove('`+product.cart_id+`');" title="Удалить" class="btn btn-rm"></button></div>
              <div class="col-md-2 col-4 order-md-4 order-1 product-price"><span>`+product.price+`</span> грн.</div>
              <div class="col-md-2 col-4 order-md-5 order-1 product-total"><span>`+product.total+`</span> грн.</div>
            </div>`;
            
            htmlCart += `<div class="row mt-3 align-items-center {% if not product.stock %}not-in-stock{% endif %}">
                  <div class="col-md-2 col-lg-1 col-3 order-0 product-img p-0"><a href="`+product.href+`"><img src="`+product.thumb+`" alt="`+product.name+`" title="`+product.name+`" class="img-thumbnail" /></a></div>
                  <div class="col-md-3 col-lg-4 col-7 product-name order-0"><a href="`+product.href+`">`+product.name+`</a><small>`+option+`</small>
                  </div>
                  <div class="col-md-2 col-lg-2 col-4 order-md-2 order-1 py-2 py-md-0 product-qty">
                            <div class="col-12 form-group d-flex align-items-center justify-content-between">
                                <div class="input-qty">
                                    <div class="input-qty-btn minus"></div>
                                    <input type="text" class="input-qty-field" name="quantity" value="`+product.quantity+`" size="2" id="input-quantity" onchange="cart.update('`+product.cart_id+`', this.value);">
                                    <div class="input-qty-btn plus"></div> 
                                </div>
                            </div>
                  </div>
                  <div class="col-1 text-center order-md-3 order-0 produt-rm"><button type="button" onclick="cart.remove('`+product.cart_id+`');" title="Удалить" class="btn btn-rm"></button></div>
                  <div class="col-md-2 col-lg-2 col-4 order-md-4 order-1 product-price"><span>`+product.price+`</span> грн.</div>
                  <div class="col-md-2 col-lg-2 col-4 order-md-5 order-1 product-total"><span>`+product.total+`</span> грн.</div>
                </div>`;
    }
    
    $('#cart-total').html(data['total']);
    $('#cart-total-sum, #cartpage-total-sum').html(data.total_summ);
    $('#modal-cart .modal-body').html(html);
    $('.cartpage-products').html(htmlCart);
    initAdditionsCatalog();
}
    
// Cart add remove functions
var cart = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: $('#product-'+product_id+' input[type=\'text\'], #product-'+product_id+' input[type=\'hidden\'], #product-'+product_id+' input[type=\'radio\']:checked, #product-'+product_id+' input[type=\'checkbox\']:checked, #product-'+product_id+' select, #product-'+product_id+' textarea'),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				$('.alert-dismissible, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					// $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
                        $('#cart-total-sum').html(json['total_summ']);
                        
                        if (!json['total_summ'])
                            $('#cart-total .cart-totals-row').hide();
                        else
                            $('#cart-total .cart-totals-row').show();
					}, 100);

					//$('html, body').animate({ scrollTop: 0 }, 'slow');
					//$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
                
                updateModal(json);
                cartModal.show();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
/* 				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100); */
                console.log('upd');
				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					//location = 'index.php?route=checkout/cart';
				} else {
					//$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}

                updateModal(json);
                //cartModal.show();                
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart-total').html(json['total']);
					$('#cart-total-sum').html(json['total_summ']);
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
                
                updateModal(json);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert-dismissible').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				$('#wishlist-total span').html(json['total']);
				$('#wishlist-total').attr('title', json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function() {

	}
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert-dismissible').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#compare-total').html(json['total']);

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function() {

	}
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div>';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();

			$.extend(this, option);

			$(this).attr('autocomplete', 'off');

			// Focus
			$(this).on('focus', function() {
				this.request();
			});

			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}
			});

			// Click
			this.click = function(event) {
				event.preventDefault();

				value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}

			// Show
			this.show = function() {
				var pos = $(this).position();

				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});

				$(this).siblings('ul.dropdown-menu').show();
			}

			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}

			// Request
			this.request = function() {
				clearTimeout(this.timer);

				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}

			// Response
			this.response = function(json) {
				html = '';

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}

					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}

					// Get all the ones with a categories
					var category = new Array();

					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}

							category[json[i]['category']]['item'].push(json[i]);
						}
					}

					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$(this).siblings('ul.dropdown-menu').html(html);
			}

			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

		});
	}
})(window.jQuery);
  
//Map-settings
function initMap(id, coord) {
  //try {
      console.log(coord);
    const isAnimate = id ? false : true;
/*     const coord = [
      [
        46.43441657392796,
        30.76065171505657,
        "г. Одесса, Генуэзская 3Б",
        "+38 (093) 612-12-12",
        "10:00-22:00",
      ], //Генуезська
      [
        46.477967032294345,
        30.7348557267025,
        "Одесса, Книжный рынок",
        "+38 (093) 612-12-12",
        "9:00-21:00",
      ], //Книжный
      [
        46.35577267552708,
        30.705660297862934,
        "г. Одесса, Жемчужная 5Б",
        "+38 (093) 612-12-12",
        "10:00-22:00",
      ], //Перлинна
    ]; */
    const sumCols = coord.length ? coord[0].map((n, i) =>
      coord.reduce((sum, m) => sum + m[i], 0)
    ) : 0;

    if (document.getElementById("stores-map")) {
      const map = new google.maps.Map(document.getElementById("stores-map"), {
        zoom: 12,
        center: new google.maps.LatLng(
          sumCols[0] / coord.length,
          sumCols[1] / coord.length
        ),
        scrollwheel: false,
      });
      map.setOptions({ styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
] });
      
      const iconBase = "image/catalog/"; //папка с картинками
      const icons = {
        //разные картинки на типы объектов
        pin: { icon: iconBase + "pinMap.svg" },
        pinActive: { icon: iconBase + "pinMapActive.svg" },
      };
      let features = [];
      //Маркеры с даными
      for (let i = 0; i < coord.length; i++) {
        let typePin = "pin";
        if (id && i == id) {
          typePin = "pinActive";
        }

        features.push({
          position: new google.maps.LatLng(coord[i][0], coord[i][1]),
          title: coord[i][2],
          type: typePin,
          html: `<br><p>тел: <span>${coord[i][3]}</span></p><p>График работы: ${coord[i][4]}</p>`,
        });
      }

      //для всплывайки
      let contentString = "";
      const infowindow = new google.maps.InfoWindow({ content: contentString });

      //инициализируем маркеты
      features.forEach(function (feature) {
        const marker = new google.maps.Marker({
          position: feature.position,
          icon: icons[feature.type].icon,
          title: feature.title,
          html: feature.html,
          animation: isAnimate ? google.maps.Animation.DROP : null,
          map: map,
        });
        //передаем во всплывайку текст маркера
        marker.addListener("click", function () {
          infowindow.setContent(this.title + this.html);
          infowindow.open(map, marker);
        });
      });
    }
  //} catch (e) { }
}

