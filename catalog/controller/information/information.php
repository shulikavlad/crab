<?php
class ControllerInformationInformation extends Controller {
	public function index() {
		$this->load->language('information/information');

		$this->load->model('catalog/information');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$information_info = $this->model_catalog_information->getInformation($information_id);
        
		if ($information_info) {
			$this->document->setTitle($information_info['meta_title']);
			$this->document->setDescription($information_info['meta_description']);
			$this->document->setKeywords($information_info['meta_keyword']);

			$data['breadcrumbs'][] = array(
				'text' => $information_info['title'],
				'href' => $this->url->link('information/information', 'information_id=' .  $information_id)
			);

			$data['heading_title'] = $information_info['title'];

			$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
			if (mb_stristr($data['description'], '{{our-partners}}')) {
                $partners_html = '<div class="parthers-list">';
                $partners = $this->getPartners();
                foreach($partners as $partner) {
                    $partners_html .= '<div class="parthers-list-item">
                        <img src="/image/'.$partner['image'].'">
                        <div class="parthner-text-block">
                            <h4 class="parthner-text-title">'.$partner['title'].'</h4>
                            <div class="parthner-text-descr">
                                <p>'.$partner['text'].'</p>
                            </div>
                        </div>
                    </div>';
                }
                
                $partners_html .= '</div>';
                $data['description'] = str_replace('{{our-partners}}', $partners_html, $data['description']);
/*                
		<div class="parthers-list">
			<div class="parthers-list-item">
				<img src="image/catalog/1parth.jpg">
				<div class="parthner-text-block">
					<h4 class="parthner-text-title">The HUNTER Fishing Club</h4>
					<div class="parthner-text-descr">
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<p>
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
					</div>
				</div>
			</div>
			<div class="parthers-list-item">
				<img src="image/catalog/4parth.jpg">
				<div class="parthner-text-block">
					<h4 class="parthner-text-title">The HUNTER Fishing Club</h4>
					<div class="parthner-text-descr">
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<p>
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
					</div>
				</div>
			</div>
			<div class="parthers-list-item">
				<img src="image/catalog/3parth.jpg">
				<div class="parthner-text-block">
					<h4 class="parthner-text-title">The HUNTER Fishing Club</h4>
					<div class="parthner-text-descr">
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<p>
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
					</div>
				</div>
			</div>
			<div class="parthers-list-item">
				<img src="image/catalog/4parth.jpg">
				<div class="parthner-text-block">
					<h4 class="parthner-text-title">The HUNTER Fishing Club</h4>
					<div class="parthner-text-descr">
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<p>
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
					</div>
				</div>
			</div>
			<div class="parthers-list-item">
				<img src="image/catalog/5parth.jpg">
				<div class="parthner-text-block">
					<h4 class="parthner-text-title">The HUNTER Fishing Club</h4>
					<div class="parthner-text-descr">
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<p>
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
					</div>
				</div>
			</div>
			<div class="parthers-list-item">
				<img src="image/catalog/1parth.jpg">
				<div class="parthner-text-block">
					<h4 class="parthner-text-title">The HUNTER Fishing Club</h4>
					<div class="parthner-text-descr">
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
						<p>
							Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
					</div>
				</div>
			</div>
		</div>*/
            }

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			
            
            if ($information_info['information_id'] == 4) {
                $this->load->model('design/banner');
                $this->load->model('tool/image');
                
                $banner = $this->model_design_banner->getBanner(9);
                foreach($banner as &$slide) {
                    $slide['image'] = '/image/'.$slide['image'];
                    //$slide['image'] = $this->model_tool_image->resize($slide['image'], 1200, 600);
                }
                $data['banner'] = $banner;
            }
            
            $this->response->setOutput($this->load->view('information/information', $data));
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

    public function getPartners(){
		$query2 = $this->db->query("SELECT * FROM " . DB_PREFIX . "partners f LEFT JOIN " . DB_PREFIX . "partners_description fd ON (f.partners_id = fd.partners_id) where fd.language_id = '" . (int)$this->config->get('config_language_id') . "' and f.status=1 order by f.sort_order");
		$data=array();
		foreach($query2->rows as $row2){
			$data[]=array(
                'title' => $row2['title'], 
                'text' =>$row2['text'], 
                'image' =>$row2['image']
            );
		}
			
        return $data;
    }
        
	public function agree() {
		$this->load->model('catalog/information');

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$output = '';

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$output .= html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
		}

		$this->response->addHeader('X-Robots-Tag: noindex');

		$this->response->setOutput($output);
	}
}
