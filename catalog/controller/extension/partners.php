<?php
class ControllerExtensionPartners extends Controller {
	public function index() {
		$this->load->language('extension/partners');
		
		$this->load->model('catalog/information');

		$data['partners'] = $this->model_catalog_information->getPartners();
		
		return $this->load->view('extension/partners', $data);
	}
}
