<?php
// Heading
$_['heading_title']    = 'Our Partners';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified partners module!';
$_['text_edit']        = 'Edit partners Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify partners module!';
