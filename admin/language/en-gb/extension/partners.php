<?php
// Heading
$_['heading_title']     = 'Partners';

// Text
$_['text_success']      = 'Success: You have modified Partners!';
$_['text_default']      = 'Default';
$_['text_list']         = 'Partners List';
$_['text_add']         = 'Add Partners';
$_['text_edit']         = 'Edit Partners';

// Column
$_['column_title']      = 'Partners Title';
$_['column_sort_order']	= 'Sort Order';
$_['column_action']     = 'Action';
$_['button_insert']     = 'Add';

// Entry
$_['entry_title']       = 'Partners Title:';
$_['entry_description'] = 'Description:';
$_['entry_store']       = 'Stores:';
$_['entry_keyword']     = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_bottom']      = 'Bottom:<br/><span class="help">Display in the bottom footer.</span>';
$_['entry_top']      = 'Top:<br/><span class="help">Display in the top header.</span>';
$_['entry_status']      = 'Status:';
$_['entry_sort_order']  = 'Sort Order:';
$_['entry_link']  = 'Link:';
$_['entry_select_heading']  = 'Select Heading';
$_['column_link']  = 'Link';
$_['text_select']  = '--Select--';


// Error 
$_['error_warning']     = 'Warning: Please check the form carefully for errors!';
$_['error_permission']  = 'Warning: You do not have permission to modify Partners!';
$_['error_title']       = 'Partners Title must be between 3 and 64 characters!';
$_['error_description'] = 'Description must be more than 3 characters!';
$_['error_account']     = 'Warning: This Partners page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']    = 'Warning: This Partners page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']   = 'Warning: This Partners page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_store']       = 'Warning: This Partners page cannot be deleted as its currently used by %s stores!';
?>