<?php
class ModelExtensionpartners extends Model {
	public function install() {
	$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."partners` (
  `partners_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL, 
  PRIMARY KEY (`partners_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."partners_description` (
  `partners_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
	}
	public function uninstall() {
	$this->db->query("DROP TABLE IF EXISTS `".DB_PREFIX."partners`");
	$this->db->query("DROP TABLE IF EXISTS `".DB_PREFIX."partners_description`");
	}
	
	public function addpartners($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "partners SET image = '" .$data['image'] . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$partners_id = $this->db->getLastId(); 
		
		foreach ($data['partners_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "partners_description SET partners_id = '" . (int)$partners_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', text = '" . $this->db->escape($value['text']) . "'");
		}

		$this->cache->delete('partners');
	}
	
	public function editpartners($partners_id, $data) {
		
		$this->db->query("UPDATE " . DB_PREFIX . " image = '" .$data['image'] . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE partners_id = '" . (int)$partners_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "partners_description WHERE partners_id = '" . (int)$partners_id . "'");
		
		foreach ($data['partners_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "partners_description SET partners_id = '" . (int)$partners_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', text = '" . $this->db->escape($value['text']) . "'");
		}
		
		$this->cache->delete('partners');
	}
	
	public function deletepartners($partners_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "partners WHERE partners_id = '" . (int)$partners_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "partners_description WHERE partners_id = '" . (int)$partners_id . "'");
		
		

		$this->cache->delete('partners');
	}	
	public function getpartners($partners_id) {
	
			$sql = "SELECT * FROM " . DB_PREFIX . "partners where partners_id='".$partners_id."' ";
			$query = $this->db->query($sql);
			return $query->row;
		
	}
		

	
	public function getpartnerss($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "partners i LEFT JOIN " . DB_PREFIX . "partners_description id ON (i.partners_id = id.partners_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
			$sort_data = array(
				'id.title'
			);		
		
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY id.title";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}		

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
			
			$query = $this->db->query($sql);
			
			return $query->rows;
		} else {
			$partners_data = $this->cache->get('partners.' . (int)$this->config->get('config_language_id'));
		
			if (!$partners_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "partners i LEFT JOIN " . DB_PREFIX . "partners_description id ON (i.partners_id = id.partners_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");
	
				$partners_data = $query->rows;
			
				$this->cache->set('partners.' . (int)$this->config->get('config_language_id'), $partners_data);
			}	
	
			return $partners_data;			
		}
	}
	
	public function getpartnersDescriptions($partners_id) {
		$partners_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "partners_description WHERE partners_id = '" . (int)$partners_id . "'");

		foreach ($query->rows as $result) {
			$partners_description_data[$result['language_id']] = array(
				'title'       => $result['title'],
				'text'       => $result['text'],
				);
		}
		
		return $partners_description_data;
	}
	
	
		
	public function getTotalpartnerss() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "partners_description");
		
		return $query->row['total'];
	}	
	
	
}
?>